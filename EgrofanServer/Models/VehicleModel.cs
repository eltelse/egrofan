﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Models
{
    public class VehicleModel
    {
        // PK Identity
        public int Id { set; get; }
        public string LicensePlateNumber { set; get; }
        public int VehicleTypeId { get; set; }
        public string Remark { set; get; }
    }
}

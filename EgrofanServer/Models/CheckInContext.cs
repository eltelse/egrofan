﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Models
{
    public class CheckInContext : DbContext
    {
        public CheckInContext(DbContextOptions<CheckInContext> options) : base(options)
        {

        }

        public DbSet<CheckIn> checkIn { get; set; }
    }
}

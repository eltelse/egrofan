﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Models
{
    public class InspectionTypeModel
    {
        public string Id { set; get; }
        public string Name {set;get;}
    }
}

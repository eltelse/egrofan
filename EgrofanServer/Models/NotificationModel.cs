﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Models
{
    public class NotificationModel
    {
        public int Id { get; set; }
        public int NotificationTypeId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string JsonData { get; set; }
        public int SourceUserId { get; set; }
        public int TargetUserId { get; set; }
        public string Reply { get; set; }
        public bool? IsReplyable { get; set; }
        public bool? IsHandled { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool? IsDeleted { get; set; }
    }

    public class NotificationIsHandledModel
    {
        public int Id { get; set; }
        public bool IsHandled { get; set; }
    }
}

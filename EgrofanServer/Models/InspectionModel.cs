﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Models
{
    public class InspectionModel
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public int VehicleId { get; set; }
        public int WorkflowTypeId { get; set; }
        public string LPR { get; set; }
        public int LPRCorrectionTypeId { get; set; }
        public string CCR { get; set; }
        public int CCRCorrectionTypeId { get; set; }
        public string ManifestCode { get; set; }
        public string DriverTagCode { get; set; }
        public int ConclusionTypeId { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime LeaveTime { get; set; }
        public DateTime InspectionEndTime { get; set; }
        public string Remark { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool? IsDeleted { get; set; }
    }
}

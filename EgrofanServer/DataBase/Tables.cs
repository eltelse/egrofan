﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Models
{
    public class Vehicle
    {
        public long id { set; get; }
        public string licensePlateNumber { set; get; }
        public string containerNumber { set; get; }
        public DateTime arrivedSiteDate { set; get; }
        public string driverTag { set; get; }
        public int workflowId { set; get; }
        public DateTime checkInDateTime { set; get; }
        public string checkInUser { set; get; }
        public DateTime enterScantunnelDateTime { set; get; }
        public DateTime concludeDateTime { set; get; }
        public string conclusionType { set; get; }
        public string suspiciousType { set; get; }
        public string inspectWorkFlow { set; get; }
    }
}

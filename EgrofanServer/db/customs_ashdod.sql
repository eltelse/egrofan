USE [Customs_Ashdod]
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_vehicles_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [mgmt].[sp_import_vehicles_from_sdms]
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_inspection_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [mgmt].[sp_import_inspection_from_sdms]
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_inspection_data_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [mgmt].[sp_import_inspection_data_from_sdms]
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [mgmt].[sp_import_from_sdms]
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_device_events_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [mgmt].[sp_import_device_events_from_sdms]
GO
/****** Object:  StoredProcedure [mgmt].[sp_add_table_display_columns]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [mgmt].[sp_add_table_display_columns]
GO
/****** Object:  StoredProcedure [dbo].[sp_work_station_update]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_work_station_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_work_station_add]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_work_station_add]
GO
/****** Object:  StoredProcedure [dbo].[sp_vehicle_update]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_vehicle_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_vehicle_add]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_vehicle_add]
GO
/****** Object:  StoredProcedure [dbo].[sp_user_update]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_user_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_user_add]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_user_add]
GO
/****** Object:  StoredProcedure [dbo].[sp_type_entry_update]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_type_entry_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_type_entry_add]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_type_entry_add]
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_update_ishandled]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_notification_update_ishandled]
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_update]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_notification_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_add_return_id]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_notification_add_return_id]
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_add]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_notification_add]
GO
/****** Object:  StoredProcedure [dbo].[sp_inspection_update]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_inspection_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_inspection_init]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_inspection_init]
GO
/****** Object:  StoredProcedure [dbo].[sp_inspection_add]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_inspection_add]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_type_table_entries]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP PROCEDURE [dbo].[sp_get_type_table_entries]
GO
ALTER TABLE [dbo].[WorkStation] DROP CONSTRAINT [FK_WorkStation_WorkStationType]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK_Vehicle_VehicleType]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [FK_Notification_TargetUserId]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [FK_Notification_SourceUserId]
GO
ALTER TABLE [dbo].[Lane] DROP CONSTRAINT [FK_Lane_AreaId]
GO
ALTER TABLE [dbo].[InspectionDataHistory] DROP CONSTRAINT [FK_InspectionDataHistory_UpdaterSourceType]
GO
ALTER TABLE [dbo].[InspectionData] DROP CONSTRAINT [FK_InspectionData_InspectionDataType]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [FK_Inspection_WorkflowType]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [FK_Inspection_ConclusionType]
GO
ALTER TABLE [dbo].[DeviceEvent] DROP CONSTRAINT [FK_DeviceEvent_DeviceEventType]
GO
ALTER TABLE [dbo].[Device] DROP CONSTRAINT [FK_Device_Lane]
GO
ALTER TABLE [dbo].[Device] DROP CONSTRAINT [FK_Device_DeviceType]
GO
ALTER TABLE [dbo].[Area] DROP CONSTRAINT [FK_Area_AreaType]
GO
ALTER TABLE [type].[WorkStationType] DROP CONSTRAINT [DF_WorkStation_isDeleted]
GO
ALTER TABLE [type].[WorkStationType] DROP CONSTRAINT [DF_WorkStation_updated]
GO
ALTER TABLE [type].[WorkStationType] DROP CONSTRAINT [DF_WorkStation_created]
GO
ALTER TABLE [type].[VehicleType] DROP CONSTRAINT [DF_VehicleType_isDeleted]
GO
ALTER TABLE [type].[VehicleType] DROP CONSTRAINT [DF_VehicleType_updated]
GO
ALTER TABLE [type].[VehicleType] DROP CONSTRAINT [DF_VehicleType_created]
GO
ALTER TABLE [type].[NotificationType] DROP CONSTRAINT [DF_NotificationType_isDeleted]
GO
ALTER TABLE [type].[NotificationType] DROP CONSTRAINT [DF_NotificationType_updated]
GO
ALTER TABLE [type].[NotificationType] DROP CONSTRAINT [DF_NotificationType_created]
GO
ALTER TABLE [type].[InspectionWorkflowType] DROP CONSTRAINT [DF_InspectionWorkflow_isDeleted]
GO
ALTER TABLE [type].[InspectionWorkflowType] DROP CONSTRAINT [DF_InspectionWorkflow_updated]
GO
ALTER TABLE [type].[InspectionWorkflowType] DROP CONSTRAINT [DF_InspectionWorkflow_created]
GO
ALTER TABLE [type].[InspectionOperationType] DROP CONSTRAINT [DF_InspectionOperation_isDeleted]
GO
ALTER TABLE [type].[InspectionOperationType] DROP CONSTRAINT [DF_InspectionOperation_updated]
GO
ALTER TABLE [type].[InspectionOperationType] DROP CONSTRAINT [DF_InspectionOperation_created]
GO
ALTER TABLE [type].[InspectionHistorySourceType] DROP CONSTRAINT [DF_InspectionHistorySourceType_isDeleted]
GO
ALTER TABLE [type].[InspectionHistorySourceType] DROP CONSTRAINT [DF_InspectionHistorySourceType_updated]
GO
ALTER TABLE [type].[InspectionHistorySourceType] DROP CONSTRAINT [DF_InspectionHistorySourceType_created]
GO
ALTER TABLE [type].[InspectionDataType] DROP CONSTRAINT [DF_InspectionData_isDeleted]
GO
ALTER TABLE [type].[InspectionDataType] DROP CONSTRAINT [DF_InspectionData_updated]
GO
ALTER TABLE [type].[InspectionDataType] DROP CONSTRAINT [DF_InspectionData_created]
GO
ALTER TABLE [type].[DeviceType] DROP CONSTRAINT [DF_DeviceType_isDeleted]
GO
ALTER TABLE [type].[DeviceType] DROP CONSTRAINT [DF_DeviceType_updated]
GO
ALTER TABLE [type].[DeviceType] DROP CONSTRAINT [DF_DeviceType_created]
GO
ALTER TABLE [type].[DeviceStatusType] DROP CONSTRAINT [DF_DeviceStatus_isDeleted]
GO
ALTER TABLE [type].[DeviceStatusType] DROP CONSTRAINT [DF_DeviceStatus_updated]
GO
ALTER TABLE [type].[DeviceStatusType] DROP CONSTRAINT [DF_DeviceStatus_created]
GO
ALTER TABLE [type].[DeviceEventType] DROP CONSTRAINT [DF_DeviceEventType_isDeleted]
GO
ALTER TABLE [type].[DeviceEventType] DROP CONSTRAINT [DF_DeviceEventType_updated]
GO
ALTER TABLE [type].[DeviceEventType] DROP CONSTRAINT [DF_DeviceEventType_created]
GO
ALTER TABLE [type].[DeviceCommandSourceType] DROP CONSTRAINT [DF_DeviceCommandSource_isDeleted]
GO
ALTER TABLE [type].[DeviceCommandSourceType] DROP CONSTRAINT [DF_DeviceCommandSource_updated]
GO
ALTER TABLE [type].[DeviceCommandSourceType] DROP CONSTRAINT [DF_DeviceCommandSource_created]
GO
ALTER TABLE [type].[DataCorrectionType] DROP CONSTRAINT [DF_OCRCorrection_isDeleted]
GO
ALTER TABLE [type].[DataCorrectionType] DROP CONSTRAINT [DF_OCRCorrection_updated]
GO
ALTER TABLE [type].[DataCorrectionType] DROP CONSTRAINT [DF_OCRCorrection_created]
GO
ALTER TABLE [type].[ConclusionType] DROP CONSTRAINT [DF_Conclusion_isDeleted]
GO
ALTER TABLE [type].[ConclusionType] DROP CONSTRAINT [DF_Conclusion_updated]
GO
ALTER TABLE [type].[ConclusionType] DROP CONSTRAINT [DF_Conclusion_created]
GO
ALTER TABLE [type].[AreaType] DROP CONSTRAINT [DF_AreaType_isDeleted]
GO
ALTER TABLE [type].[AreaType] DROP CONSTRAINT [DF_AreaType_updated]
GO
ALTER TABLE [type].[AreaType] DROP CONSTRAINT [DF_AreaType_created]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] DROP CONSTRAINT [DF_TableDisplayedColumns_isDeleted]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] DROP CONSTRAINT [DF_TableDisplayedColumns_updated]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] DROP CONSTRAINT [DF_TableDisplayedColumns_created]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] DROP CONSTRAINT [DF_TableDisplayedColumns_isHidden]
GO
ALTER TABLE [dbo].[WorkStation] DROP CONSTRAINT [DF_WorkStation_isDeleted]
GO
ALTER TABLE [dbo].[WorkStation] DROP CONSTRAINT [DF_WorkStation_updated]
GO
ALTER TABLE [dbo].[WorkStation] DROP CONSTRAINT [DF_WorkStation_created]
GO
ALTER TABLE [dbo].[WorkStation] DROP CONSTRAINT [DF_WorkStation_IsActive]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [DF_Vehicle_isDeleted]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [DF_Vehicle_updated]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [DF_Vehicle_created]
GO
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [DF_Vehicle_VehicleTypeId]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [DF_Notification_isDeleted]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [DF_Notification_updated]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [DF_Notification_created]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [DF_Notification_IsHandled]
GO
ALTER TABLE [dbo].[Notification] DROP CONSTRAINT [DF_Notification_IsReplyable]
GO
ALTER TABLE [dbo].[Lane] DROP CONSTRAINT [DF_Lane_isDeleted]
GO
ALTER TABLE [dbo].[Lane] DROP CONSTRAINT [DF_Lane_updated]
GO
ALTER TABLE [dbo].[Lane] DROP CONSTRAINT [DF_Lane_created]
GO
ALTER TABLE [dbo].[InspectionHistory] DROP CONSTRAINT [DF_InspectionHistory_isDeleted]
GO
ALTER TABLE [dbo].[InspectionHistory] DROP CONSTRAINT [DF_InspectionHistory_created]
GO
ALTER TABLE [dbo].[InspectionHistory] DROP CONSTRAINT [DF_InspectionHistory_ConclusionTypeId]
GO
ALTER TABLE [dbo].[InspectionDataHistory] DROP CONSTRAINT [DF_InspectionDataHistory_isDeleted]
GO
ALTER TABLE [dbo].[InspectionDataHistory] DROP CONSTRAINT [DF_InspectionDataHistory_created]
GO
ALTER TABLE [dbo].[InspectionDataHistory] DROP CONSTRAINT [DF_InspectionDataHistory_Value]
GO
ALTER TABLE [dbo].[InspectionData] DROP CONSTRAINT [DF_InspectionData_isDeleted]
GO
ALTER TABLE [dbo].[InspectionData] DROP CONSTRAINT [DF_InspectionData_updated]
GO
ALTER TABLE [dbo].[InspectionData] DROP CONSTRAINT [DF_InspectionData_created]
GO
ALTER TABLE [dbo].[InspectionData] DROP CONSTRAINT [DF_InspectionData_Value]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_isDeleted]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_updated]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_created]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_arrivaltime]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_ConclusionTypeId]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_ContainerCodeCorrectionTypeId]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_LicencePlateNumberCorrectionTypeId]
GO
ALTER TABLE [dbo].[Inspection] DROP CONSTRAINT [DF_Inspection_WorkflowTypeId]
GO
ALTER TABLE [dbo].[DeviceEvent] DROP CONSTRAINT [DF_DeviceEvent_created]
GO
ALTER TABLE [dbo].[Device] DROP CONSTRAINT [DF_Device_isDeleted]
GO
ALTER TABLE [dbo].[Device] DROP CONSTRAINT [DF_Device_updated]
GO
ALTER TABLE [dbo].[Device] DROP CONSTRAINT [DF_Device_created]
GO
ALTER TABLE [dbo].[Device] DROP CONSTRAINT [DF_Device_IsActive]
GO
ALTER TABLE [dbo].[Area] DROP CONSTRAINT [DF_Area_isDeleted]
GO
ALTER TABLE [dbo].[Area] DROP CONSTRAINT [DF_Area_updated]
GO
ALTER TABLE [dbo].[Area] DROP CONSTRAINT [DF_Area_created]
GO
ALTER TABLE [auth].[User] DROP CONSTRAINT [DF_User_isDeleted]
GO
ALTER TABLE [auth].[User] DROP CONSTRAINT [DF_User_updated]
GO
ALTER TABLE [auth].[User] DROP CONSTRAINT [DF_User_created]
GO
/****** Object:  Index [IX_Vehicle_LPR_Unique]    Script Date: 1/23/2022 9:25:00 AM ******/
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [IX_Vehicle_LPR_Unique]
GO
/****** Object:  Table [type].[InspectionOperationType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[InspectionOperationType]') AND type in (N'U'))
DROP TABLE [type].[InspectionOperationType]
GO
/****** Object:  Table [type].[InspectionHistorySourceType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[InspectionHistorySourceType]') AND type in (N'U'))
DROP TABLE [type].[InspectionHistorySourceType]
GO
/****** Object:  Table [type].[InspectionDataType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[InspectionDataType]') AND type in (N'U'))
DROP TABLE [type].[InspectionDataType]
GO
/****** Object:  Table [type].[DeviceStatusType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[DeviceStatusType]') AND type in (N'U'))
DROP TABLE [type].[DeviceStatusType]
GO
/****** Object:  Table [type].[DeviceCommandSourceType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[DeviceCommandSourceType]') AND type in (N'U'))
DROP TABLE [type].[DeviceCommandSourceType]
GO
/****** Object:  Table [dbo].[RoleUser]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleUser]') AND type in (N'U'))
DROP TABLE [dbo].[RoleUser]
GO
/****** Object:  Table [dbo].[RoleList]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleList]') AND type in (N'U'))
DROP TABLE [dbo].[RoleList]
GO
/****** Object:  Table [dbo].[InspectionHistory]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InspectionHistory]') AND type in (N'U'))
DROP TABLE [dbo].[InspectionHistory]
GO
/****** Object:  Table [dbo].[InspectionDataHistory]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InspectionDataHistory]') AND type in (N'U'))
DROP TABLE [dbo].[InspectionDataHistory]
GO
/****** Object:  View [mgmt].[v_heavy_tables]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [mgmt].[v_heavy_tables]
GO
/****** Object:  View [dbo].[v_checkin_inspection]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_checkin_inspection]
GO
/****** Object:  Table [dbo].[InspectionData]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InspectionData]') AND type in (N'U'))
DROP TABLE [dbo].[InspectionData]
GO
/****** Object:  View [dbo].[v_work_stations]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_work_stations]
GO
/****** Object:  Table [dbo].[WorkStation]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WorkStation]') AND type in (N'U'))
DROP TABLE [dbo].[WorkStation]
GO
/****** Object:  View [dbo].[v_vehicles]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_vehicles]
GO
/****** Object:  View [dbo].[v_notification_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_notification_types]
GO
/****** Object:  View [dbo].[v_users_list]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_users_list]
GO
/****** Object:  View [dbo].[v_notifications]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_notifications]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Notification]') AND type in (N'U'))
DROP TABLE [dbo].[Notification]
GO
/****** Object:  Table [type].[NotificationType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[NotificationType]') AND type in (N'U'))
DROP TABLE [type].[NotificationType]
GO
/****** Object:  View [dbo].[v_area_lane_device]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_area_lane_device]
GO
/****** Object:  Table [dbo].[Lane]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Lane]') AND type in (N'U'))
DROP TABLE [dbo].[Lane]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Area]') AND type in (N'U'))
DROP TABLE [dbo].[Area]
GO
/****** Object:  Table [type].[AreaType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[AreaType]') AND type in (N'U'))
DROP TABLE [type].[AreaType]
GO
/****** Object:  Table [type].[DeviceType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[DeviceType]') AND type in (N'U'))
DROP TABLE [type].[DeviceType]
GO
/****** Object:  View [dbo].[v_vehicle_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_vehicle_types]
GO
/****** Object:  Table [type].[VehicleType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[VehicleType]') AND type in (N'U'))
DROP TABLE [type].[VehicleType]
GO
/****** Object:  View [dbo].[v_conclusion_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_conclusion_types]
GO
/****** Object:  View [dbo].[v_inspection_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_inspection_types]
GO
/****** Object:  View [dbo].[v_work_station_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_work_station_types]
GO
/****** Object:  Table [type].[WorkStationType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[WorkStationType]') AND type in (N'U'))
DROP TABLE [type].[WorkStationType]
GO
/****** Object:  View [dbo].[v_device_events]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_device_events]
GO
/****** Object:  Table [type].[DeviceEventType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[DeviceEventType]') AND type in (N'U'))
DROP TABLE [type].[DeviceEventType]
GO
/****** Object:  Table [dbo].[DeviceEvent]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceEvent]') AND type in (N'U'))
DROP TABLE [dbo].[DeviceEvent]
GO
/****** Object:  Table [dbo].[Device]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Device]') AND type in (N'U'))
DROP TABLE [dbo].[Device]
GO
/****** Object:  View [dbo].[v_ccr_correction_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_ccr_correction_types]
GO
/****** Object:  View [dbo].[v_lpr_correction_types]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_lpr_correction_types]
GO
/****** Object:  View [dbo].[v_inspections]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_inspections]
GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vehicle]') AND type in (N'U'))
DROP TABLE [dbo].[Vehicle]
GO
/****** Object:  Table [type].[ConclusionType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[ConclusionType]') AND type in (N'U'))
DROP TABLE [type].[ConclusionType]
GO
/****** Object:  Table [type].[InspectionWorkflowType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[InspectionWorkflowType]') AND type in (N'U'))
DROP TABLE [type].[InspectionWorkflowType]
GO
/****** Object:  Table [type].[DataCorrectionType]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[type].[DataCorrectionType]') AND type in (N'U'))
DROP TABLE [type].[DataCorrectionType]
GO
/****** Object:  Table [dbo].[Inspection]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Inspection]') AND type in (N'U'))
DROP TABLE [dbo].[Inspection]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTableColumns]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP FUNCTION [dbo].[fn_GetTableColumns]
GO
/****** Object:  Table [mgmt].[TableDisplayedColumns]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[mgmt].[TableDisplayedColumns]') AND type in (N'U'))
DROP TABLE [mgmt].[TableDisplayedColumns]
GO
/****** Object:  View [dbo].[v_users]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP VIEW [dbo].[v_users]
GO
/****** Object:  Table [auth].[User]    Script Date: 1/23/2022 9:25:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[User]') AND type in (N'U'))
DROP TABLE [auth].[User]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getNextInspectionIdentifier]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP FUNCTION [dbo].[fn_getNextInspectionIdentifier]
GO
/****** Object:  Schema [type]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP SCHEMA [type]
GO
/****** Object:  Schema [mgmt]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP SCHEMA [mgmt]
GO
/****** Object:  Schema [auth]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP SCHEMA [auth]
GO
/****** Object:  User [sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP USER [sdms]
GO
/****** Object:  User [Orenn]    Script Date: 1/23/2022 9:25:00 AM ******/
DROP USER [Orenn]
GO
/****** Object:  User [Orenn]    Script Date: 1/23/2022 9:25:00 AM ******/
CREATE USER [Orenn] FOR LOGIN [TEAM\Orenn] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
CREATE USER [sdms] FOR LOGIN [sdms] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [sdms]
GO
/****** Object:  Schema [auth]    Script Date: 1/23/2022 9:25:00 AM ******/
CREATE SCHEMA [auth]
GO
/****** Object:  Schema [mgmt]    Script Date: 1/23/2022 9:25:00 AM ******/
CREATE SCHEMA [mgmt]
GO
/****** Object:  Schema [type]    Script Date: 1/23/2022 9:25:00 AM ******/
CREATE SCHEMA [type]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getNextInspectionIdentifier]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_getNextInspectionIdentifier] ()
RETURNS varchar(12)
AS
BEGIN
	
	DECLARE @identifier varchar(12) = (
						SELECT [Identifier]
						FROM [dbo].[Inspection]
						WHERE id = (SELECT (MAX(id)) FROM [dbo].[Inspection])
						)

	DECLARE @str_counter varchar(4) = (SELECT SUBSTRING(@identifier, 9, 4))
	DECLARE @LastInspectionDate datetime = (SELECT MAX([created]) FROM [dbo].[Inspection] ) 

	DECLARE @Result varchar(12) = FORMAT(getdate(), 'yyyyMMdd') + '0001'

	IF DAY(GETDATE()) =  DAY(@LastInspectionDate)
		BEGIN
			DECLARE @tmp varchar(4) = CAST(CAST(@str_counter as int) + 1 AS varchar(4))
			SET @Result = FORMAT(getdate(), 'yyyyMMdd') + REPLICATE('0', 4-LEN(@tmp)) + @tmp
		END


	RETURN @Result



END
GO
/****** Object:  Table [auth].[User]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [auth].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_users]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







/*
	Select * from [dbo].[v_users] order by [LastName], [FirstName]
*/

CREATE VIEW [dbo].[v_users]
AS
SELECT top 1000 [id]
      ,[FirstName]
      ,[LastName] 
      ,[Email]
      ,[Password] 
      ,[Created] = FORMAT([created], 'dd/MM/yyyy HH:mm:ss')
	  ,[Updated] = FORMAT([updated], 'dd/MM/yyyy HH:mm:ss')
	  ,[IsDeleted] = [isDeleted]
FROM [auth].[User]
WHERE isDeleted = 0



GO
/****** Object:  Table [mgmt].[TableDisplayedColumns]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mgmt].[TableDisplayedColumns](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[ColumnName] [varchar](50) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[visiable] [bit] NOT NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TableDisplayedColumnsDetails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTableColumns]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetTableColumns](@tbl_name_schema varchar(50))
RETURNS TABLE

/*
	SELECT * FROM [dbo].[fn_GetTableColumns] ('auth.User' )
	SELECT * FROM [dbo].[fn_GetTableColumns] ('dbo.WorkStation' )
*/


AS
RETURN

WITH SRC
AS
(
			SELECT TOP 1000
			UPPER(LEFT(ColumnName,1))+SUBSTRING(ColumnName,2,LEN(ColumnName))  As ColumnName
			,[TableName]
			,[DisplayOrder]

		FROM [mgmt].[TableDisplayedColumns] t
		WHERE [isDeleted] = 0 AND visiable = 1 
		AND t.TableName = @tbl_name_schema
		ORDER BY t.DisplayOrder
)



SELECT DISTINCT '['+ LEFT(Main.Names,Len(Main.Names)-1) + ']' As ColumnNames
FROM
    (
        SELECT DISTINCT src2.ColumnName, 
            (
                SELECT '"' + src1.ColumnName + '",' AS [text()]
                FROM SRC src1
                FOR XML PATH ('')
            ) [Names]
        FROM SRC src2
    ) [Main]

		
		
		
GO
/****** Object:  Table [dbo].[Inspection]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inspection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Identifier] [varchar](12) NOT NULL,
	[VehicleId] [int] NOT NULL,
	[WorkflowTypeId] [int] NULL,
	[LPR] [varchar](20) NULL,
	[LPRCorrectionTypeId] [int] NOT NULL,
	[CCR] [varchar](20) NULL,
	[CCRCorrectionTypeId] [int] NOT NULL,
	[ManifestCode] [varchar](20) NULL,
	[DriverTagCode] [varchar](20) NULL,
	[ConclusionTypeId] [int] NOT NULL,
	[ArrivalTime] [datetime] NOT NULL,
	[LeaveTime] [datetime] NULL,
	[InspectionEndTime] [datetime] NULL,
	[Remark] [varchar](300) NULL,
	[SourceInspectionIdentifier] [varchar](50) NULL,
	[SourceTable] [varchar](50) NULL,
	[SourceId] [varchar](50) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Inspection] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[DataCorrectionType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[DataCorrectionType](
	[id] [int] IDENTITY(0,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_OCRCorrectionType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[InspectionWorkflowType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[InspectionWorkflowType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionWorkflowType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[ConclusionType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[ConclusionType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ConclusionType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LicensePlateNumber] [varchar](20) NOT NULL,
	[VehicleTypeId] [int] NOT NULL,
	[Remark] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_inspections]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






/*
	Select * from [dbo].[v_inspections]
*/

CREATE VIEW [dbo].[v_inspections]
AS
SELECT 
	i.id
	,i.Identifier
	,VehicleLicencePlate = v.LicensePlateNumber
	,i.WorkflowTypeId
	,WorkflowType = wf.[name]
	,i.LPR
	,i.LPRCorrectionTypeId
	,LPRCorrectionType = lpr_cor_t.[name]
	,i.CCR
	,i.CCRCorrectionTypeId
	,CCRCorrectionType = ccr_cor_t.[name]
	,i.ManifestCode
	,i.DriverTagCode
	,i.ConclusionTypeId
	,ConclusionType = ct.[name]
	,Arrival = FORMAT(i.ArrivalTime, 'dd/MM/yyyy HH:mm:ss')
	,Leave = FORMAT(i.LeaveTime, 'dd/MM/yyyy HH:mm:ss')
	,[End] = FORMAT(i.InspectionEndTime, 'dd/MM/yyyy HH:mm:ss')
	,i.Remark
	,i.SourceInspectionIdentifier
	,i.SourceTable
	,i.SourceId
	,[Created] = FORMAT(i.created, 'dd/MM/yyyy HH:mm:ss')
	,[Updated] = FORMAT(i.updated, 'dd/MM/yyyy HH:mm:ss')
	,i.isDeleted
FROM [dbo].[Inspection] i
LEFT JOIN [type].[InspectionWorkflowType] wf ON wf.id = i.WorkflowTypeId
LEFT JOIN [type].[DataCorrectionType] lpr_cor_t ON lpr_cor_t.id = i.LPRCorrectionTypeId
LEFT JOIN [type].[DataCorrectionType] ccr_cor_t ON ccr_cor_t.id = i.CCRCorrectionTypeId
LEFT JOIN [type].[ConclusionType] ct ON ct.id = i.ConclusionTypeId
LEFT JOIN [dbo].[Vehicle] v ON v.id = i.VehicleId
WHERE i.isDeleted = 0
GO
/****** Object:  View [dbo].[v_lpr_correction_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[v_lpr_correction_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
      
FROM [type].[DataCorrectionType]
WHERE [isDeleted] = 0
	AND id in (0,2,3)
GO
/****** Object:  View [dbo].[v_ccr_correction_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_ccr_correction_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
      
FROM [type].[DataCorrectionType]
WHERE [isDeleted] = 0
	AND id in (0,1,2)
GO
/****** Object:  Table [dbo].[Device]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Device](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[Identifier] [varchar](20) NULL,
	[HostIp] [varchar](20) NULL,
	[DirectoryPattern] [varchar](250) NULL,
	[DeviceTypeId] [int] NOT NULL,
	[LaneId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeviceEvent]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceEvent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceId] [int] NOT NULL,
	[DeviceEventTypeId] [int] NOT NULL,
	[Data] [xml] NULL,
	[InspectionId] [int] NULL,
	[Remark] [varchar](20) NULL,
	[SourceInspectionIdentifier] [varchar](50) NULL,
	[SourceTable] [varchar](50) NULL,
	[SourceId] [varchar](50) NULL,
	[timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_DeviceEvent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [type].[DeviceEventType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[DeviceEventType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_DeviceEventType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_device_events]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_device_events]
AS

/*
	SELECT * FROM v_device_events
*/

SELECT de.[id]
      ,[DeviceId]
	  ,d.[Name] As DeviceName
      ,[DeviceEventTypeId]
	  ,det.[name] As DeviceEventType
      ,[Data]
      ,[InspectionId]
      ,[Remark]
      ,[SourceInspectioIdentifier]
      ,[SourceTable]
      ,[SourceId]
      ,[timestamp]
FROM [dbo].[DeviceEvent] de
LEFT JOIN [dbo].[Device] d on d.id = de.DeviceId
LEFT JOIN [type].[DeviceEventType] det on det.id = de.DeviceEventTypeId
GO
/****** Object:  Table [type].[WorkStationType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[WorkStationType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_WorkStationType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_work_station_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	Select * from [dbo].[v_work_station_types]
*/


CREATE VIEW [dbo].[v_work_station_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
FROM [type].[WorkStationType] 
WHERE [isDeleted] = 0
GO
/****** Object:  View [dbo].[v_inspection_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[v_inspection_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
FROM [type].[InspectionWorkflowType] 
WHERE [isDeleted] = 0
GO
/****** Object:  View [dbo].[v_conclusion_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[v_conclusion_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
      
FROM [type].[ConclusionType]
WHERE [isDeleted] = 0
GO
/****** Object:  Table [type].[VehicleType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[VehicleType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_vehicle_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[v_vehicle_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
FROM [type].[VehicleType] 
WHERE [isDeleted] = 0
GO
/****** Object:  Table [type].[DeviceType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[DeviceType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_DeviceType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[AreaType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[AreaType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AreaType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Area](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[AreaTypeId] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lane]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lane](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[AreaId] [int] NOT NULL,
	[MaxQueueQuantity] [int] NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Lane] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_area_lane_device]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_area_lane_device]
AS

/*
	SELECT * FROM dbo.v_area_lane_device v ORDER BY v.AreaId
*/

SELECT 
	a.id As AreaId,
	a.[Name] As AreaName,
	a.[Description] As AreaDescription,
	at.[name] As AreaType,
	at.[description] As AreaTypeDescription,
	l.[Name] As LaneName,
	d.[Name] As DeviveName,
	d.[Description] As DeviceDescription,
	d.[Identifier] As DeviceIdentifier,
	dt.[name] As DeviceType,
	dt.[description] As DeviceTypeDescription
FROM [dbo].[Area] a
INNER JOIN type.AreaType at on at.id = a.AreaTypeId
LEFT JOIN dbo.Lane l on l.AreaId = a.id
LEFT JOIN dbo.Device d ON d.LaneId = l.id
LEFT JOIN type.DeviceType dt ON dt.id = d.DeviceTypeId



GO
/****** Object:  Table [type].[NotificationType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[NotificationType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_NotificationType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[Subject] [varchar](50) NOT NULL,
	[Message] [varchar](250) NULL,
	[JsonData] [varchar](1000) NULL,
	[SourceUserId] [int] NOT NULL,
	[TargetUserId] [int] NOT NULL,
	[Reply] [varchar](250) NULL,
	[IsReplyable] [bit] NOT NULL,
	[IsHandled] [bit] NOT NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_notifications]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






/*
	Select * from [dbo].[v_notification]
*/

CREATE view [dbo].[v_notifications]
AS


SELECT 
	n.[id] As Id
	,n.[NotificationTypeId]
	,nt.[name] As NotificationType
	,nt.[description] As [Subject]
	,n.[Message]
	,n.SourceUserId
	,SourceUserFullName = u_src.LastName + ' ' + u_src.FirstName
	,SourceUserEmail = u_src.Email
	,n.JsonData
	,n.TargetUserId
	,TargetUserFullName = u_dest.LastName + ' ' + u_dest.FirstName
	,TargetUserEmail = u_dest.Email
	,n.Reply
	,n.IsReplyable
	,n.IsHandled
	,[Created] = FORMAT(n.[created], 'dd/MM/yyyy HH:mm:ss')
	,[Updated] = FORMAT(n.[updated], 'dd/MM/yyyy HH:mm:ss')
	,n.[isDeleted] As IsDeleted

FROM [dbo].[Notification] n
INNER JOIN [type].[NotificationType] nt ON nt.id = n.NotificationTypeId
LEFT JOIN auth.[user] u_src on u_src.id = n.SourceUserId
LEFT JOIN auth.[user] u_dest on u_dest.id = n.TargetUserId
WHERE n.isDeleted = 0




GO
/****** Object:  View [dbo].[v_users_list]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_users_list]
AS
SELECT [id]
      ,[FirstName]
      ,[LastName] 
	  ,FullName = LastName + ' ' + FirstName
      ,[Email]
      
FROM [auth].[User]
WHERE isDeleted = 0



GO
/****** Object:  View [dbo].[v_notification_types]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[v_notification_types]
AS

SELECT [id] ,[name] As Name, [description] As Description
FROM [type].[NotificationType] 
WHERE [isDeleted] = 0
GO
/****** Object:  View [dbo].[v_vehicles]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/*
	Select * from [dbo].[v_vehicles]
*/

CREATE view [dbo].[v_vehicles]
AS


SELECT top 8
	v.[id] As Id
	,[LicensePlateNumber]
	,v.[VehicleTypeId]
	,vt.[name] As VehicleType
	,vt.[description] As VehicleTypeDescription
	,[Remark]
	,[Created] = FORMAT(v.[created], 'dd/MM/yyyy HH:mm:ss')
	,[Updated] = FORMAT(v.[updated], 'dd/MM/yyyy HH:mm:ss')
	,v.[isDeleted] As IsDeleted

FROM [dbo].[Vehicle] v
INNER JOIN [type].[VehicleType] vt ON vt.id = v.VehicleTypeId

WHERE v.isDeleted = 0




GO
/****** Object:  Table [dbo].[WorkStation]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkStation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[IP] [varchar](20) NULL,
	[MAC] [varchar](20) NULL,
	[WorkStationTypeId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NULL,
 CONSTRAINT [PK_WorkStation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_work_stations]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*
	Select * from [dbo].[v_work_stations]
*/

CREATE VIEW [dbo].[v_work_stations]
AS

SELECT 
	ws.[id]
	,ws.[Name]
	,ws.[Description]
	,[IP]
	,[MAC]
	,[WorkStationTypeId]
	,st.[name] As WorkStationType
	,st.[description] As [Type Description]
	,ws.IsActive
	,ws.[created] As Created
	,ws.[updated] As Updated
	,ws.[isDeleted] As IsDeleted
FROM [dbo].[WorkStation] ws
INNER JOIN [type].[WorkStationType] st ON st.id = ws.WorkStationTypeId
WHERE ws.isDeleted = 0

GO
/****** Object:  Table [dbo].[InspectionData]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InspectionData](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[InspectionId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[Value] [varchar](250) NOT NULL,
	[Qualifier] [decimal](10, 2) NULL,
	[DataPath] [varchar](250) NULL,
	[Remark] [varchar](300) NULL,
	[SourceInspectionIdentifier] [varchar](50) NULL,
	[SourceTable] [varchar](50) NULL,
	[SourceId] [varchar](50) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_checkin_inspection]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_checkin_inspection]
AS
/*
	SELECT * FROM dbo.v_checkin_inspection WHERE InspectionId = 1
*/

SELECT 
	t.[id] As InspectionId
	,[WorkflowTypeId]
	,[LPR]
	,[CCR]
	,[ManifestCode]
	,[DriverTagCode]
	,t.[Remark]
	,d.DataPath
	,d.DataTypeId
	,d.Qualifier As DataQualifier
  FROM [dbo].[Inspection] t
  INNER JOIN dbo.Vehicle v on v.id = t.VehicleId
  INNER JOIN dbo.InspectionData d on d.InspectionId = t.id


GO
/****** Object:  View [mgmt].[v_heavy_tables]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [mgmt].[v_heavy_tables]
AS

SELECT TOP (10) 
	object_name(id) as TableName,
	reserved/128 as Reserved_Size_MB 
FROM sysindexes 
WHERE indid = 1 ORDER BY reserved DESC
GO
/****** Object:  Table [dbo].[InspectionDataHistory]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InspectionDataHistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[InspectionDataId] [int] NOT NULL,
	[InspectionId] [int] NULL,
	[DataTypeId] [int] NOT NULL,
	[Value] [varchar](50) NOT NULL,
	[Qualifier] [decimal](10, 2) NULL,
	[DataPath] [varchar](250) NULL,
	[Remark] [varchar](300) NULL,
	[UpdaterSourceTypeId] [int] NOT NULL,
	[UpdaterIdentifier] [varchar](20) NULL,
	[created] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionDataHistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InspectionHistory]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InspectionHistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[InspectionId] [int] NOT NULL,
	[VehicleId] [int] NULL,
	[ArrivalTime] [datetime] NULL,
	[WorkflowTypeId] [int] NULL,
	[ConclusionTypeId] [int] NULL,
	[InspectionEndTime] [datetime] NULL,
	[LeaveTime] [datetime] NULL,
	[Remark] [varchar](300) NULL,
	[UpdaterSourceTypeId] [int] NULL,
	[UpdaterIdentifier] [varchar](20) NULL,
	[created] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionHistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleList]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleList](
	[roleName] [varchar](50) NOT NULL,
	[orderId] [numeric](19, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleUser]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleUser](
	[userId] [varchar](20) NOT NULL,
	[roleId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RoleUser] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[DeviceCommandSourceType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[DeviceCommandSourceType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_DeviceCommandSourceType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[DeviceStatusType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[DeviceStatusType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_DeviceStatusType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[InspectionDataType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[InspectionDataType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionDataType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[InspectionHistorySourceType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[InspectionHistorySourceType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionHistorySourceTypeType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [type].[InspectionOperationType]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [type].[InspectionOperationType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](250) NULL,
	[created] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionOperationType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [auth].[User] ON 

INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (1, N'Avinoam', N'Hadad', N'avinoam@eltel.co.il', N'Aa123', CAST(N'2021-12-15T16:40:20.250' AS DateTime), CAST(N'2021-12-23T11:05:32.327' AS DateTime), 0)
INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (2, N'Natalie', N'Pesachove', N'nataliep@eltel.co.il', N'12345', CAST(N'2021-12-15T16:40:20.250' AS DateTime), CAST(N'2021-12-15T16:40:20.250' AS DateTime), 0)
INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (3, N'Benyamin', N'Pesachove', N'beni@gmail.com', N'134', CAST(N'2021-12-15T16:40:20.250' AS DateTime), CAST(N'2021-12-15T16:40:20.250' AS DateTime), 0)
INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (4, N'Oren', N'Nitzan', N'orenn@eltel.co.il', N'Aa123456', CAST(N'2021-12-15T16:40:20.250' AS DateTime), CAST(N'2021-12-15T16:40:20.250' AS DateTime), 0)
INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (6, N'Oren1', N'Nitzan1', N'onn@gmail.com', N'Aa123456', CAST(N'2021-12-20T11:15:21.370' AS DateTime), CAST(N'2021-12-26T14:51:03.990' AS DateTime), 0)
INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (17, N'Eliko', N'Kopter', N'ramon@israel.co.il', N'f16', CAST(N'2022-01-13T15:22:16.870' AS DateTime), CAST(N'2022-01-20T16:47:25.040' AS DateTime), 0)
INSERT [auth].[User] ([id], [FirstName], [LastName], [Email], [Password], [created], [updated], [isDeleted]) VALUES (20, N'חילחי', N'לחי', N'o@g', N'לחי', CAST(N'2022-01-20T16:47:38.737' AS DateTime), CAST(N'2022-01-20T16:47:38.737' AS DateTime), 1)
SET IDENTITY_INSERT [auth].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[Area] ON 

INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (0, N'Default', N'Uses ad default value', 0, CAST(N'2021-12-27T16:39:59.103' AS DateTime), CAST(N'2021-12-27T16:39:59.103' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (2, N'Check In 1', N'Check in gate and cabin where the operator validates inspection data and starts it', 3, CAST(N'2021-12-15T12:18:55.387' AS DateTime), CAST(N'2021-12-15T12:18:55.387' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (4, N'Befor Check In 1', N'The rode tkhat leads to check in where the first OCR is located', 1, CAST(N'2021-12-15T12:20:23.483' AS DateTime), CAST(N'2021-12-15T12:20:23.483' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (6, N'Pre Scan Area 1', N'The parking lot where trucke wait befor x-ray', 7, CAST(N'2021-12-15T12:21:36.557' AS DateTime), CAST(N'2021-12-15T12:21:36.557' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (7, N'X-Ray Scan 1', N'Old X-Ray area', 8, CAST(N'2021-12-15T12:22:24.890' AS DateTime), CAST(N'2021-12-15T12:22:24.890' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (9, N'Pre Clearance 1', N'The parking lot where truck wait after scan while waiting for cunclusion or redirect to manual chek', 6, CAST(N'2021-12-15T12:23:37.143' AS DateTime), CAST(N'2021-12-15T12:23:37.143' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (10, N'Manual Check 1', N'Old manual check area', 5, CAST(N'2021-12-15T12:24:12.213' AS DateTime), CAST(N'2021-12-15T12:24:12.213' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (11, N'Befor Check Out', N'Probably the road to check out where OCR 3 is located', 2, CAST(N'2021-12-15T12:24:59.953' AS DateTime), CAST(N'2021-12-15T12:24:59.953' AS DateTime), 0)
INSERT [dbo].[Area] ([id], [Name], [Description], [AreaTypeId], [created], [updated], [isDeleted]) VALUES (12, N'Check Out 1', N'Check out area', 4, CAST(N'2021-12-15T12:25:28.660' AS DateTime), CAST(N'2021-12-15T12:25:28.660' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Area] OFF
GO
SET IDENTITY_INSERT [dbo].[Device] ON 

INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (1, N'BIA1OCR1', N'OCR1 also called Lane11', N'11', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 4, 3, 1, CAST(N'2021-12-21T17:43:47.773' AS DateTime), CAST(N'2021-12-21T17:43:47.773' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (4, N'BIA3OCR1', N'OCR2 also called Lane12', N'12', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 4, 11, 1, CAST(N'2021-12-21T17:47:52.060' AS DateTime), CAST(N'2021-12-21T17:47:52.060' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (5, N'BOA1OCR1', N'OCR 3 also called Lane13', N'13', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 4, 7, 1, CAST(N'2021-12-21T17:50:54.070' AS DateTime), CAST(N'2021-12-21T17:50:54.070' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (6, N'CIA1LPR1', N'LPR4 also called Lane4', N'4', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 3, 2, 1, CAST(N'2021-12-21T17:54:42.507' AS DateTime), CAST(N'2021-12-21T17:54:42.507' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (7, N'CIA2LPR1', N'LPR3 also called Lane3', N'3', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 3, 10, 1, CAST(N'2021-12-21T17:55:59.123' AS DateTime), CAST(N'2021-12-21T17:55:59.123' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (8, N'COA1LPR1', N'LPR6 also called Lane6', N'6', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 3, 9, 1, CAST(N'2021-12-21T18:10:05.237' AS DateTime), CAST(N'2021-12-21T18:10:05.237' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (9, N'SCA1LPR1', N'LPR5 also called LAne5', N'5', N'127.0.0.1', N'yyyy/mm/dd/index/HHmmss_*', 3, 5, 1, CAST(N'2021-12-21T18:11:05.300' AS DateTime), CAST(N'2021-12-21T18:11:05.300' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (13, N'CIA1SYSWS1', N'Trucks system work station at check in area', N'99', NULL, NULL, 8, 2, 1, CAST(N'2021-12-27T16:02:47.670' AS DateTime), CAST(N'2021-12-27T16:02:47.670' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (14, N'MCA1IAW1', N'X-Ray Image processing system at manual check', N'99', NULL, NULL, 6, 7, 1, CAST(N'2021-12-27T16:32:21.860' AS DateTime), CAST(N'2021-12-27T16:32:21.860' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (15, N'MCA2MCW1', N'X-Ray Image processing system at manual check', N'99', NULL, NULL, 6, 7, 1, CAST(N'2021-12-27T16:33:15.313' AS DateTime), CAST(N'2021-12-27T16:33:15.313' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (17, N'PSA1LED2', N'Pre scan area 1 LED sign Lane 2 (legacy name)', N'99', NULL, NULL, 2, 0, 1, CAST(N'2021-12-27T16:41:33.353' AS DateTime), CAST(N'2021-12-27T16:41:33.353' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (18, N'PSA1LED1', N'Pre scan area 1 LED sign Lane 1 (legacy name)', N'99', NULL, NULL, 2, 0, 1, CAST(N'2021-12-27T16:42:30.250' AS DateTime), CAST(N'2021-12-27T16:42:30.250' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (19, N'MCA1LED1', N'Manual check area LET sign???? Dont know it', N'99', NULL, NULL, 2, 0, 1, CAST(N'2021-12-27T16:44:42.280' AS DateTime), CAST(N'2021-12-27T16:44:42.280' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (22, N'SCA1TagReader1', N'Tag reader at X-Ray operator', N'99', NULL, NULL, 5, 5, 1, CAST(N'2021-12-27T16:46:05.767' AS DateTime), CAST(N'2021-12-27T16:46:05.767' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (23, N'COA1MCW1', N'Dont know', N'99', NULL, NULL, 0, 0, 1, CAST(N'2021-12-27T16:48:19.217' AS DateTime), CAST(N'2021-12-27T16:48:19.217' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (25, N'PCA1LED2', N'Led sign at pre clearance parking lot Lane 2 (legacy name)', N'99', NULL, NULL, 2, 6, 1, CAST(N'2021-12-27T16:51:05.447' AS DateTime), CAST(N'2021-12-27T16:51:05.447' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (26, N'COA1Printer1', N'Dont know!! Printer at checkout area 1 Lane 1 (legacy name).', N'99', NULL, NULL, 9, 9, 1, CAST(N'2021-12-27T16:53:46.037' AS DateTime), CAST(N'2021-12-27T16:53:46.037' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (27, N'MCA1TagReader1', N'Dont know!! Manual check tag reaer work station 1', N'99', NULL, NULL, 5, 7, 1, CAST(N'2021-12-27T17:01:31.277' AS DateTime), CAST(N'2021-12-27T17:01:31.277' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (28, N'MCA2TagReader1', N'Dont know!! Manual check tag reaer work station 1', N'99', NULL, NULL, 5, 7, 1, CAST(N'2021-12-27T17:01:56.197' AS DateTime), CAST(N'2021-12-27T17:01:56.197' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (30, N'PCA1IAW1', N'Don know', N'99', NULL, NULL, 7, 9, 1, CAST(N'2021-12-27T17:04:28.640' AS DateTime), CAST(N'2021-12-27T17:04:28.640' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (31, N'SCA1NII1', N'Lane 1 X-Ray device ', N'99', NULL, NULL, 6, 5, 1, CAST(N'2021-12-27T17:05:46.310' AS DateTime), CAST(N'2021-12-27T17:05:46.310' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (32, N'SCA1SYSWS1', N'IAW- Scan area 1 trucks work station', N'99', NULL, NULL, 8, 5, 1, CAST(N'2021-12-27T17:12:23.837' AS DateTime), CAST(N'2021-12-27T17:12:23.837' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (33, N'COA1Barrier1', N'Check out barrier', N'99', NULL, NULL, 1, 9, 1, CAST(N'2021-12-27T17:13:35.760' AS DateTime), CAST(N'2021-12-27T17:13:35.760' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (34, N'MCA1MCW1', N'Dont', N'99', NULL, NULL, 1, 9, 1, CAST(N'2021-12-27T17:26:56.513' AS DateTime), CAST(N'2021-12-27T17:26:56.513' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (35, N'COA1IAW1', N'Dont', N'99', NULL, NULL, 1, 9, 1, CAST(N'2021-12-27T17:27:17.570' AS DateTime), CAST(N'2021-12-27T17:27:17.570' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (36, N'COA1TagReader1', N'Check out tag reader', N'99', NULL, NULL, 5, 8, 1, CAST(N'2021-12-27T17:28:00.767' AS DateTime), CAST(N'2021-12-27T17:28:00.767' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (38, N'MCA1LED2', N'Manual check area LET sign???? Dont know it', N'99', NULL, NULL, 2, 0, 1, CAST(N'2021-12-27T17:28:41.613' AS DateTime), CAST(N'2021-12-27T17:28:41.613' AS DateTime), 0)
INSERT [dbo].[Device] ([id], [Name], [Description], [Identifier], [HostIp], [DirectoryPattern], [DeviceTypeId], [LaneId], [IsActive], [created], [updated], [isDeleted]) VALUES (39, N'PCA1LED1', N'Led sign at pre clearance parking lot Lane 1 (legacy name)', N'99', NULL, NULL, 2, 6, 1, CAST(N'2021-12-27T17:29:27.797' AS DateTime), CAST(N'2021-12-27T17:29:27.797' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Device] OFF
GO
SET IDENTITY_INSERT [dbo].[DeviceEvent] ON 

INSERT [dbo].[DeviceEvent] ([id], [DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp]) VALUES (1, 13, 4, N'<properties><comment>storeToXML</comment><entry key="ccr">MCLU5092803</entry><entry key="sId">258277</entry><entry key="bypass">false</entry><entry key="lprId">753067</entry><entry key="bypassUserId" /><entry key="remark" /><entry key="inspectType">RD</entry><entry key="originalLpn">7380952</entry><entry key="ocrId">514627</entry><entry key="userId">aviki</entry><entry key="lpn">7380952</entry><entry key="manifestNo">3093183</entry><entry key="manifestPic">/CIA1/2021/12/22/20211222120723_0001N.jpg</entry></properties>', 1, N'Imported', N'202112220064', N'EventHistory', N'4993172', CAST(N'2021-12-22T12:01:11.410' AS DateTime))
INSERT [dbo].[DeviceEvent] ([id], [DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp]) VALUES (2, 9, 27, N'<properties><comment>storeToXML</comment><entry key="lprCredibility">99</entry><entry key="licensePlate">7380952</entry><entry key="lprPhotoPathCsv">/SCA1LPR1/2021/12/22/122636/LPR5Cam2_22121521779_1_Recognition_7380952.jpg</entry></properties>', 1, N'Imported', N'202112220064', N'EventHistory', N'4993323', CAST(N'2021-12-22T12:26:36.017' AS DateTime))
INSERT [dbo].[DeviceEvent] ([id], [DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp]) VALUES (3, 8, 27, N'<properties><comment>storeToXML</comment><entry key="lprCredibility">96</entry><entry key="licensePlate">7380952</entry><entry key="lprPhotoPathCsv">/COA1LPR1/2021/12/22/123446/Lpr66Cam_22122332061_1_Recognition_7380952.jpg</entry></properties>', 1, N'Imported', N'202112220064', N'EventHistory', N'4993373', CAST(N'2021-12-22T12:34:46.903' AS DateTime))
INSERT [dbo].[DeviceEvent] ([id], [DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp]) VALUES (4, 1, 25, N'<properties><comment>storeToXML</comment><entry key="ccrPhotoPathCsv">/BIA1OCR1/2021/12/22/115254/145034_ContTop_f.jpg,/BIA1OCR1/2021/12/22/115254/145034_ContLeftUpper_f.jpg</entry><entry key="lprCredibility">82</entry><entry key="licensePlate">7380952</entry><entry key="containerNumberCsv">MCLU5092803</entry><entry key="lprPhotoPathCsv">/BIA1OCR1/2021/12/22/115254/145034_CarLeftUpper.jpg</entry><entry key="ccrCredibility">70</entry></properties>', NULL, N'Imported', NULL, N'Event', N'4993111', CAST(N'2021-12-22T11:52:54.847' AS DateTime))
INSERT [dbo].[DeviceEvent] ([id], [DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp]) VALUES (5, 6, 27, N'<properties><comment>storeToXML</comment><entry key="lprCredibility">99</entry><entry key="licensePlate">7380952</entry><entry key="lprPhotoPathCsv">/CIA1LPR1/2021/12/22/120011/LPR4Cam2_22114851751_1_Recognition_7380952.jpg</entry></properties>', NULL, N'Imported', NULL, N'Event', N'4993167', CAST(N'2021-12-22T12:00:11.333' AS DateTime))
INSERT [dbo].[DeviceEvent] ([id], [DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp]) VALUES (6, 5, 25, N'<properties><comment>storeToXML</comment><entry key="ccrPhotoPathCsv">/BOA1OCR1/2021/12/22/123429/231637_ContRightUpper_f.jpg,/BOA1OCR1/2021/12/22/123429/231637_ContBack_f.jpg,/BOA1OCR1/2021/12/22/123429/231637_ContTop_f.jpg</entry><entry key="lprCredibility">87</entry><entry key="licensePlate">7380952</entry><entry key="containerNumberCsv">MCLU5092803</entry><entry key="lprPhotoPathCsv">/BOA1OCR1/2021/12/22/123429/231637_CarRightUpper.jpg</entry><entry key="ccrCredibility">100</entry></properties>', NULL, N'Imported', NULL, N'Event', N'4993370', CAST(N'2021-12-22T12:34:29.960' AS DateTime))
SET IDENTITY_INSERT [dbo].[DeviceEvent] OFF
GO
SET IDENTITY_INSERT [dbo].[Inspection] ON 

INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (1, N'202201110064', 560, 2, N'1122335544', 2, N'ASDF123456', 2, N'ABC1234GH', N'202201110064', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'Test', N'202112220064', N'IsraelVehicleHistory', N'202112220064', CAST(N'2021-12-30T10:19:59.557' AS DateTime), CAST(N'2022-01-20T16:35:20.233' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (2, N'202201110065', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T15:59:13.590' AS DateTime), CAST(N'2022-01-11T15:59:13.590' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (3, N'202201110001', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T15:59:55.013' AS DateTime), CAST(N'2022-01-11T15:59:55.013' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (4, N'202201110001', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T15:59:55.013' AS DateTime), CAST(N'2022-01-11T15:59:55.013' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (5, N'202201110002', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:01:19.153' AS DateTime), CAST(N'2022-01-11T16:01:19.153' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (6, N'202201110002', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:01:19.153' AS DateTime), CAST(N'2022-01-11T16:01:19.153' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (7, N'202201110002', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:01:19.153' AS DateTime), CAST(N'2022-01-11T16:01:19.153' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (8, N'202201110002', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:01:19.153' AS DateTime), CAST(N'2022-01-11T16:01:19.153' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (9, N'202201110003', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:01:55.743' AS DateTime), CAST(N'2022-01-11T16:01:55.743' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (10, N'202201110004', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:02:03.237' AS DateTime), CAST(N'2022-01-11T16:02:03.237' AS DateTime), 0)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (11, N'202201110005', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:02:21.273' AS DateTime), CAST(N'2022-01-11T16:02:21.273' AS DateTime), 1)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (12, N'202201110006', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:02:22.120' AS DateTime), CAST(N'2022-01-11T16:02:22.120' AS DateTime), 1)
INSERT [dbo].[Inspection] ([id], [Identifier], [VehicleId], [WorkflowTypeId], [LPR], [LPRCorrectionTypeId], [CCR], [CCRCorrectionTypeId], [ManifestCode], [DriverTagCode], [ConclusionTypeId], [ArrivalTime], [LeaveTime], [InspectionEndTime], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (13, N'202201110007', 560, 2, N'7380952', 0, N'MCLU5092803', 0, N'3093183', N'2021122200640', 2, CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:34:46.903' AS DateTime), CAST(N'2021-12-22T12:33:21.100' AS DateTime), N'', NULL, NULL, NULL, CAST(N'2022-01-11T16:02:22.613' AS DateTime), CAST(N'2022-01-11T16:02:22.613' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Inspection] OFF
GO
SET IDENTITY_INSERT [dbo].[InspectionData] ON 

INSERT [dbo].[InspectionData] ([id], [InspectionId], [DataTypeId], [Value], [Qualifier], [DataPath], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (2, 1, 1, N'1234567', CAST(80.00 AS Decimal(10, 2)), N'/BIA1OCR1/2021/12/22/115254/145034_CarLeftUpper.jpg', N'Imported', N'202112220064', N'OCRHistory', N'514627', CAST(N'2021-12-22T11:52:54.847' AS DateTime), CAST(N'2021-12-22T11:52:54.847' AS DateTime), 1)
INSERT [dbo].[InspectionData] ([id], [InspectionId], [DataTypeId], [Value], [Qualifier], [DataPath], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (3, 1, 2, N'MCLU5092803', CAST(70.00 AS Decimal(10, 2)), N'/BIA1OCR1/2021/12/22/115254/145034_ContLeftUpper_f.jpg', N'Imported', N'202112220064', N'OCRHistory', N'514627', CAST(N'2021-12-22T11:52:54.847' AS DateTime), CAST(N'2021-12-22T11:52:54.847' AS DateTime), 1)
INSERT [dbo].[InspectionData] ([id], [InspectionId], [DataTypeId], [Value], [Qualifier], [DataPath], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [created], [updated], [isDeleted]) VALUES (4, 1, 2, N'MCLU5092803', CAST(70.00 AS Decimal(10, 2)), N'/BIA1OCR1/2021/12/22/115254/145034_ContTop_f.jpg', N'Imported', N'202112220064', N'OCRHistory', N'514627', CAST(N'2021-12-22T11:52:54.847' AS DateTime), CAST(N'2021-12-22T11:52:54.847' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[InspectionData] OFF
GO
SET IDENTITY_INSERT [dbo].[InspectionDataHistory] ON 

INSERT [dbo].[InspectionDataHistory] ([id], [InspectionDataId], [InspectionId], [DataTypeId], [Value], [Qualifier], [DataPath], [Remark], [UpdaterSourceTypeId], [UpdaterIdentifier], [created], [isDeleted]) VALUES (2, 2, 1, 1, N'MCLU5092803', CAST(70.00 AS Decimal(10, 2)), N'/BIA1OCR1/2021/12/22/115254/145034_CarLeftUpper.jpg', N'Imported', 2, NULL, CAST(N'2021-12-22T11:52:54.847' AS DateTime), 1)
INSERT [dbo].[InspectionDataHistory] ([id], [InspectionDataId], [InspectionId], [DataTypeId], [Value], [Qualifier], [DataPath], [Remark], [UpdaterSourceTypeId], [UpdaterIdentifier], [created], [isDeleted]) VALUES (3, 3, 1, 2, N'MCLU5092803', CAST(70.00 AS Decimal(10, 2)), N'/BIA1OCR1/2021/12/22/115254/145034_ContLeftUpper_f.jpg', N'Imported', 2, NULL, CAST(N'2021-12-22T11:52:54.847' AS DateTime), 1)
INSERT [dbo].[InspectionDataHistory] ([id], [InspectionDataId], [InspectionId], [DataTypeId], [Value], [Qualifier], [DataPath], [Remark], [UpdaterSourceTypeId], [UpdaterIdentifier], [created], [isDeleted]) VALUES (4, 4, 1, 2, N'MCLU5092803', CAST(70.00 AS Decimal(10, 2)), N'/BIA1OCR1/2021/12/22/115254/145034_ContTop_f.jpg', N'Imported', 2, NULL, CAST(N'2021-12-22T11:52:54.847' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[InspectionDataHistory] OFF
GO
SET IDENTITY_INSERT [dbo].[Lane] ON 

INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (0, N'Default', N'Used as default value', 0, NULL, CAST(N'2021-12-27T16:40:31.390' AS DateTime), CAST(N'2021-12-27T16:40:31.390' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (2, N'Check In Lane 1', N'Check in 1', 2, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (3, N'Befor Check In Lane 1', N'OCR 1', 4, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (4, N'Pre Scan Lane 1', N'The old parking lot', 6, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (5, N'X-Ray Lane 1', N'X-Ray lane 1', 7, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (6, N'Pre Clearance Lane 1', N'The old parking lot', 9, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (7, N'MCA Lane 1', N'Lane 1', 10, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (8, N'BCO Lane 1', N'OCR 3', 11, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (9, N'COA 1 Lane 1', NULL, 12, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (10, N'Check In Lane 2', N'Check In 2', 2, NULL, CAST(N'2021-12-15T12:36:25.843' AS DateTime), CAST(N'2021-12-15T12:36:25.843' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (11, N'Befor Check In Lane 2', N'OCR 2', 4, NULL, CAST(N'2021-12-15T12:36:44.060' AS DateTime), CAST(N'2021-12-15T12:36:44.060' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (12, N'MCA Lane 2', N'Lane 2', 7, NULL, CAST(N'2021-12-15T13:02:08.437' AS DateTime), CAST(N'2021-12-15T13:02:08.437' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (13, N'MCA Lane 3', N'Lane 3', 7, NULL, CAST(N'2021-12-15T13:02:08.440' AS DateTime), CAST(N'2021-12-15T13:02:08.440' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (14, N'MCA Lane 4', N'Lane 4', 7, NULL, CAST(N'2021-12-15T13:02:08.440' AS DateTime), CAST(N'2021-12-15T13:02:08.440' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (15, N'MCA Lane 5', N'Lane 5', 7, NULL, CAST(N'2021-12-15T13:02:08.443' AS DateTime), CAST(N'2021-12-15T13:02:08.443' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (16, N'MCA Lane 6', N'Lane 6', 7, NULL, CAST(N'2021-12-15T13:02:08.443' AS DateTime), CAST(N'2021-12-15T13:02:08.443' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (17, N'MCA Lane 7', N'Lane 7', 7, NULL, CAST(N'2021-12-15T13:02:08.447' AS DateTime), CAST(N'2021-12-15T13:02:08.447' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (18, N'MCA Lane 8', N'Lane 8', 7, NULL, CAST(N'2021-12-15T13:02:08.450' AS DateTime), CAST(N'2021-12-15T13:02:08.450' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (19, N'MCA Lane 9', N'Lane 9', 7, NULL, CAST(N'2021-12-15T13:02:08.450' AS DateTime), CAST(N'2021-12-15T13:02:08.450' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (20, N'MCA Lane 10', N'Lane 10', 7, NULL, CAST(N'2021-12-15T13:02:08.450' AS DateTime), CAST(N'2021-12-15T13:02:08.450' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (21, N'MCA Lane 11', NULL, 7, NULL, CAST(N'2021-12-15T13:56:28.470' AS DateTime), CAST(N'2021-12-15T13:56:28.470' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (22, N'MCA Lane 12', NULL, 7, NULL, CAST(N'2021-12-15T13:56:28.473' AS DateTime), CAST(N'2021-12-15T13:56:28.473' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (23, N'MCA Lane 13', NULL, 7, NULL, CAST(N'2021-12-15T13:56:28.473' AS DateTime), CAST(N'2021-12-15T13:56:28.473' AS DateTime), 0)
INSERT [dbo].[Lane] ([id], [Name], [Description], [AreaId], [MaxQueueQuantity], [created], [updated], [isDeleted]) VALUES (24, N'Befor Check In Lane 2', N'OCR 2', 4, NULL, CAST(N'2021-12-15T12:34:20.427' AS DateTime), CAST(N'2021-12-15T12:34:20.427' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Lane] OFF
GO
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:32:54.720' AS DateTime), CAST(N'2022-01-19T16:32:54.720' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (2, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:33:54.297' AS DateTime), CAST(N'2022-01-19T16:33:54.297' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (3, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:38:27.610' AS DateTime), CAST(N'2022-01-19T16:38:27.610' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (4, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:38:32.910' AS DateTime), CAST(N'2022-01-19T16:38:32.910' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (5, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:38:34.110' AS DateTime), CAST(N'2022-01-19T16:38:34.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (6, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:38:34.533' AS DateTime), CAST(N'2022-01-19T16:38:34.533' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (7, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-19T16:38:35.000' AS DateTime), CAST(N'2022-01-19T16:38:35.000' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (8, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:10:24.600' AS DateTime), CAST(N'2022-01-20T12:10:24.600' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (9, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:10:32.970' AS DateTime), CAST(N'2022-01-20T12:10:32.970' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (10, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:10:35.447' AS DateTime), CAST(N'2022-01-20T12:10:35.447' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (11, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:11:01.657' AS DateTime), CAST(N'2022-01-20T12:11:01.657' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (12, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:11:20.477' AS DateTime), CAST(N'2022-01-20T12:11:20.477' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (13, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:12:54.433' AS DateTime), CAST(N'2022-01-20T12:12:54.433' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (14, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:13:19.413' AS DateTime), CAST(N'2022-01-20T12:13:19.413' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (15, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T12:13:53.140' AS DateTime), CAST(N'2022-01-20T12:13:53.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (16, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T13:23:54.027' AS DateTime), CAST(N'2022-01-20T13:23:54.027' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (17, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T13:24:06.417' AS DateTime), CAST(N'2022-01-20T13:24:06.417' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (18, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T13:25:22.360' AS DateTime), CAST(N'2022-01-20T13:25:22.360' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (19, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T13:25:25.810' AS DateTime), CAST(N'2022-01-20T13:25:25.810' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (20, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T13:30:47.630' AS DateTime), CAST(N'2022-01-20T13:30:47.630' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (21, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "1/1/0001 12:00:00 AM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T13:36:53.747' AS DateTime), CAST(N'2022-01-20T13:36:53.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (22, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:38.910' AS DateTime), CAST(N'2022-01-20T14:15:38.910' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (23, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:50.450' AS DateTime), CAST(N'2022-01-20T14:15:50.450' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (24, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:52.183' AS DateTime), CAST(N'2022-01-20T14:15:52.183' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (25, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:53.403' AS DateTime), CAST(N'2022-01-20T14:15:53.403' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (26, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:54.223' AS DateTime), CAST(N'2022-01-20T14:15:54.223' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (27, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:54.957' AS DateTime), CAST(N'2022-01-20T14:15:54.957' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (28, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:55.140' AS DateTime), CAST(N'2022-01-20T14:15:55.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (29, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:55.767' AS DateTime), CAST(N'2022-01-20T14:15:55.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (30, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:56.453' AS DateTime), CAST(N'2022-01-20T14:15:56.453' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (31, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:57.103' AS DateTime), CAST(N'2022-01-20T14:15:57.103' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (32, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:57.300' AS DateTime), CAST(N'2022-01-20T14:15:57.300' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (33, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:57.487' AS DateTime), CAST(N'2022-01-20T14:15:57.487' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (34, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:57.660' AS DateTime), CAST(N'2022-01-20T14:15:57.660' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (35, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:57.850' AS DateTime), CAST(N'2022-01-20T14:15:57.850' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (36, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:58.040' AS DateTime), CAST(N'2022-01-20T14:15:58.040' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (37, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:58.220' AS DateTime), CAST(N'2022-01-20T14:15:58.220' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (38, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:58.410' AS DateTime), CAST(N'2022-01-20T14:15:58.410' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (39, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:58.590' AS DateTime), CAST(N'2022-01-20T14:15:58.590' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (40, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:58.750' AS DateTime), CAST(N'2022-01-20T14:15:58.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (41, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:15:58.937' AS DateTime), CAST(N'2022-01-20T14:15:58.937' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (42, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:00.710' AS DateTime), CAST(N'2022-01-20T14:16:00.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (43, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:00.980' AS DateTime), CAST(N'2022-01-20T14:16:00.980' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (44, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:01.147' AS DateTime), CAST(N'2022-01-20T14:16:01.147' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (45, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:01.330' AS DateTime), CAST(N'2022-01-20T14:16:01.330' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (46, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:01.520' AS DateTime), CAST(N'2022-01-20T14:16:01.520' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (47, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:01.710' AS DateTime), CAST(N'2022-01-20T14:16:01.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (48, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:01.877' AS DateTime), CAST(N'2022-01-20T14:16:01.877' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (49, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:02.073' AS DateTime), CAST(N'2022-01-20T14:16:02.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (50, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:02.243' AS DateTime), CAST(N'2022-01-20T14:16:02.243' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (51, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:02.433' AS DateTime), CAST(N'2022-01-20T14:16:02.433' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (52, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:02.610' AS DateTime), CAST(N'2022-01-20T14:16:02.610' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (53, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:02.787' AS DateTime), CAST(N'2022-01-20T14:16:02.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (54, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:02.987' AS DateTime), CAST(N'2022-01-20T14:16:02.987' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (55, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:03.187' AS DateTime), CAST(N'2022-01-20T14:16:03.187' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (56, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:03.340' AS DateTime), CAST(N'2022-01-20T14:16:03.340' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (57, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:03.530' AS DateTime), CAST(N'2022-01-20T14:16:03.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (58, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:03.710' AS DateTime), CAST(N'2022-01-20T14:16:03.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (59, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:03.957' AS DateTime), CAST(N'2022-01-20T14:16:03.957' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (60, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:04.130' AS DateTime), CAST(N'2022-01-20T14:16:04.130' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (61, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:04.310' AS DateTime), CAST(N'2022-01-20T14:16:04.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (62, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:04.460' AS DateTime), CAST(N'2022-01-20T14:16:04.460' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (63, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:04.660' AS DateTime), CAST(N'2022-01-20T14:16:04.660' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (64, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:04.840' AS DateTime), CAST(N'2022-01-20T14:16:04.840' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (65, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:05.040' AS DateTime), CAST(N'2022-01-20T14:16:05.040' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (66, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:05.217' AS DateTime), CAST(N'2022-01-20T14:16:05.217' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (67, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:05.397' AS DateTime), CAST(N'2022-01-20T14:16:05.397' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (68, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:16:05.563' AS DateTime), CAST(N'2022-01-20T14:16:05.563' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (69, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:35:05.170' AS DateTime), CAST(N'2022-01-20T14:35:05.170' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (70, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:50:46.537' AS DateTime), CAST(N'2022-01-20T14:50:46.537' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (71, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:50:52.507' AS DateTime), CAST(N'2022-01-20T14:50:52.507' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (72, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:51:43.217' AS DateTime), CAST(N'2022-01-20T14:51:43.217' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (73, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:51:56.587' AS DateTime), CAST(N'2022-01-20T14:51:56.587' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (74, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:52:22.670' AS DateTime), CAST(N'2022-01-20T14:52:22.670' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (75, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:54:34.517' AS DateTime), CAST(N'2022-01-20T14:54:34.517' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (76, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T14:57:31.463' AS DateTime), CAST(N'2022-01-20T14:57:31.463' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (77, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:14.757' AS DateTime), CAST(N'2022-01-20T15:23:14.757' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (78, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:27.013' AS DateTime), CAST(N'2022-01-20T15:23:27.013' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (79, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:30.357' AS DateTime), CAST(N'2022-01-20T15:23:30.357' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (80, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:30.940' AS DateTime), CAST(N'2022-01-20T15:23:30.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (81, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:31.120' AS DateTime), CAST(N'2022-01-20T15:23:31.120' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (82, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:31.293' AS DateTime), CAST(N'2022-01-20T15:23:31.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (83, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:31.467' AS DateTime), CAST(N'2022-01-20T15:23:31.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (84, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:31.610' AS DateTime), CAST(N'2022-01-20T15:23:31.610' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (85, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:31.950' AS DateTime), CAST(N'2022-01-20T15:23:31.950' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (86, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:32.140' AS DateTime), CAST(N'2022-01-20T15:23:32.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (87, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:32.293' AS DateTime), CAST(N'2022-01-20T15:23:32.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (88, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:44.003' AS DateTime), CAST(N'2022-01-20T15:23:44.003' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (89, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:55.777' AS DateTime), CAST(N'2022-01-20T15:23:55.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (90, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:23:57.810' AS DateTime), CAST(N'2022-01-20T15:23:57.810' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (91, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:17.493' AS DateTime), CAST(N'2022-01-20T15:39:17.493' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (92, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:35.513' AS DateTime), CAST(N'2022-01-20T15:39:35.513' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (93, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:39.823' AS DateTime), CAST(N'2022-01-20T15:39:39.823' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (94, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:43.370' AS DateTime), CAST(N'2022-01-20T15:39:43.370' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (95, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:50.937' AS DateTime), CAST(N'2022-01-20T15:39:50.937' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (96, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:56.090' AS DateTime), CAST(N'2022-01-20T15:39:56.090' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (97, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:39:59.963' AS DateTime), CAST(N'2022-01-20T15:39:59.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (98, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:01.780' AS DateTime), CAST(N'2022-01-20T15:40:01.780' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (99, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:02.007' AS DateTime), CAST(N'2022-01-20T15:40:02.007' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (100, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:03.097' AS DateTime), CAST(N'2022-01-20T15:40:03.097' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (101, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:07.753' AS DateTime), CAST(N'2022-01-20T15:40:07.753' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (102, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:09.650' AS DateTime), CAST(N'2022-01-20T15:40:09.650' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (103, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:24.530' AS DateTime), CAST(N'2022-01-20T15:40:24.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (104, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:26.287' AS DateTime), CAST(N'2022-01-20T15:40:26.287' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (105, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:28.307' AS DateTime), CAST(N'2022-01-20T15:40:28.307' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (106, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:30.440' AS DateTime), CAST(N'2022-01-20T15:40:30.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (107, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:32.860' AS DateTime), CAST(N'2022-01-20T15:40:32.860' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (108, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:40:36.123' AS DateTime), CAST(N'2022-01-20T15:40:36.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (109, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:45:52.710' AS DateTime), CAST(N'2022-01-20T15:45:52.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (110, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:45:55.250' AS DateTime), CAST(N'2022-01-20T15:45:55.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (111, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:45:57.620' AS DateTime), CAST(N'2022-01-20T15:45:57.620' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (112, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:45:58.287' AS DateTime), CAST(N'2022-01-20T15:45:58.287' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (113, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:45:58.460' AS DateTime), CAST(N'2022-01-20T15:45:58.460' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (114, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:01.227' AS DateTime), CAST(N'2022-01-20T15:46:01.227' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (115, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:02.407' AS DateTime), CAST(N'2022-01-20T15:46:02.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (116, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:03.793' AS DateTime), CAST(N'2022-01-20T15:46:03.793' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (117, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:04.407' AS DateTime), CAST(N'2022-01-20T15:46:04.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (118, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:04.590' AS DateTime), CAST(N'2022-01-20T15:46:04.590' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (119, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:04.777' AS DateTime), CAST(N'2022-01-20T15:46:04.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (120, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:04.970' AS DateTime), CAST(N'2022-01-20T15:46:04.970' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (121, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:05.160' AS DateTime), CAST(N'2022-01-20T15:46:05.160' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (122, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:46:05.310' AS DateTime), CAST(N'2022-01-20T15:46:05.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (123, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:49:42.887' AS DateTime), CAST(N'2022-01-20T15:49:42.887' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (124, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:49:44.153' AS DateTime), CAST(N'2022-01-20T15:49:44.153' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (125, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:49:45.290' AS DateTime), CAST(N'2022-01-20T15:49:45.290' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (126, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T15:49:45.547' AS DateTime), CAST(N'2022-01-20T15:49:45.547' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (127, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:33:53.700' AS DateTime), CAST(N'2022-01-20T16:33:53.700' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (128, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:33:55.413' AS DateTime), CAST(N'2022-01-20T16:33:55.413' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (129, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:33:56.813' AS DateTime), CAST(N'2022-01-20T16:33:56.813' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (130, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:33:57.540' AS DateTime), CAST(N'2022-01-20T16:33:57.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (131, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:33:57.717' AS DateTime), CAST(N'2022-01-20T16:33:57.717' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (132, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:00.030' AS DateTime), CAST(N'2022-01-20T16:34:00.030' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (133, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:02.777' AS DateTime), CAST(N'2022-01-20T16:34:02.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (134, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:03.423' AS DateTime), CAST(N'2022-01-20T16:34:03.423' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (135, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:03.610' AS DateTime), CAST(N'2022-01-20T16:34:03.610' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (136, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:03.780' AS DateTime), CAST(N'2022-01-20T16:34:03.780' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (137, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:03.943' AS DateTime), CAST(N'2022-01-20T16:34:03.943' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (138, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:10.620' AS DateTime), CAST(N'2022-01-20T16:34:10.620' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (139, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:13.450' AS DateTime), CAST(N'2022-01-20T16:34:13.450' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (140, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:15.403' AS DateTime), CAST(N'2022-01-20T16:34:15.403' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (141, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:19.573' AS DateTime), CAST(N'2022-01-20T16:34:19.573' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (142, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:20.170' AS DateTime), CAST(N'2022-01-20T16:34:20.170' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (143, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:20.360' AS DateTime), CAST(N'2022-01-20T16:34:20.360' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (144, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:20.530' AS DateTime), CAST(N'2022-01-20T16:34:20.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (145, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:20.723' AS DateTime), CAST(N'2022-01-20T16:34:20.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (146, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:20.880' AS DateTime), CAST(N'2022-01-20T16:34:20.880' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (147, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:21.060' AS DateTime), CAST(N'2022-01-20T16:34:21.060' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (148, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:21.213' AS DateTime), CAST(N'2022-01-20T16:34:21.213' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (149, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:21.380' AS DateTime), CAST(N'2022-01-20T16:34:21.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (150, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:21.553' AS DateTime), CAST(N'2022-01-20T16:34:21.553' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (151, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:39.637' AS DateTime), CAST(N'2022-01-20T16:34:39.637' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (152, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:42.677' AS DateTime), CAST(N'2022-01-20T16:34:42.677' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (153, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:34:59.730' AS DateTime), CAST(N'2022-01-20T16:34:59.730' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (154, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:01.073' AS DateTime), CAST(N'2022-01-20T16:35:01.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (155, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:04.107' AS DateTime), CAST(N'2022-01-20T16:35:04.107' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (156, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:05.123' AS DateTime), CAST(N'2022-01-20T16:35:05.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (157, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:05.290' AS DateTime), CAST(N'2022-01-20T16:35:05.290' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (158, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:05.467' AS DateTime), CAST(N'2022-01-20T16:35:05.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (159, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:05.640' AS DateTime), CAST(N'2022-01-20T16:35:05.640' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (160, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:05.817' AS DateTime), CAST(N'2022-01-20T16:35:05.817' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (161, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:05.947' AS DateTime), CAST(N'2022-01-20T16:35:05.947' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (162, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:24.790' AS DateTime), CAST(N'2022-01-20T16:35:24.790' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (163, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:28.160' AS DateTime), CAST(N'2022-01-20T16:35:28.160' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (164, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:35:29.663' AS DateTime), CAST(N'2022-01-20T16:35:29.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (165, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:07.903' AS DateTime), CAST(N'2022-01-20T16:42:07.903' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (166, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:12.447' AS DateTime), CAST(N'2022-01-20T16:42:12.447' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (167, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:13.230' AS DateTime), CAST(N'2022-01-20T16:42:13.230' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (168, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:13.443' AS DateTime), CAST(N'2022-01-20T16:42:13.443' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (169, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:21.907' AS DateTime), CAST(N'2022-01-20T16:42:21.907' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (170, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:25.863' AS DateTime), CAST(N'2022-01-20T16:42:25.863' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (171, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:26.623' AS DateTime), CAST(N'2022-01-20T16:42:26.623' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (172, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:26.830' AS DateTime), CAST(N'2022-01-20T16:42:26.830' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (173, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:26.980' AS DateTime), CAST(N'2022-01-20T16:42:26.980' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (174, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:29.893' AS DateTime), CAST(N'2022-01-20T16:42:29.893' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (175, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:32.357' AS DateTime), CAST(N'2022-01-20T16:42:32.357' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (176, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:32.530' AS DateTime), CAST(N'2022-01-20T16:42:32.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (177, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:33.953' AS DateTime), CAST(N'2022-01-20T16:42:33.953' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (178, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:34.500' AS DateTime), CAST(N'2022-01-20T16:42:34.500' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (179, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:34.713' AS DateTime), CAST(N'2022-01-20T16:42:34.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (180, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:34.897' AS DateTime), CAST(N'2022-01-20T16:42:34.897' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (181, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:35.077' AS DateTime), CAST(N'2022-01-20T16:42:35.077' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (182, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:35.253' AS DateTime), CAST(N'2022-01-20T16:42:35.253' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (183, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:35.437' AS DateTime), CAST(N'2022-01-20T16:42:35.437' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (184, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:35.620' AS DateTime), CAST(N'2022-01-20T16:42:35.620' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (185, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:35.800' AS DateTime), CAST(N'2022-01-20T16:42:35.800' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (186, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:35.970' AS DateTime), CAST(N'2022-01-20T16:42:35.970' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (187, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:36.140' AS DateTime), CAST(N'2022-01-20T16:42:36.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (188, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:36.890' AS DateTime), CAST(N'2022-01-20T16:42:36.890' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (189, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:37.103' AS DateTime), CAST(N'2022-01-20T16:42:37.103' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (190, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:37.303' AS DateTime), CAST(N'2022-01-20T16:42:37.303' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (191, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:37.663' AS DateTime), CAST(N'2022-01-20T16:42:37.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (192, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:37.860' AS DateTime), CAST(N'2022-01-20T16:42:37.860' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (193, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:38.023' AS DateTime), CAST(N'2022-01-20T16:42:38.023' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (194, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:46.090' AS DateTime), CAST(N'2022-01-20T16:42:46.090' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (195, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:49.887' AS DateTime), CAST(N'2022-01-20T16:42:49.887' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (196, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:51.023' AS DateTime), CAST(N'2022-01-20T16:42:51.023' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (197, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:52.637' AS DateTime), CAST(N'2022-01-20T16:42:52.637' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (198, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:53.837' AS DateTime), CAST(N'2022-01-20T16:42:53.837' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (199, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:54.040' AS DateTime), CAST(N'2022-01-20T16:42:54.040' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (200, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:54.223' AS DateTime), CAST(N'2022-01-20T16:42:54.223' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (201, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:54.417' AS DateTime), CAST(N'2022-01-20T16:42:54.417' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (202, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:54.750' AS DateTime), CAST(N'2022-01-20T16:42:54.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (203, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:54.967' AS DateTime), CAST(N'2022-01-20T16:42:54.967' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (204, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:55.150' AS DateTime), CAST(N'2022-01-20T16:42:55.150' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (205, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:55.350' AS DateTime), CAST(N'2022-01-20T16:42:55.350' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (206, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:55.543' AS DateTime), CAST(N'2022-01-20T16:42:55.543' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (207, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:42:55.723' AS DateTime), CAST(N'2022-01-20T16:42:55.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (208, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:19.993' AS DateTime), CAST(N'2022-01-20T16:43:19.993' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (209, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:21.203' AS DateTime), CAST(N'2022-01-20T16:43:21.203' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (210, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:21.367' AS DateTime), CAST(N'2022-01-20T16:43:21.367' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (211, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:21.560' AS DateTime), CAST(N'2022-01-20T16:43:21.560' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (212, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:21.750' AS DateTime), CAST(N'2022-01-20T16:43:21.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (213, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:22.217' AS DateTime), CAST(N'2022-01-20T16:43:22.217' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (214, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:22.397' AS DateTime), CAST(N'2022-01-20T16:43:22.397' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (215, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:22.580' AS DateTime), CAST(N'2022-01-20T16:43:22.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (216, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:22.750' AS DateTime), CAST(N'2022-01-20T16:43:22.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (217, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:23.120' AS DateTime), CAST(N'2022-01-20T16:43:23.120' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (218, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:23.333' AS DateTime), CAST(N'2022-01-20T16:43:23.333' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (219, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:23.513' AS DateTime), CAST(N'2022-01-20T16:43:23.513' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (220, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:23.693' AS DateTime), CAST(N'2022-01-20T16:43:23.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (221, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:23.960' AS DateTime), CAST(N'2022-01-20T16:43:23.960' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (222, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:24.160' AS DateTime), CAST(N'2022-01-20T16:43:24.160' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (223, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:24.353' AS DateTime), CAST(N'2022-01-20T16:43:24.353' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (224, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:24.537' AS DateTime), CAST(N'2022-01-20T16:43:24.537' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (225, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:25.133' AS DateTime), CAST(N'2022-01-20T16:43:25.133' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (226, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:25.347' AS DateTime), CAST(N'2022-01-20T16:43:25.347' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (227, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:25.547' AS DateTime), CAST(N'2022-01-20T16:43:25.547' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (228, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:25.730' AS DateTime), CAST(N'2022-01-20T16:43:25.730' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (229, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:25.920' AS DateTime), CAST(N'2022-01-20T16:43:25.920' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (230, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:27.007' AS DateTime), CAST(N'2022-01-20T16:43:27.007' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (231, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:28.217' AS DateTime), CAST(N'2022-01-20T16:43:28.217' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (232, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:29.423' AS DateTime), CAST(N'2022-01-20T16:43:29.423' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (233, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:32.440' AS DateTime), CAST(N'2022-01-20T16:43:32.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (234, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:39.357' AS DateTime), CAST(N'2022-01-20T16:43:39.357' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (235, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:40.713' AS DateTime), CAST(N'2022-01-20T16:43:40.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (236, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-20T16:43:41.817' AS DateTime), CAST(N'2022-01-20T16:43:41.817' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (237, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:12.767' AS DateTime), CAST(N'2022-01-22T20:31:12.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (238, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:33.940' AS DateTime), CAST(N'2022-01-22T20:31:33.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (239, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:40.540' AS DateTime), CAST(N'2022-01-22T20:31:40.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (240, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:42.380' AS DateTime), CAST(N'2022-01-22T20:31:42.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (241, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:46.527' AS DateTime), CAST(N'2022-01-22T20:31:46.527' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (242, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:50.813' AS DateTime), CAST(N'2022-01-22T20:31:50.813' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (243, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:52.160' AS DateTime), CAST(N'2022-01-22T20:31:52.160' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (244, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:31:58.670' AS DateTime), CAST(N'2022-01-22T20:31:58.670' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (245, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:32:26.270' AS DateTime), CAST(N'2022-01-22T20:32:26.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (246, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:32:49.620' AS DateTime), CAST(N'2022-01-22T20:32:49.620' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (247, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:32:55.460' AS DateTime), CAST(N'2022-01-22T20:32:55.460' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (248, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:05.647' AS DateTime), CAST(N'2022-01-22T20:33:05.647' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (249, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:07.893' AS DateTime), CAST(N'2022-01-22T20:33:07.893' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (250, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:08.877' AS DateTime), CAST(N'2022-01-22T20:33:08.877' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (251, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:09.540' AS DateTime), CAST(N'2022-01-22T20:33:09.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (252, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:10.543' AS DateTime), CAST(N'2022-01-22T20:33:10.543' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (253, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:11.220' AS DateTime), CAST(N'2022-01-22T20:33:11.220' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (254, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:12.407' AS DateTime), CAST(N'2022-01-22T20:33:12.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (255, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:13.277' AS DateTime), CAST(N'2022-01-22T20:33:13.277' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (256, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:14.400' AS DateTime), CAST(N'2022-01-22T20:33:14.400' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (257, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:15.140' AS DateTime), CAST(N'2022-01-22T20:33:15.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (258, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:15.540' AS DateTime), CAST(N'2022-01-22T20:33:15.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (259, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:15.767' AS DateTime), CAST(N'2022-01-22T20:33:15.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (260, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:15.957' AS DateTime), CAST(N'2022-01-22T20:33:15.957' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (261, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:16.133' AS DateTime), CAST(N'2022-01-22T20:33:16.133' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (262, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:16.323' AS DateTime), CAST(N'2022-01-22T20:33:16.323' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (263, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:16.460' AS DateTime), CAST(N'2022-01-22T20:33:16.460' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (264, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:16.653' AS DateTime), CAST(N'2022-01-22T20:33:16.653' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (265, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:18.110' AS DateTime), CAST(N'2022-01-22T20:33:18.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (266, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:18.290' AS DateTime), CAST(N'2022-01-22T20:33:18.290' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (267, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:18.470' AS DateTime), CAST(N'2022-01-22T20:33:18.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (268, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:18.657' AS DateTime), CAST(N'2022-01-22T20:33:18.657' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (269, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:18.830' AS DateTime), CAST(N'2022-01-22T20:33:18.830' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (270, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:18.993' AS DateTime), CAST(N'2022-01-22T20:33:18.993' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (271, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:19.857' AS DateTime), CAST(N'2022-01-22T20:33:19.857' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (272, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:20.040' AS DateTime), CAST(N'2022-01-22T20:33:20.040' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (273, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:20.237' AS DateTime), CAST(N'2022-01-22T20:33:20.237' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (274, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:20.423' AS DateTime), CAST(N'2022-01-22T20:33:20.423' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (275, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:20.613' AS DateTime), CAST(N'2022-01-22T20:33:20.613' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (276, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:20.780' AS DateTime), CAST(N'2022-01-22T20:33:20.780' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (277, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:20.963' AS DateTime), CAST(N'2022-01-22T20:33:20.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (278, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:21.170' AS DateTime), CAST(N'2022-01-22T20:33:21.170' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (279, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:21.397' AS DateTime), CAST(N'2022-01-22T20:33:21.397' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (280, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:21.610' AS DateTime), CAST(N'2022-01-22T20:33:21.610' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (281, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:21.787' AS DateTime), CAST(N'2022-01-22T20:33:21.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (282, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:21.963' AS DateTime), CAST(N'2022-01-22T20:33:21.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (283, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:22.150' AS DateTime), CAST(N'2022-01-22T20:33:22.150' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (284, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:22.343' AS DateTime), CAST(N'2022-01-22T20:33:22.343' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (285, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:22.540' AS DateTime), CAST(N'2022-01-22T20:33:22.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (286, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:22.723' AS DateTime), CAST(N'2022-01-22T20:33:22.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (287, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:46.897' AS DateTime), CAST(N'2022-01-22T20:33:46.897' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (288, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:47.910' AS DateTime), CAST(N'2022-01-22T20:33:47.910' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (289, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:48.960' AS DateTime), CAST(N'2022-01-22T20:33:48.960' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (290, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:49.287' AS DateTime), CAST(N'2022-01-22T20:33:49.287' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (291, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:49.543' AS DateTime), CAST(N'2022-01-22T20:33:49.543' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (292, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:51.390' AS DateTime), CAST(N'2022-01-22T20:33:51.390' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (293, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:51.563' AS DateTime), CAST(N'2022-01-22T20:33:51.563' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (294, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:51.747' AS DateTime), CAST(N'2022-01-22T20:33:51.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (295, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:56.557' AS DateTime), CAST(N'2022-01-22T20:33:56.557' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (296, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:56.730' AS DateTime), CAST(N'2022-01-22T20:33:56.730' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (297, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:56.917' AS DateTime), CAST(N'2022-01-22T20:33:56.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (298, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:33:57.097' AS DateTime), CAST(N'2022-01-22T20:33:57.097' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (299, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:36.077' AS DateTime), CAST(N'2022-01-22T20:35:36.077' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (300, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:37.173' AS DateTime), CAST(N'2022-01-22T20:35:37.173' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (301, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:38.063' AS DateTime), CAST(N'2022-01-22T20:35:38.063' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (302, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:38.763' AS DateTime), CAST(N'2022-01-22T20:35:38.763' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (303, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:41.910' AS DateTime), CAST(N'2022-01-22T20:35:41.910' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (304, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:42.723' AS DateTime), CAST(N'2022-01-22T20:35:42.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (305, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:42.987' AS DateTime), CAST(N'2022-01-22T20:35:42.987' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (306, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:43.197' AS DateTime), CAST(N'2022-01-22T20:35:43.197' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (307, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:44.357' AS DateTime), CAST(N'2022-01-22T20:35:44.357' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (308, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:44.533' AS DateTime), CAST(N'2022-01-22T20:35:44.533' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (309, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:44.713' AS DateTime), CAST(N'2022-01-22T20:35:44.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (310, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:44.917' AS DateTime), CAST(N'2022-01-22T20:35:44.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (311, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:45.080' AS DateTime), CAST(N'2022-01-22T20:35:45.080' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (312, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:46.240' AS DateTime), CAST(N'2022-01-22T20:35:46.240' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (313, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:46.423' AS DateTime), CAST(N'2022-01-22T20:35:46.423' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (314, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:46.590' AS DateTime), CAST(N'2022-01-22T20:35:46.590' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (315, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:46.790' AS DateTime), CAST(N'2022-01-22T20:35:46.790' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (316, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:46.957' AS DateTime), CAST(N'2022-01-22T20:35:46.957' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (317, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:57.533' AS DateTime), CAST(N'2022-01-22T20:35:57.533' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (318, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:58.340' AS DateTime), CAST(N'2022-01-22T20:35:58.340' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (319, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:58.543' AS DateTime), CAST(N'2022-01-22T20:35:58.543' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (320, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:58.733' AS DateTime), CAST(N'2022-01-22T20:35:58.733' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (321, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:58.923' AS DateTime), CAST(N'2022-01-22T20:35:58.923' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (322, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:59.110' AS DateTime), CAST(N'2022-01-22T20:35:59.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (323, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:59.297' AS DateTime), CAST(N'2022-01-22T20:35:59.297' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (324, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:59.487' AS DateTime), CAST(N'2022-01-22T20:35:59.487' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (325, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:59.693' AS DateTime), CAST(N'2022-01-22T20:35:59.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (326, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:35:59.877' AS DateTime), CAST(N'2022-01-22T20:35:59.877' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (327, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:00.073' AS DateTime), CAST(N'2022-01-22T20:36:00.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (328, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:00.270' AS DateTime), CAST(N'2022-01-22T20:36:00.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (329, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:00.450' AS DateTime), CAST(N'2022-01-22T20:36:00.450' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (330, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:00.653' AS DateTime), CAST(N'2022-01-22T20:36:00.653' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (331, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:00.860' AS DateTime), CAST(N'2022-01-22T20:36:00.860' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (332, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:01.057' AS DateTime), CAST(N'2022-01-22T20:36:01.057' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (333, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:01.260' AS DateTime), CAST(N'2022-01-22T20:36:01.260' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (334, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:01.470' AS DateTime), CAST(N'2022-01-22T20:36:01.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (335, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:01.650' AS DateTime), CAST(N'2022-01-22T20:36:01.650' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (336, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:01.857' AS DateTime), CAST(N'2022-01-22T20:36:01.857' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (337, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:02.063' AS DateTime), CAST(N'2022-01-22T20:36:02.063' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (338, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:02.257' AS DateTime), CAST(N'2022-01-22T20:36:02.257' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (339, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:02.440' AS DateTime), CAST(N'2022-01-22T20:36:02.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (340, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:02.653' AS DateTime), CAST(N'2022-01-22T20:36:02.653' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (341, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:02.877' AS DateTime), CAST(N'2022-01-22T20:36:02.877' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (342, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:03.073' AS DateTime), CAST(N'2022-01-22T20:36:03.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (343, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:03.263' AS DateTime), CAST(N'2022-01-22T20:36:03.263' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (344, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:03.443' AS DateTime), CAST(N'2022-01-22T20:36:03.443' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (345, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:03.640' AS DateTime), CAST(N'2022-01-22T20:36:03.640' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (346, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:03.833' AS DateTime), CAST(N'2022-01-22T20:36:03.833' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (347, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:04.027' AS DateTime), CAST(N'2022-01-22T20:36:04.027' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (348, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:04.223' AS DateTime), CAST(N'2022-01-22T20:36:04.223' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (349, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:04.427' AS DateTime), CAST(N'2022-01-22T20:36:04.427' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (350, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:04.640' AS DateTime), CAST(N'2022-01-22T20:36:04.640' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (351, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:04.823' AS DateTime), CAST(N'2022-01-22T20:36:04.823' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (352, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:05.020' AS DateTime), CAST(N'2022-01-22T20:36:05.020' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (353, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:05.233' AS DateTime), CAST(N'2022-01-22T20:36:05.233' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (354, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:05.440' AS DateTime), CAST(N'2022-01-22T20:36:05.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (355, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:05.637' AS DateTime), CAST(N'2022-01-22T20:36:05.637' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (356, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:05.840' AS DateTime), CAST(N'2022-01-22T20:36:05.840' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (357, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:06.033' AS DateTime), CAST(N'2022-01-22T20:36:06.033' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (358, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:06.233' AS DateTime), CAST(N'2022-01-22T20:36:06.233' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (359, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:06.420' AS DateTime), CAST(N'2022-01-22T20:36:06.420' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (360, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:06.630' AS DateTime), CAST(N'2022-01-22T20:36:06.630' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (361, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:06.810' AS DateTime), CAST(N'2022-01-22T20:36:06.810' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (362, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:07.013' AS DateTime), CAST(N'2022-01-22T20:36:07.013' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (363, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:07.197' AS DateTime), CAST(N'2022-01-22T20:36:07.197' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (364, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:07.387' AS DateTime), CAST(N'2022-01-22T20:36:07.387' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (365, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:07.583' AS DateTime), CAST(N'2022-01-22T20:36:07.583' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (366, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:08.240' AS DateTime), CAST(N'2022-01-22T20:36:08.240' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (367, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:08.440' AS DateTime), CAST(N'2022-01-22T20:36:08.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (368, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:08.627' AS DateTime), CAST(N'2022-01-22T20:36:08.627' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (369, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:08.783' AS DateTime), CAST(N'2022-01-22T20:36:08.783' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (370, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:09.313' AS DateTime), CAST(N'2022-01-22T20:36:09.313' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (371, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:09.517' AS DateTime), CAST(N'2022-01-22T20:36:09.517' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (372, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:09.700' AS DateTime), CAST(N'2022-01-22T20:36:09.700' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (373, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:09.873' AS DateTime), CAST(N'2022-01-22T20:36:09.873' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (374, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:18.993' AS DateTime), CAST(N'2022-01-22T20:36:18.993' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (375, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:19.203' AS DateTime), CAST(N'2022-01-22T20:36:19.203' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (376, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:19.380' AS DateTime), CAST(N'2022-01-22T20:36:19.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (377, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:19.567' AS DateTime), CAST(N'2022-01-22T20:36:19.567' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (378, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:19.750' AS DateTime), CAST(N'2022-01-22T20:36:19.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (379, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:19.933' AS DateTime), CAST(N'2022-01-22T20:36:19.933' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (380, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:20.117' AS DateTime), CAST(N'2022-01-22T20:36:20.117' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (381, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:20.273' AS DateTime), CAST(N'2022-01-22T20:36:20.273' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (382, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:20.463' AS DateTime), CAST(N'2022-01-22T20:36:20.463' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (383, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:24.570' AS DateTime), CAST(N'2022-01-22T20:36:24.570' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (384, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:24.767' AS DateTime), CAST(N'2022-01-22T20:36:24.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (385, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:33.973' AS DateTime), CAST(N'2022-01-22T20:36:33.973' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (386, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:34.180' AS DateTime), CAST(N'2022-01-22T20:36:34.180' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (387, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:34.377' AS DateTime), CAST(N'2022-01-22T20:36:34.377' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (388, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:34.577' AS DateTime), CAST(N'2022-01-22T20:36:34.577' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (389, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:34.763' AS DateTime), CAST(N'2022-01-22T20:36:34.763' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (390, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:34.940' AS DateTime), CAST(N'2022-01-22T20:36:34.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (391, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:35.130' AS DateTime), CAST(N'2022-01-22T20:36:35.130' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (392, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:35.327' AS DateTime), CAST(N'2022-01-22T20:36:35.327' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (393, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:35.513' AS DateTime), CAST(N'2022-01-22T20:36:35.513' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (394, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:35.713' AS DateTime), CAST(N'2022-01-22T20:36:35.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (395, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:35.913' AS DateTime), CAST(N'2022-01-22T20:36:35.913' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (396, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:36.107' AS DateTime), CAST(N'2022-01-22T20:36:36.107' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (397, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:36.310' AS DateTime), CAST(N'2022-01-22T20:36:36.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (398, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:36.490' AS DateTime), CAST(N'2022-01-22T20:36:36.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (399, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:36.690' AS DateTime), CAST(N'2022-01-22T20:36:36.690' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (400, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:36.880' AS DateTime), CAST(N'2022-01-22T20:36:36.880' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (401, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:37.073' AS DateTime), CAST(N'2022-01-22T20:36:37.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (402, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:37.270' AS DateTime), CAST(N'2022-01-22T20:36:37.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (403, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:37.473' AS DateTime), CAST(N'2022-01-22T20:36:37.473' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (404, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:37.670' AS DateTime), CAST(N'2022-01-22T20:36:37.670' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (405, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:37.887' AS DateTime), CAST(N'2022-01-22T20:36:37.887' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (406, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:38.057' AS DateTime), CAST(N'2022-01-22T20:36:38.057' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (407, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:38.273' AS DateTime), CAST(N'2022-01-22T20:36:38.273' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (408, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:38.457' AS DateTime), CAST(N'2022-01-22T20:36:38.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (409, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:38.697' AS DateTime), CAST(N'2022-01-22T20:36:38.697' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (410, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:38.877' AS DateTime), CAST(N'2022-01-22T20:36:38.877' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (411, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:39.067' AS DateTime), CAST(N'2022-01-22T20:36:39.067' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (412, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:39.260' AS DateTime), CAST(N'2022-01-22T20:36:39.260' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (413, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:39.457' AS DateTime), CAST(N'2022-01-22T20:36:39.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (414, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:39.660' AS DateTime), CAST(N'2022-01-22T20:36:39.660' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (415, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:39.857' AS DateTime), CAST(N'2022-01-22T20:36:39.857' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (416, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:40.047' AS DateTime), CAST(N'2022-01-22T20:36:40.047' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (417, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:40.250' AS DateTime), CAST(N'2022-01-22T20:36:40.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (418, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:40.457' AS DateTime), CAST(N'2022-01-22T20:36:40.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (419, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:40.673' AS DateTime), CAST(N'2022-01-22T20:36:40.673' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (420, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:40.853' AS DateTime), CAST(N'2022-01-22T20:36:40.853' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (421, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:41.040' AS DateTime), CAST(N'2022-01-22T20:36:41.040' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (422, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:41.243' AS DateTime), CAST(N'2022-01-22T20:36:41.243' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (423, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:41.457' AS DateTime), CAST(N'2022-01-22T20:36:41.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (424, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:41.643' AS DateTime), CAST(N'2022-01-22T20:36:41.643' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (425, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:41.837' AS DateTime), CAST(N'2022-01-22T20:36:41.837' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (426, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:42.030' AS DateTime), CAST(N'2022-01-22T20:36:42.030' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (427, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:42.220' AS DateTime), CAST(N'2022-01-22T20:36:42.220' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (428, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:42.430' AS DateTime), CAST(N'2022-01-22T20:36:42.430' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (429, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:42.643' AS DateTime), CAST(N'2022-01-22T20:36:42.643' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (430, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:42.827' AS DateTime), CAST(N'2022-01-22T20:36:42.827' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (431, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:43.030' AS DateTime), CAST(N'2022-01-22T20:36:43.030' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (432, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:43.213' AS DateTime), CAST(N'2022-01-22T20:36:43.213' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (433, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:43.407' AS DateTime), CAST(N'2022-01-22T20:36:43.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (434, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:43.593' AS DateTime), CAST(N'2022-01-22T20:36:43.593' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (435, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:43.790' AS DateTime), CAST(N'2022-01-22T20:36:43.790' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (436, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:43.977' AS DateTime), CAST(N'2022-01-22T20:36:43.977' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (437, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:44.183' AS DateTime), CAST(N'2022-01-22T20:36:44.183' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (438, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:44.370' AS DateTime), CAST(N'2022-01-22T20:36:44.370' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (439, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:44.560' AS DateTime), CAST(N'2022-01-22T20:36:44.560' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (440, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:44.747' AS DateTime), CAST(N'2022-01-22T20:36:44.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (441, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:44.940' AS DateTime), CAST(N'2022-01-22T20:36:44.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (442, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:45.130' AS DateTime), CAST(N'2022-01-22T20:36:45.130' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (443, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:45.307' AS DateTime), CAST(N'2022-01-22T20:36:45.307' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (444, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:45.517' AS DateTime), CAST(N'2022-01-22T20:36:45.517' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (445, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:45.757' AS DateTime), CAST(N'2022-01-22T20:36:45.757' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (446, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:45.957' AS DateTime), CAST(N'2022-01-22T20:36:45.957' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (447, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:46.183' AS DateTime), CAST(N'2022-01-22T20:36:46.183' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (448, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:46.390' AS DateTime), CAST(N'2022-01-22T20:36:46.390' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (449, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:46.593' AS DateTime), CAST(N'2022-01-22T20:36:46.593' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (450, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:46.777' AS DateTime), CAST(N'2022-01-22T20:36:46.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (451, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:46.957' AS DateTime), CAST(N'2022-01-22T20:36:46.957' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (452, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:47.150' AS DateTime), CAST(N'2022-01-22T20:36:47.150' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (453, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:47.317' AS DateTime), CAST(N'2022-01-22T20:36:47.317' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (454, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:47.523' AS DateTime), CAST(N'2022-01-22T20:36:47.523' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (455, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:47.693' AS DateTime), CAST(N'2022-01-22T20:36:47.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (456, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:47.890' AS DateTime), CAST(N'2022-01-22T20:36:47.890' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (457, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:48.247' AS DateTime), CAST(N'2022-01-22T20:36:48.247' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (458, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:48.440' AS DateTime), CAST(N'2022-01-22T20:36:48.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (459, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:48.640' AS DateTime), CAST(N'2022-01-22T20:36:48.640' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (460, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:48.850' AS DateTime), CAST(N'2022-01-22T20:36:48.850' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (461, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:49.053' AS DateTime), CAST(N'2022-01-22T20:36:49.053' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (462, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:49.243' AS DateTime), CAST(N'2022-01-22T20:36:49.243' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (463, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:49.450' AS DateTime), CAST(N'2022-01-22T20:36:49.450' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (464, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:49.663' AS DateTime), CAST(N'2022-01-22T20:36:49.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (465, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:49.860' AS DateTime), CAST(N'2022-01-22T20:36:49.860' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (466, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:50.063' AS DateTime), CAST(N'2022-01-22T20:36:50.063' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (467, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:50.277' AS DateTime), CAST(N'2022-01-22T20:36:50.277' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (468, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:50.523' AS DateTime), CAST(N'2022-01-22T20:36:50.523' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (469, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:50.740' AS DateTime), CAST(N'2022-01-22T20:36:50.740' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (470, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:50.930' AS DateTime), CAST(N'2022-01-22T20:36:50.930' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (471, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:51.123' AS DateTime), CAST(N'2022-01-22T20:36:51.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (472, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:51.337' AS DateTime), CAST(N'2022-01-22T20:36:51.337' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (473, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:51.570' AS DateTime), CAST(N'2022-01-22T20:36:51.570' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (474, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:51.787' AS DateTime), CAST(N'2022-01-22T20:36:51.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (475, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:52.007' AS DateTime), CAST(N'2022-01-22T20:36:52.007' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (476, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:52.240' AS DateTime), CAST(N'2022-01-22T20:36:52.240' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (477, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:52.470' AS DateTime), CAST(N'2022-01-22T20:36:52.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (478, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:52.667' AS DateTime), CAST(N'2022-01-22T20:36:52.667' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (479, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:52.887' AS DateTime), CAST(N'2022-01-22T20:36:52.887' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (480, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:53.103' AS DateTime), CAST(N'2022-01-22T20:36:53.103' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (481, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:53.310' AS DateTime), CAST(N'2022-01-22T20:36:53.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (482, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:53.510' AS DateTime), CAST(N'2022-01-22T20:36:53.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (483, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:53.713' AS DateTime), CAST(N'2022-01-22T20:36:53.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (484, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:53.910' AS DateTime), CAST(N'2022-01-22T20:36:53.910' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (485, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:54.100' AS DateTime), CAST(N'2022-01-22T20:36:54.100' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (486, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:54.297' AS DateTime), CAST(N'2022-01-22T20:36:54.297' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (487, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:54.510' AS DateTime), CAST(N'2022-01-22T20:36:54.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (488, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:55.573' AS DateTime), CAST(N'2022-01-22T20:36:55.573' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (489, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:55.770' AS DateTime), CAST(N'2022-01-22T20:36:55.770' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (490, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:56.000' AS DateTime), CAST(N'2022-01-22T20:36:56.000' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (491, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:56.157' AS DateTime), CAST(N'2022-01-22T20:36:56.157' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (492, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:56.347' AS DateTime), CAST(N'2022-01-22T20:36:56.347' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (493, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:56.570' AS DateTime), CAST(N'2022-01-22T20:36:56.570' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (494, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T20:36:56.773' AS DateTime), CAST(N'2022-01-22T20:36:56.773' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (495, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:05:20.750' AS DateTime), CAST(N'2022-01-22T21:05:20.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (496, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:07:58.873' AS DateTime), CAST(N'2022-01-22T21:07:58.873' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (497, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:35:21.490' AS DateTime), CAST(N'2022-01-22T21:35:21.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (498, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:36:28.803' AS DateTime), CAST(N'2022-01-22T21:36:28.803' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (499, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:37:25.467' AS DateTime), CAST(N'2022-01-22T21:37:25.467' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (500, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:44:43.330' AS DateTime), CAST(N'2022-01-22T21:44:43.330' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (501, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:16.340' AS DateTime), CAST(N'2022-01-22T21:47:16.340' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (502, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:17.270' AS DateTime), CAST(N'2022-01-22T21:47:17.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (503, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:18.110' AS DateTime), CAST(N'2022-01-22T21:47:18.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (504, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:18.853' AS DateTime), CAST(N'2022-01-22T21:47:18.853' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (505, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:19.613' AS DateTime), CAST(N'2022-01-22T21:47:19.613' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (506, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:20.157' AS DateTime), CAST(N'2022-01-22T21:47:20.157' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (507, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:20.580' AS DateTime), CAST(N'2022-01-22T21:47:20.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (508, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:20.787' AS DateTime), CAST(N'2022-01-22T21:47:20.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (509, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:20.997' AS DateTime), CAST(N'2022-01-22T21:47:20.997' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (510, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:21.187' AS DateTime), CAST(N'2022-01-22T21:47:21.187' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (511, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:21.363' AS DateTime), CAST(N'2022-01-22T21:47:21.363' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (512, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:21.530' AS DateTime), CAST(N'2022-01-22T21:47:21.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (513, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:21.710' AS DateTime), CAST(N'2022-01-22T21:47:21.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (514, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:21.883' AS DateTime), CAST(N'2022-01-22T21:47:21.883' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (515, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:22.073' AS DateTime), CAST(N'2022-01-22T21:47:22.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (516, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:22.240' AS DateTime), CAST(N'2022-01-22T21:47:22.240' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (517, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:22.423' AS DateTime), CAST(N'2022-01-22T21:47:22.423' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (518, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:22.580' AS DateTime), CAST(N'2022-01-22T21:47:22.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (519, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:22.757' AS DateTime), CAST(N'2022-01-22T21:47:22.757' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (520, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:22.937' AS DateTime), CAST(N'2022-01-22T21:47:22.937' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (521, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:23.113' AS DateTime), CAST(N'2022-01-22T21:47:23.113' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (522, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:23.297' AS DateTime), CAST(N'2022-01-22T21:47:23.297' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (523, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:23.477' AS DateTime), CAST(N'2022-01-22T21:47:23.477' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (524, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:23.687' AS DateTime), CAST(N'2022-01-22T21:47:23.687' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (525, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:23.870' AS DateTime), CAST(N'2022-01-22T21:47:23.870' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (526, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:24.047' AS DateTime), CAST(N'2022-01-22T21:47:24.047' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (527, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:47:24.220' AS DateTime), CAST(N'2022-01-22T21:47:24.220' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (528, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:44.557' AS DateTime), CAST(N'2022-01-22T21:57:44.557' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (529, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:45.257' AS DateTime), CAST(N'2022-01-22T21:57:45.257' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (530, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:45.500' AS DateTime), CAST(N'2022-01-22T21:57:45.500' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (531, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:45.723' AS DateTime), CAST(N'2022-01-22T21:57:45.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (532, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:45.943' AS DateTime), CAST(N'2022-01-22T21:57:45.943' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (533, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:46.140' AS DateTime), CAST(N'2022-01-22T21:57:46.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (534, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:47.453' AS DateTime), CAST(N'2022-01-22T21:57:47.453' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (535, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:47.677' AS DateTime), CAST(N'2022-01-22T21:57:47.677' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (536, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:47.857' AS DateTime), CAST(N'2022-01-22T21:57:47.857' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (537, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:48.043' AS DateTime), CAST(N'2022-01-22T21:57:48.043' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (538, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:56.470' AS DateTime), CAST(N'2022-01-22T21:57:56.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (539, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:57:59.030' AS DateTime), CAST(N'2022-01-22T21:57:59.030' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (540, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:00.020' AS DateTime), CAST(N'2022-01-22T21:58:00.020' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (541, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:00.307' AS DateTime), CAST(N'2022-01-22T21:58:00.307' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (542, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:00.480' AS DateTime), CAST(N'2022-01-22T21:58:00.480' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (543, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:00.660' AS DateTime), CAST(N'2022-01-22T21:58:00.660' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (544, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:03.097' AS DateTime), CAST(N'2022-01-22T21:58:03.097' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (545, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:11.197' AS DateTime), CAST(N'2022-01-22T21:58:11.197' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (546, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:15.310' AS DateTime), CAST(N'2022-01-22T21:58:15.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (547, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:17.400' AS DateTime), CAST(N'2022-01-22T21:58:17.400' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (548, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:17.613' AS DateTime), CAST(N'2022-01-22T21:58:17.613' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (549, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:17.783' AS DateTime), CAST(N'2022-01-22T21:58:17.783' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (550, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:17.967' AS DateTime), CAST(N'2022-01-22T21:58:17.967' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (551, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:18.160' AS DateTime), CAST(N'2022-01-22T21:58:18.160' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (552, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:18.340' AS DateTime), CAST(N'2022-01-22T21:58:18.340' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (553, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:19.467' AS DateTime), CAST(N'2022-01-22T21:58:19.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (554, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:19.650' AS DateTime), CAST(N'2022-01-22T21:58:19.650' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (555, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:19.833' AS DateTime), CAST(N'2022-01-22T21:58:19.833' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (556, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:20.017' AS DateTime), CAST(N'2022-01-22T21:58:20.017' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (557, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:20.210' AS DateTime), CAST(N'2022-01-22T21:58:20.210' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (558, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:20.387' AS DateTime), CAST(N'2022-01-22T21:58:20.387' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (559, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:20.580' AS DateTime), CAST(N'2022-01-22T21:58:20.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (560, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:20.763' AS DateTime), CAST(N'2022-01-22T21:58:20.763' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (561, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:20.940' AS DateTime), CAST(N'2022-01-22T21:58:20.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (562, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:21.127' AS DateTime), CAST(N'2022-01-22T21:58:21.127' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (563, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:21.313' AS DateTime), CAST(N'2022-01-22T21:58:21.313' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (564, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:21.487' AS DateTime), CAST(N'2022-01-22T21:58:21.487' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (565, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:21.673' AS DateTime), CAST(N'2022-01-22T21:58:21.673' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (566, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:21.860' AS DateTime), CAST(N'2022-01-22T21:58:21.860' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (567, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:26.810' AS DateTime), CAST(N'2022-01-22T21:58:26.810' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (568, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:27.490' AS DateTime), CAST(N'2022-01-22T21:58:27.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (569, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:27.657' AS DateTime), CAST(N'2022-01-22T21:58:27.657' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (570, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:27.867' AS DateTime), CAST(N'2022-01-22T21:58:27.867' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (571, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:28.040' AS DateTime), CAST(N'2022-01-22T21:58:28.040' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (572, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:28.723' AS DateTime), CAST(N'2022-01-22T21:58:28.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (573, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:28.927' AS DateTime), CAST(N'2022-01-22T21:58:28.927' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (574, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:29.107' AS DateTime), CAST(N'2022-01-22T21:58:29.107' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (575, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:29.290' AS DateTime), CAST(N'2022-01-22T21:58:29.290' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (576, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:29.570' AS DateTime), CAST(N'2022-01-22T21:58:29.570' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (577, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:29.993' AS DateTime), CAST(N'2022-01-22T21:58:29.993' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (578, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:30.210' AS DateTime), CAST(N'2022-01-22T21:58:30.210' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (579, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:30.397' AS DateTime), CAST(N'2022-01-22T21:58:30.397' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (580, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:30.580' AS DateTime), CAST(N'2022-01-22T21:58:30.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (581, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:30.773' AS DateTime), CAST(N'2022-01-22T21:58:30.773' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (582, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:30.950' AS DateTime), CAST(N'2022-01-22T21:58:30.950' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (583, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:31.143' AS DateTime), CAST(N'2022-01-22T21:58:31.143' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (584, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:31.310' AS DateTime), CAST(N'2022-01-22T21:58:31.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (585, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:32.123' AS DateTime), CAST(N'2022-01-22T21:58:32.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (586, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:32.293' AS DateTime), CAST(N'2022-01-22T21:58:32.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (587, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:58:48.963' AS DateTime), CAST(N'2022-01-22T21:58:48.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (588, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:59:00.693' AS DateTime), CAST(N'2022-01-22T21:59:00.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (589, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T21:59:25.007' AS DateTime), CAST(N'2022-01-22T21:59:25.007' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (590, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:24.633' AS DateTime), CAST(N'2022-01-22T22:01:24.633' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (591, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:36.917' AS DateTime), CAST(N'2022-01-22T22:01:36.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (592, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:38.920' AS DateTime), CAST(N'2022-01-22T22:01:38.920' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (593, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:40.187' AS DateTime), CAST(N'2022-01-22T22:01:40.187' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (594, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:41.333' AS DateTime), CAST(N'2022-01-22T22:01:41.333' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (595, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:42.073' AS DateTime), CAST(N'2022-01-22T22:01:42.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (596, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:42.327' AS DateTime), CAST(N'2022-01-22T22:01:42.327' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (597, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:42.550' AS DateTime), CAST(N'2022-01-22T22:01:42.550' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (598, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:42.737' AS DateTime), CAST(N'2022-01-22T22:01:42.737' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (599, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:42.940' AS DateTime), CAST(N'2022-01-22T22:01:42.940' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (600, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:43.117' AS DateTime), CAST(N'2022-01-22T22:01:43.117' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (601, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:43.910' AS DateTime), CAST(N'2022-01-22T22:01:43.910' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (602, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:44.110' AS DateTime), CAST(N'2022-01-22T22:01:44.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (603, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:44.283' AS DateTime), CAST(N'2022-01-22T22:01:44.283' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (604, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:44.463' AS DateTime), CAST(N'2022-01-22T22:01:44.463' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (605, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:44.647' AS DateTime), CAST(N'2022-01-22T22:01:44.647' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (606, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:44.843' AS DateTime), CAST(N'2022-01-22T22:01:44.843' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (607, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:45.043' AS DateTime), CAST(N'2022-01-22T22:01:45.043' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (608, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:45.227' AS DateTime), CAST(N'2022-01-22T22:01:45.227' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (609, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:45.410' AS DateTime), CAST(N'2022-01-22T22:01:45.410' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (610, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:45.587' AS DateTime), CAST(N'2022-01-22T22:01:45.587' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (611, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:45.773' AS DateTime), CAST(N'2022-01-22T22:01:45.773' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (612, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:45.960' AS DateTime), CAST(N'2022-01-22T22:01:45.960' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (613, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:46.150' AS DateTime), CAST(N'2022-01-22T22:01:46.150' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (614, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:46.340' AS DateTime), CAST(N'2022-01-22T22:01:46.340' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (615, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:46.523' AS DateTime), CAST(N'2022-01-22T22:01:46.523' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (616, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:46.713' AS DateTime), CAST(N'2022-01-22T22:01:46.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (617, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:46.953' AS DateTime), CAST(N'2022-01-22T22:01:46.953' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (618, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:47.123' AS DateTime), CAST(N'2022-01-22T22:01:47.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (619, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:47.310' AS DateTime), CAST(N'2022-01-22T22:01:47.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (620, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:47.510' AS DateTime), CAST(N'2022-01-22T22:01:47.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (621, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:47.700' AS DateTime), CAST(N'2022-01-22T22:01:47.700' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (622, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:47.897' AS DateTime), CAST(N'2022-01-22T22:01:47.897' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (623, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:48.080' AS DateTime), CAST(N'2022-01-22T22:01:48.080' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (624, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:48.253' AS DateTime), CAST(N'2022-01-22T22:01:48.253' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (625, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:48.440' AS DateTime), CAST(N'2022-01-22T22:01:48.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (626, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:48.653' AS DateTime), CAST(N'2022-01-22T22:01:48.653' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (627, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:48.853' AS DateTime), CAST(N'2022-01-22T22:01:48.853' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (628, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:49.040' AS DateTime), CAST(N'2022-01-22T22:01:49.040' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (629, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:49.220' AS DateTime), CAST(N'2022-01-22T22:01:49.220' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (630, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:01:49.393' AS DateTime), CAST(N'2022-01-22T22:01:49.393' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (631, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:00.070' AS DateTime), CAST(N'2022-01-22T22:02:00.070' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (632, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:04.183' AS DateTime), CAST(N'2022-01-22T22:02:04.183' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (633, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:05.520' AS DateTime), CAST(N'2022-01-22T22:02:05.520' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (634, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:06.550' AS DateTime), CAST(N'2022-01-22T22:02:06.550' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (635, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:08.187' AS DateTime), CAST(N'2022-01-22T22:02:08.187' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (636, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:08.390' AS DateTime), CAST(N'2022-01-22T22:02:08.390' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (637, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:08.560' AS DateTime), CAST(N'2022-01-22T22:02:08.560' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (638, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:08.750' AS DateTime), CAST(N'2022-01-22T22:02:08.750' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (639, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:08.920' AS DateTime), CAST(N'2022-01-22T22:02:08.920' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (640, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:10.213' AS DateTime), CAST(N'2022-01-22T22:02:10.213' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (641, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:10.410' AS DateTime), CAST(N'2022-01-22T22:02:10.410' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (642, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:10.587' AS DateTime), CAST(N'2022-01-22T22:02:10.587' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (643, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:02:10.760' AS DateTime), CAST(N'2022-01-22T22:02:10.760' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (644, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:29.467' AS DateTime), CAST(N'2022-01-22T22:04:29.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (645, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:31.747' AS DateTime), CAST(N'2022-01-22T22:04:31.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (646, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:33.390' AS DateTime), CAST(N'2022-01-22T22:04:33.390' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (647, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:34.250' AS DateTime), CAST(N'2022-01-22T22:04:34.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (648, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:34.447' AS DateTime), CAST(N'2022-01-22T22:04:34.447' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (649, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:34.623' AS DateTime), CAST(N'2022-01-22T22:04:34.623' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (650, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:34.830' AS DateTime), CAST(N'2022-01-22T22:04:34.830' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (651, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:35.023' AS DateTime), CAST(N'2022-01-22T22:04:35.023' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (652, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:35.197' AS DateTime), CAST(N'2022-01-22T22:04:35.197' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (653, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:35.387' AS DateTime), CAST(N'2022-01-22T22:04:35.387' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (654, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:35.560' AS DateTime), CAST(N'2022-01-22T22:04:35.560' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (655, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:36.883' AS DateTime), CAST(N'2022-01-22T22:04:36.883' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (656, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:37.083' AS DateTime), CAST(N'2022-01-22T22:04:37.083' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (657, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:37.270' AS DateTime), CAST(N'2022-01-22T22:04:37.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (658, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:37.453' AS DateTime), CAST(N'2022-01-22T22:04:37.453' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (659, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:37.617' AS DateTime), CAST(N'2022-01-22T22:04:37.617' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (660, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:37.803' AS DateTime), CAST(N'2022-01-22T22:04:37.803' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (661, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:04:38.000' AS DateTime), CAST(N'2022-01-22T22:04:38.000' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (662, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:31.100' AS DateTime), CAST(N'2022-01-22T22:05:31.100' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (663, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:34.333' AS DateTime), CAST(N'2022-01-22T22:05:34.333' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (664, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:36.450' AS DateTime), CAST(N'2022-01-22T22:05:36.450' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (665, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:40.213' AS DateTime), CAST(N'2022-01-22T22:05:40.213' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (666, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:42.490' AS DateTime), CAST(N'2022-01-22T22:05:42.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (667, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:44.690' AS DateTime), CAST(N'2022-01-22T22:05:44.690' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (668, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:47.437' AS DateTime), CAST(N'2022-01-22T22:05:47.437' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (669, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:50.323' AS DateTime), CAST(N'2022-01-22T22:05:50.323' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (670, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:05:56.877' AS DateTime), CAST(N'2022-01-22T22:05:56.877' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (671, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:06:10.303' AS DateTime), CAST(N'2022-01-22T22:06:10.303' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (672, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:06:23.113' AS DateTime), CAST(N'2022-01-22T22:06:23.113' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (673, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:06:44.127' AS DateTime), CAST(N'2022-01-22T22:06:44.127' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (674, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:06:47.783' AS DateTime), CAST(N'2022-01-22T22:06:47.783' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (675, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:06:51.643' AS DateTime), CAST(N'2022-01-22T22:06:51.643' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (676, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:00.340' AS DateTime), CAST(N'2022-01-22T22:07:00.340' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (677, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:04.247' AS DateTime), CAST(N'2022-01-22T22:07:04.247' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (678, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:09.360' AS DateTime), CAST(N'2022-01-22T22:07:09.360' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (679, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:16.363' AS DateTime), CAST(N'2022-01-22T22:07:16.363' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (680, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:20.547' AS DateTime), CAST(N'2022-01-22T22:07:20.547' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (681, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:24.263' AS DateTime), CAST(N'2022-01-22T22:07:24.263' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (682, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:26.210' AS DateTime), CAST(N'2022-01-22T22:07:26.210' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (683, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:27.770' AS DateTime), CAST(N'2022-01-22T22:07:27.770' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (684, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:30.623' AS DateTime), CAST(N'2022-01-22T22:07:30.623' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (685, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:32.293' AS DateTime), CAST(N'2022-01-22T22:07:32.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (686, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:33.630' AS DateTime), CAST(N'2022-01-22T22:07:33.630' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (687, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:34.710' AS DateTime), CAST(N'2022-01-22T22:07:34.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (688, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:36.827' AS DateTime), CAST(N'2022-01-22T22:07:36.827' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (689, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:38.417' AS DateTime), CAST(N'2022-01-22T22:07:38.417' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (690, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:40.123' AS DateTime), CAST(N'2022-01-22T22:07:40.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (691, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:40.297' AS DateTime), CAST(N'2022-01-22T22:07:40.297' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (692, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:43.237' AS DateTime), CAST(N'2022-01-22T22:07:43.237' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (693, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:45.390' AS DateTime), CAST(N'2022-01-22T22:07:45.390' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (694, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:46.467' AS DateTime), CAST(N'2022-01-22T22:07:46.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (695, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:46.707' AS DateTime), CAST(N'2022-01-22T22:07:46.707' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (696, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:47.707' AS DateTime), CAST(N'2022-01-22T22:07:47.707' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (697, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:07:47.880' AS DateTime), CAST(N'2022-01-22T22:07:47.880' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (698, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:13:13.013' AS DateTime), CAST(N'2022-01-22T22:13:13.013' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (699, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:13:15.300' AS DateTime), CAST(N'2022-01-22T22:13:15.300' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (700, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:13:16.930' AS DateTime), CAST(N'2022-01-22T22:13:16.930' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (701, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:15:18.027' AS DateTime), CAST(N'2022-01-22T22:15:18.027' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (702, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:34.430' AS DateTime), CAST(N'2022-01-22T22:17:34.430' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (703, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:36.440' AS DateTime), CAST(N'2022-01-22T22:17:36.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (704, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:39.597' AS DateTime), CAST(N'2022-01-22T22:17:39.597' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (705, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:42.333' AS DateTime), CAST(N'2022-01-22T22:17:42.333' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (706, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:46.847' AS DateTime), CAST(N'2022-01-22T22:17:46.847' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (707, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:47.870' AS DateTime), CAST(N'2022-01-22T22:17:47.870' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (708, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:48.603' AS DateTime), CAST(N'2022-01-22T22:17:48.603' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (709, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:49.613' AS DateTime), CAST(N'2022-01-22T22:17:49.613' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (710, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:50.247' AS DateTime), CAST(N'2022-01-22T22:17:50.247' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (711, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:50.687' AS DateTime), CAST(N'2022-01-22T22:17:50.687' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (712, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:50.873' AS DateTime), CAST(N'2022-01-22T22:17:50.873' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (713, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:51.057' AS DateTime), CAST(N'2022-01-22T22:17:51.057' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (714, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:51.250' AS DateTime), CAST(N'2022-01-22T22:17:51.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (715, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:51.433' AS DateTime), CAST(N'2022-01-22T22:17:51.433' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (716, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:51.610' AS DateTime), CAST(N'2022-01-22T22:17:51.610' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (717, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:53.200' AS DateTime), CAST(N'2022-01-22T22:17:53.200' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (718, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:54.733' AS DateTime), CAST(N'2022-01-22T22:17:54.733' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (719, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:55.980' AS DateTime), CAST(N'2022-01-22T22:17:55.980' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (720, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:56.963' AS DateTime), CAST(N'2022-01-22T22:17:56.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (721, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:57.107' AS DateTime), CAST(N'2022-01-22T22:17:57.107' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (722, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:58.180' AS DateTime), CAST(N'2022-01-22T22:17:58.180' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (723, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:58.343' AS DateTime), CAST(N'2022-01-22T22:17:58.343' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (724, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:17:59.413' AS DateTime), CAST(N'2022-01-22T22:17:59.413' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (725, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:01.213' AS DateTime), CAST(N'2022-01-22T22:18:01.213' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (726, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:03.130' AS DateTime), CAST(N'2022-01-22T22:18:03.130' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (727, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:03.310' AS DateTime), CAST(N'2022-01-22T22:18:03.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (728, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:05.360' AS DateTime), CAST(N'2022-01-22T22:18:05.360' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (729, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:05.550' AS DateTime), CAST(N'2022-01-22T22:18:05.550' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (730, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:08.853' AS DateTime), CAST(N'2022-01-22T22:18:08.853' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (731, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:09.010' AS DateTime), CAST(N'2022-01-22T22:18:09.010' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (732, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:10.573' AS DateTime), CAST(N'2022-01-22T22:18:10.573' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (733, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:10.727' AS DateTime), CAST(N'2022-01-22T22:18:10.727' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (734, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:12.270' AS DateTime), CAST(N'2022-01-22T22:18:12.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (735, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:15.837' AS DateTime), CAST(N'2022-01-22T22:18:15.837' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (736, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:17.383' AS DateTime), CAST(N'2022-01-22T22:18:17.383' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (737, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:18.760' AS DateTime), CAST(N'2022-01-22T22:18:18.760' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (738, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:19.990' AS DateTime), CAST(N'2022-01-22T22:18:19.990' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (739, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:22.097' AS DateTime), CAST(N'2022-01-22T22:18:22.097' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (740, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:23.773' AS DateTime), CAST(N'2022-01-22T22:18:23.773' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (741, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:25.517' AS DateTime), CAST(N'2022-01-22T22:18:25.517' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (742, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:26.787' AS DateTime), CAST(N'2022-01-22T22:18:26.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (743, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:27.883' AS DateTime), CAST(N'2022-01-22T22:18:27.883' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (744, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:29.347' AS DateTime), CAST(N'2022-01-22T22:18:29.347' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (745, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:30.567' AS DateTime), CAST(N'2022-01-22T22:18:30.567' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (746, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:31.883' AS DateTime), CAST(N'2022-01-22T22:18:31.883' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (747, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:33.010' AS DateTime), CAST(N'2022-01-22T22:18:33.010' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (748, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:34.213' AS DateTime), CAST(N'2022-01-22T22:18:34.213' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (749, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:35.510' AS DateTime), CAST(N'2022-01-22T22:18:35.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (750, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:36.503' AS DateTime), CAST(N'2022-01-22T22:18:36.503' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (751, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:37.317' AS DateTime), CAST(N'2022-01-22T22:18:37.317' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (752, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:37.857' AS DateTime), CAST(N'2022-01-22T22:18:37.857' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (753, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:38.520' AS DateTime), CAST(N'2022-01-22T22:18:38.520' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (754, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:38.683' AS DateTime), CAST(N'2022-01-22T22:18:38.683' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (755, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:39.480' AS DateTime), CAST(N'2022-01-22T22:18:39.480' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (756, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:39.663' AS DateTime), CAST(N'2022-01-22T22:18:39.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (757, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:39.830' AS DateTime), CAST(N'2022-01-22T22:18:39.830' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (758, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.010' AS DateTime), CAST(N'2022-01-22T22:18:40.010' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (759, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.187' AS DateTime), CAST(N'2022-01-22T22:18:40.187' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (760, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.350' AS DateTime), CAST(N'2022-01-22T22:18:40.350' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (761, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.517' AS DateTime), CAST(N'2022-01-22T22:18:40.517' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (762, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.703' AS DateTime), CAST(N'2022-01-22T22:18:40.703' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (763, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.887' AS DateTime), CAST(N'2022-01-22T22:18:40.887' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (764, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:40.993' AS DateTime), CAST(N'2022-01-22T22:18:40.993' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (765, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:46.483' AS DateTime), CAST(N'2022-01-22T22:18:46.483' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (766, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:48.200' AS DateTime), CAST(N'2022-01-22T22:18:48.200' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (767, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:18:49.440' AS DateTime), CAST(N'2022-01-22T22:18:49.440' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (768, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:19:53.683' AS DateTime), CAST(N'2022-01-22T22:19:53.683' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (769, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:19:56.393' AS DateTime), CAST(N'2022-01-22T22:19:56.393' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (770, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:19:59.407' AS DateTime), CAST(N'2022-01-22T22:19:59.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (771, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:20:07.680' AS DateTime), CAST(N'2022-01-22T22:20:07.680' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (772, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:05.917' AS DateTime), CAST(N'2022-01-22T22:21:05.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (773, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:07.777' AS DateTime), CAST(N'2022-01-22T22:21:07.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (774, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:09.833' AS DateTime), CAST(N'2022-01-22T22:21:09.833' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (775, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:11.523' AS DateTime), CAST(N'2022-01-22T22:21:11.523' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (776, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:14.280' AS DateTime), CAST(N'2022-01-22T22:21:14.280' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (777, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:16.420' AS DateTime), CAST(N'2022-01-22T22:21:16.420' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (778, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:17.880' AS DateTime), CAST(N'2022-01-22T22:21:17.880' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (779, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:19.380' AS DateTime), CAST(N'2022-01-22T22:21:19.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (780, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:20.407' AS DateTime), CAST(N'2022-01-22T22:21:20.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (781, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:21.303' AS DateTime), CAST(N'2022-01-22T22:21:21.303' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (782, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:22.443' AS DateTime), CAST(N'2022-01-22T22:21:22.443' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (783, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:22.693' AS DateTime), CAST(N'2022-01-22T22:21:22.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (784, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:22.940' AS DateTime), CAST(N'2022-01-22T22:21:22.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (785, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:24.490' AS DateTime), CAST(N'2022-01-22T22:21:24.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (786, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:24.757' AS DateTime), CAST(N'2022-01-22T22:21:24.757' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (787, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:25.007' AS DateTime), CAST(N'2022-01-22T22:21:25.007' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (788, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:25.253' AS DateTime), CAST(N'2022-01-22T22:21:25.253' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (789, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:25.470' AS DateTime), CAST(N'2022-01-22T22:21:25.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (790, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:25.693' AS DateTime), CAST(N'2022-01-22T22:21:25.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (791, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:32.300' AS DateTime), CAST(N'2022-01-22T22:21:32.300' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (792, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:32.513' AS DateTime), CAST(N'2022-01-22T22:21:32.513' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (793, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:32.710' AS DateTime), CAST(N'2022-01-22T22:21:32.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (794, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:36.347' AS DateTime), CAST(N'2022-01-22T22:21:36.347' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (795, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:36.537' AS DateTime), CAST(N'2022-01-22T22:21:36.537' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (796, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:39.370' AS DateTime), CAST(N'2022-01-22T22:21:39.370' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (797, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:39.557' AS DateTime), CAST(N'2022-01-22T22:21:39.557' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (798, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:41.207' AS DateTime), CAST(N'2022-01-22T22:21:41.207' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (799, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:41.407' AS DateTime), CAST(N'2022-01-22T22:21:41.407' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (800, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:42.587' AS DateTime), CAST(N'2022-01-22T22:21:42.587' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (801, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:42.817' AS DateTime), CAST(N'2022-01-22T22:21:42.817' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (802, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:43.707' AS DateTime), CAST(N'2022-01-22T22:21:43.707' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (803, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:43.980' AS DateTime), CAST(N'2022-01-22T22:21:43.980' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (804, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:44.917' AS DateTime), CAST(N'2022-01-22T22:21:44.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (805, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:45.153' AS DateTime), CAST(N'2022-01-22T22:21:45.153' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (806, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:46.940' AS DateTime), CAST(N'2022-01-22T22:21:46.940' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (807, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:47.150' AS DateTime), CAST(N'2022-01-22T22:21:47.150' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (808, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:48.057' AS DateTime), CAST(N'2022-01-22T22:21:48.057' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (809, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:48.263' AS DateTime), CAST(N'2022-01-22T22:21:48.263' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (810, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:49.250' AS DateTime), CAST(N'2022-01-22T22:21:49.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (811, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:49.470' AS DateTime), CAST(N'2022-01-22T22:21:49.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (812, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:50.280' AS DateTime), CAST(N'2022-01-22T22:21:50.280' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (813, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:50.483' AS DateTime), CAST(N'2022-01-22T22:21:50.483' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (814, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:51.280' AS DateTime), CAST(N'2022-01-22T22:21:51.280' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (815, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:51.933' AS DateTime), CAST(N'2022-01-22T22:21:51.933' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (816, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:52.647' AS DateTime), CAST(N'2022-01-22T22:21:52.647' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (817, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:53.547' AS DateTime), CAST(N'2022-01-22T22:21:53.547' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (818, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:54.373' AS DateTime), CAST(N'2022-01-22T22:21:54.373' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (819, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:55.103' AS DateTime), CAST(N'2022-01-22T22:21:55.103' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (820, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:56.370' AS DateTime), CAST(N'2022-01-22T22:21:56.370' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (821, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:21:57.367' AS DateTime), CAST(N'2022-01-22T22:21:57.367' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (822, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:41.253' AS DateTime), CAST(N'2022-01-22T22:23:41.253' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (823, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:44.907' AS DateTime), CAST(N'2022-01-22T22:23:44.907' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (824, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:46.873' AS DateTime), CAST(N'2022-01-22T22:23:46.873' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (825, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:49.060' AS DateTime), CAST(N'2022-01-22T22:23:49.060' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (826, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:51.140' AS DateTime), CAST(N'2022-01-22T22:23:51.140' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (827, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:52.797' AS DateTime), CAST(N'2022-01-22T22:23:52.797' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (828, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:54.513' AS DateTime), CAST(N'2022-01-22T22:23:54.513' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (829, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:56.250' AS DateTime), CAST(N'2022-01-22T22:23:56.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (830, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:57.813' AS DateTime), CAST(N'2022-01-22T22:23:57.813' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (831, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:23:58.900' AS DateTime), CAST(N'2022-01-22T22:23:58.900' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (832, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:02.190' AS DateTime), CAST(N'2022-01-22T22:24:02.190' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (833, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:04.070' AS DateTime), CAST(N'2022-01-22T22:24:04.070' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (834, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:05.337' AS DateTime), CAST(N'2022-01-22T22:24:05.337' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (835, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:06.567' AS DateTime), CAST(N'2022-01-22T22:24:06.567' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (836, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:07.453' AS DateTime), CAST(N'2022-01-22T22:24:07.453' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (837, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:08.337' AS DateTime), CAST(N'2022-01-22T22:24:08.337' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (838, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:09.270' AS DateTime), CAST(N'2022-01-22T22:24:09.270' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (839, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:09.903' AS DateTime), CAST(N'2022-01-22T22:24:09.903' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (840, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:10.457' AS DateTime), CAST(N'2022-01-22T22:24:10.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (841, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:11.057' AS DateTime), CAST(N'2022-01-22T22:24:11.057' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (842, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:24.747' AS DateTime), CAST(N'2022-01-22T22:24:24.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (843, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:25.820' AS DateTime), CAST(N'2022-01-22T22:24:25.820' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (844, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T22:24:26.540' AS DateTime), CAST(N'2022-01-22T22:24:26.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (845, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:35.027' AS DateTime), CAST(N'2022-01-22T23:09:35.027' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (846, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:36.863' AS DateTime), CAST(N'2022-01-22T23:09:36.863' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (847, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:38.177' AS DateTime), CAST(N'2022-01-22T23:09:38.177' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (848, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:40.293' AS DateTime), CAST(N'2022-01-22T23:09:40.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (849, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:42.077' AS DateTime), CAST(N'2022-01-22T23:09:42.077' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (850, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:44.467' AS DateTime), CAST(N'2022-01-22T23:09:44.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (851, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:46.407' AS DateTime), CAST(N'2022-01-22T23:09:46.407' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (852, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:47.627' AS DateTime), CAST(N'2022-01-22T23:09:47.627' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (853, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:49.510' AS DateTime), CAST(N'2022-01-22T23:09:49.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (854, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:50.477' AS DateTime), CAST(N'2022-01-22T23:09:50.477' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (855, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:50.663' AS DateTime), CAST(N'2022-01-22T23:09:50.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (856, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:51.917' AS DateTime), CAST(N'2022-01-22T23:09:51.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (857, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:52.763' AS DateTime), CAST(N'2022-01-22T23:09:52.763' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (858, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:54.510' AS DateTime), CAST(N'2022-01-22T23:09:54.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (859, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:09:55.380' AS DateTime), CAST(N'2022-01-22T23:09:55.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (860, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:39.470' AS DateTime), CAST(N'2022-01-22T23:12:39.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (861, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:41.550' AS DateTime), CAST(N'2022-01-22T23:12:41.550' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (862, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:42.960' AS DateTime), CAST(N'2022-01-22T23:12:42.960' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (863, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:43.913' AS DateTime), CAST(N'2022-01-22T23:12:43.913' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (864, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:44.540' AS DateTime), CAST(N'2022-01-22T23:12:44.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (865, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:44.733' AS DateTime), CAST(N'2022-01-22T23:12:44.733' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (866, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:45.693' AS DateTime), CAST(N'2022-01-22T23:12:45.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (867, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:45.867' AS DateTime), CAST(N'2022-01-22T23:12:45.867' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (868, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:46.070' AS DateTime), CAST(N'2022-01-22T23:12:46.070' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (869, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:46.273' AS DateTime), CAST(N'2022-01-22T23:12:46.273' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (870, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:46.490' AS DateTime), CAST(N'2022-01-22T23:12:46.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (871, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:46.693' AS DateTime), CAST(N'2022-01-22T23:12:46.693' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (872, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:46.887' AS DateTime), CAST(N'2022-01-22T23:12:46.887' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (873, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:47.073' AS DateTime), CAST(N'2022-01-22T23:12:47.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (874, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:47.850' AS DateTime), CAST(N'2022-01-22T23:12:47.850' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (875, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:48.487' AS DateTime), CAST(N'2022-01-22T23:12:48.487' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (876, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:49.123' AS DateTime), CAST(N'2022-01-22T23:12:49.123' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (877, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:49.713' AS DateTime), CAST(N'2022-01-22T23:12:49.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (878, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:50.223' AS DateTime), CAST(N'2022-01-22T23:12:50.223' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (879, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:50.800' AS DateTime), CAST(N'2022-01-22T23:12:50.800' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (880, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:51.963' AS DateTime), CAST(N'2022-01-22T23:12:51.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (881, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:52.567' AS DateTime), CAST(N'2022-01-22T23:12:52.567' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (882, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:53.013' AS DateTime), CAST(N'2022-01-22T23:12:53.013' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (883, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:53.480' AS DateTime), CAST(N'2022-01-22T23:12:53.480' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (884, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:54.293' AS DateTime), CAST(N'2022-01-22T23:12:54.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (885, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:55.147' AS DateTime), CAST(N'2022-01-22T23:12:55.147' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (886, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:55.710' AS DateTime), CAST(N'2022-01-22T23:12:55.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (887, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:56.223' AS DateTime), CAST(N'2022-01-22T23:12:56.223' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (888, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:56.467' AS DateTime), CAST(N'2022-01-22T23:12:56.467' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (889, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:56.663' AS DateTime), CAST(N'2022-01-22T23:12:56.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (890, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:56.857' AS DateTime), CAST(N'2022-01-22T23:12:56.857' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (891, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:57.047' AS DateTime), CAST(N'2022-01-22T23:12:57.047' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (892, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:57.240' AS DateTime), CAST(N'2022-01-22T23:12:57.240' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (893, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:57.757' AS DateTime), CAST(N'2022-01-22T23:12:57.757' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (894, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:58.333' AS DateTime), CAST(N'2022-01-22T23:12:58.333' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (895, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:58.830' AS DateTime), CAST(N'2022-01-22T23:12:58.830' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (896, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:59.280' AS DateTime), CAST(N'2022-01-22T23:12:59.280' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (897, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:12:59.713' AS DateTime), CAST(N'2022-01-22T23:12:59.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (898, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:00.143' AS DateTime), CAST(N'2022-01-22T23:13:00.143' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (899, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:00.567' AS DateTime), CAST(N'2022-01-22T23:13:00.567' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (900, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:01.050' AS DateTime), CAST(N'2022-01-22T23:13:01.050' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (901, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:01.493' AS DateTime), CAST(N'2022-01-22T23:13:01.493' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (902, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:51.943' AS DateTime), CAST(N'2022-01-22T23:13:51.943' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (903, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:55.087' AS DateTime), CAST(N'2022-01-22T23:13:55.087' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (904, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:56.103' AS DateTime), CAST(N'2022-01-22T23:13:56.103' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (905, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:57.433' AS DateTime), CAST(N'2022-01-22T23:13:57.433' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (906, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:58.853' AS DateTime), CAST(N'2022-01-22T23:13:58.853' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (907, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:13:59.017' AS DateTime), CAST(N'2022-01-22T23:13:59.017' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (908, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:14:48.520' AS DateTime), CAST(N'2022-01-22T23:14:48.520' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (909, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:14:50.257' AS DateTime), CAST(N'2022-01-22T23:14:50.257' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (910, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:27.787' AS DateTime), CAST(N'2022-01-22T23:15:27.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (911, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:29.410' AS DateTime), CAST(N'2022-01-22T23:15:29.410' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (912, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:31.190' AS DateTime), CAST(N'2022-01-22T23:15:31.190' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (913, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:33.353' AS DateTime), CAST(N'2022-01-22T23:15:33.353' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (914, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:35.090' AS DateTime), CAST(N'2022-01-22T23:15:35.090' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (915, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:36.370' AS DateTime), CAST(N'2022-01-22T23:15:36.370' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (916, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:37.077' AS DateTime), CAST(N'2022-01-22T23:15:37.077' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (917, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:37.777' AS DateTime), CAST(N'2022-01-22T23:15:37.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (918, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:38.323' AS DateTime), CAST(N'2022-01-22T23:15:38.323' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (919, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:38.913' AS DateTime), CAST(N'2022-01-22T23:15:38.913' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (920, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:39.770' AS DateTime), CAST(N'2022-01-22T23:15:39.770' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (921, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:41.413' AS DateTime), CAST(N'2022-01-22T23:15:41.413' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (922, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:42.730' AS DateTime), CAST(N'2022-01-22T23:15:42.730' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (923, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:43.607' AS DateTime), CAST(N'2022-01-22T23:15:43.607' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (924, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:45.253' AS DateTime), CAST(N'2022-01-22T23:15:45.253' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (925, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:46.843' AS DateTime), CAST(N'2022-01-22T23:15:46.843' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (926, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:48.680' AS DateTime), CAST(N'2022-01-22T23:15:48.680' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (927, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:50.823' AS DateTime), CAST(N'2022-01-22T23:15:50.823' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (928, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:52.850' AS DateTime), CAST(N'2022-01-22T23:15:52.850' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (929, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:54.727' AS DateTime), CAST(N'2022-01-22T23:15:54.727' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (930, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:57.120' AS DateTime), CAST(N'2022-01-22T23:15:57.120' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (931, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:15:59.113' AS DateTime), CAST(N'2022-01-22T23:15:59.113' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (932, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:01.313' AS DateTime), CAST(N'2022-01-22T23:16:01.313' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (933, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:03.530' AS DateTime), CAST(N'2022-01-22T23:16:03.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (934, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:04.300' AS DateTime), CAST(N'2022-01-22T23:16:04.300' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (935, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:05.263' AS DateTime), CAST(N'2022-01-22T23:16:05.263' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (936, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:05.970' AS DateTime), CAST(N'2022-01-22T23:16:05.970' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (937, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:06.670' AS DateTime), CAST(N'2022-01-22T23:16:06.670' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (938, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:07.780' AS DateTime), CAST(N'2022-01-22T23:16:07.780' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (939, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:09.067' AS DateTime), CAST(N'2022-01-22T23:16:09.067' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (940, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:09.713' AS DateTime), CAST(N'2022-01-22T23:16:09.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (941, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:09.913' AS DateTime), CAST(N'2022-01-22T23:16:09.913' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (942, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:11.363' AS DateTime), CAST(N'2022-01-22T23:16:11.363' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (943, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:11.673' AS DateTime), CAST(N'2022-01-22T23:16:11.673' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (944, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:11.870' AS DateTime), CAST(N'2022-01-22T23:16:11.870' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (945, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:13.113' AS DateTime), CAST(N'2022-01-22T23:16:13.113' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (946, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:14.250' AS DateTime), CAST(N'2022-01-22T23:16:14.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (947, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:40.963' AS DateTime), CAST(N'2022-01-22T23:16:40.963' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (948, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:42.493' AS DateTime), CAST(N'2022-01-22T23:16:42.493' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (949, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:43.683' AS DateTime), CAST(N'2022-01-22T23:16:43.683' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (950, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:44.383' AS DateTime), CAST(N'2022-01-22T23:16:44.383' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (951, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:45.580' AS DateTime), CAST(N'2022-01-22T23:16:45.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (952, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:46.277' AS DateTime), CAST(N'2022-01-22T23:16:46.277' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (953, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:16:46.470' AS DateTime), CAST(N'2022-01-22T23:16:46.470' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (954, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:18:56.000' AS DateTime), CAST(N'2022-01-22T23:18:56.000' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (955, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:19:24.307' AS DateTime), CAST(N'2022-01-22T23:19:24.307' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (956, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:19:38.743' AS DateTime), CAST(N'2022-01-22T23:19:38.743' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (957, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:19:46.773' AS DateTime), CAST(N'2022-01-22T23:19:46.773' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (958, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:19:55.683' AS DateTime), CAST(N'2022-01-22T23:19:55.683' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (959, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:20:12.083' AS DateTime), CAST(N'2022-01-22T23:20:12.083' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (960, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:20:59.160' AS DateTime), CAST(N'2022-01-22T23:20:59.160' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (961, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:21:07.147' AS DateTime), CAST(N'2022-01-22T23:21:07.147' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (962, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:21:11.647' AS DateTime), CAST(N'2022-01-22T23:21:11.647' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (963, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:21:15.380' AS DateTime), CAST(N'2022-01-22T23:21:15.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (964, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:21:38.267' AS DateTime), CAST(N'2022-01-22T23:21:38.267' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (965, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:25.810' AS DateTime), CAST(N'2022-01-22T23:22:25.810' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (966, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:38.427' AS DateTime), CAST(N'2022-01-22T23:22:38.427' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (967, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:39.713' AS DateTime), CAST(N'2022-01-22T23:22:39.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (968, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:40.757' AS DateTime), CAST(N'2022-01-22T23:22:40.757' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (969, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:42.173' AS DateTime), CAST(N'2022-01-22T23:22:42.173' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (970, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:43.697' AS DateTime), CAST(N'2022-01-22T23:22:43.697' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (971, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:44.510' AS DateTime), CAST(N'2022-01-22T23:22:44.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (972, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:45.300' AS DateTime), CAST(N'2022-01-22T23:22:45.300' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (973, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:45.863' AS DateTime), CAST(N'2022-01-22T23:22:45.863' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (974, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:46.377' AS DateTime), CAST(N'2022-01-22T23:22:46.377' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (975, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:46.883' AS DateTime), CAST(N'2022-01-22T23:22:46.883' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (976, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:47.700' AS DateTime), CAST(N'2022-01-22T23:22:47.700' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (977, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:48.360' AS DateTime), CAST(N'2022-01-22T23:22:48.360' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (978, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:48.960' AS DateTime), CAST(N'2022-01-22T23:22:48.960' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (979, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:49.517' AS DateTime), CAST(N'2022-01-22T23:22:49.517' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (980, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:50.323' AS DateTime), CAST(N'2022-01-22T23:22:50.323' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (981, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:50.827' AS DateTime), CAST(N'2022-01-22T23:22:50.827' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (982, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:51.457' AS DateTime), CAST(N'2022-01-22T23:22:51.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (983, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:52.010' AS DateTime), CAST(N'2022-01-22T23:22:52.010' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (984, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:52.210' AS DateTime), CAST(N'2022-01-22T23:22:52.210' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (985, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:52.397' AS DateTime), CAST(N'2022-01-22T23:22:52.397' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (986, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:53.510' AS DateTime), CAST(N'2022-01-22T23:22:53.510' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (987, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:53.720' AS DateTime), CAST(N'2022-01-22T23:22:53.720' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (988, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:22:53.893' AS DateTime), CAST(N'2022-01-22T23:22:53.893' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (989, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:27:24.657' AS DateTime), CAST(N'2022-01-22T23:27:24.657' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (990, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:37.710' AS DateTime), CAST(N'2022-01-22T23:35:37.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (991, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:41.530' AS DateTime), CAST(N'2022-01-22T23:35:41.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (992, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:43.823' AS DateTime), CAST(N'2022-01-22T23:35:43.823' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (993, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:44.967' AS DateTime), CAST(N'2022-01-22T23:35:44.967' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (994, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:45.797' AS DateTime), CAST(N'2022-01-22T23:35:45.797' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (995, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:56.170' AS DateTime), CAST(N'2022-01-22T23:35:56.170' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (996, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:35:58.977' AS DateTime), CAST(N'2022-01-22T23:35:58.977' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (997, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:00.483' AS DateTime), CAST(N'2022-01-22T23:36:00.483' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (998, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:01.747' AS DateTime), CAST(N'2022-01-22T23:36:01.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (999, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:02.457' AS DateTime), CAST(N'2022-01-22T23:36:02.457' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1000, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:02.793' AS DateTime), CAST(N'2022-01-22T23:36:02.793' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1001, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:02.993' AS DateTime), CAST(N'2022-01-22T23:36:02.993' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1002, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:03.190' AS DateTime), CAST(N'2022-01-22T23:36:03.190' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1003, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:03.353' AS DateTime), CAST(N'2022-01-22T23:36:03.353' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1004, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:03.553' AS DateTime), CAST(N'2022-01-22T23:36:03.553' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1005, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:36:03.870' AS DateTime), CAST(N'2022-01-22T23:36:03.870' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1006, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:16.293' AS DateTime), CAST(N'2022-01-22T23:37:16.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1007, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:19.730' AS DateTime), CAST(N'2022-01-22T23:37:19.730' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1008, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:21.250' AS DateTime), CAST(N'2022-01-22T23:37:21.250' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1009, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:22.323' AS DateTime), CAST(N'2022-01-22T23:37:22.323' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1010, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:23.930' AS DateTime), CAST(N'2022-01-22T23:37:23.930' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1011, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:24.907' AS DateTime), CAST(N'2022-01-22T23:37:24.907' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1012, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:26.127' AS DateTime), CAST(N'2022-01-22T23:37:26.127' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1013, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:27.530' AS DateTime), CAST(N'2022-01-22T23:37:27.530' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1014, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:29.163' AS DateTime), CAST(N'2022-01-22T23:37:29.163' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1015, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:30.110' AS DateTime), CAST(N'2022-01-22T23:37:30.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1016, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:31.120' AS DateTime), CAST(N'2022-01-22T23:37:31.120' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1017, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:32.033' AS DateTime), CAST(N'2022-01-22T23:37:32.033' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1018, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:32.890' AS DateTime), CAST(N'2022-01-22T23:37:32.890' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1019, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:33.593' AS DateTime), CAST(N'2022-01-22T23:37:33.593' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1020, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:34.400' AS DateTime), CAST(N'2022-01-22T23:37:34.400' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1021, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:35.043' AS DateTime), CAST(N'2022-01-22T23:37:35.043' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1022, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:35.823' AS DateTime), CAST(N'2022-01-22T23:37:35.823' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1023, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:36.587' AS DateTime), CAST(N'2022-01-22T23:37:36.587' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1024, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:37.380' AS DateTime), CAST(N'2022-01-22T23:37:37.380' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1025, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:38.200' AS DateTime), CAST(N'2022-01-22T23:37:38.200' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1026, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:39.193' AS DateTime), CAST(N'2022-01-22T23:37:39.193' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1027, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:40.050' AS DateTime), CAST(N'2022-01-22T23:37:40.050' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1028, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:40.893' AS DateTime), CAST(N'2022-01-22T23:37:40.893' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1029, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:41.723' AS DateTime), CAST(N'2022-01-22T23:37:41.723' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1030, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:42.373' AS DateTime), CAST(N'2022-01-22T23:37:42.373' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1031, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:42.987' AS DateTime), CAST(N'2022-01-22T23:37:42.987' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1032, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:43.787' AS DateTime), CAST(N'2022-01-22T23:37:43.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1033, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:44.453' AS DateTime), CAST(N'2022-01-22T23:37:44.453' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1034, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:45.067' AS DateTime), CAST(N'2022-01-22T23:37:45.067' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1035, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:45.697' AS DateTime), CAST(N'2022-01-22T23:37:45.697' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1036, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:46.710' AS DateTime), CAST(N'2022-01-22T23:37:46.710' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1037, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:47.527' AS DateTime), CAST(N'2022-01-22T23:37:47.527' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1038, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:37:48.540' AS DateTime), CAST(N'2022-01-22T23:37:48.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1039, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:27.583' AS DateTime), CAST(N'2022-01-22T23:38:27.583' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1040, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:28.170' AS DateTime), CAST(N'2022-01-22T23:38:28.170' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1041, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:29.310' AS DateTime), CAST(N'2022-01-22T23:38:29.310' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1042, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:30.147' AS DateTime), CAST(N'2022-01-22T23:38:30.147' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1043, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:30.803' AS DateTime), CAST(N'2022-01-22T23:38:30.803' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1044, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:31.290' AS DateTime), CAST(N'2022-01-22T23:38:31.290' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1045, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:31.580' AS DateTime), CAST(N'2022-01-22T23:38:31.580' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1046, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:31.767' AS DateTime), CAST(N'2022-01-22T23:38:31.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1047, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:37.687' AS DateTime), CAST(N'2022-01-22T23:38:37.687' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1048, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:38.490' AS DateTime), CAST(N'2022-01-22T23:38:38.490' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1049, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:39.117' AS DateTime), CAST(N'2022-01-22T23:38:39.117' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1050, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:40.073' AS DateTime), CAST(N'2022-01-22T23:38:40.073' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1051, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:40.980' AS DateTime), CAST(N'2022-01-22T23:38:40.980' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1052, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:42.163' AS DateTime), CAST(N'2022-01-22T23:38:42.163' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1053, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:43.713' AS DateTime), CAST(N'2022-01-22T23:38:43.713' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1054, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:44.663' AS DateTime), CAST(N'2022-01-22T23:38:44.663' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1055, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:45.787' AS DateTime), CAST(N'2022-01-22T23:38:45.787' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1056, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:46.880' AS DateTime), CAST(N'2022-01-22T23:38:46.880' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1057, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:47.793' AS DateTime), CAST(N'2022-01-22T23:38:47.793' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1058, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:48.823' AS DateTime), CAST(N'2022-01-22T23:38:48.823' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1059, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:50.500' AS DateTime), CAST(N'2022-01-22T23:38:50.500' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1060, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:51.853' AS DateTime), CAST(N'2022-01-22T23:38:51.853' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1061, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:53.087' AS DateTime), CAST(N'2022-01-22T23:38:53.087' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1062, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:54.063' AS DateTime), CAST(N'2022-01-22T23:38:54.063' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1063, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:54.900' AS DateTime), CAST(N'2022-01-22T23:38:54.900' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1064, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:55.783' AS DateTime), CAST(N'2022-01-22T23:38:55.783' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1065, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:56.447' AS DateTime), CAST(N'2022-01-22T23:38:56.447' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1066, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:57.100' AS DateTime), CAST(N'2022-01-22T23:38:57.100' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1067, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:57.647' AS DateTime), CAST(N'2022-01-22T23:38:57.647' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1068, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:58.320' AS DateTime), CAST(N'2022-01-22T23:38:58.320' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1069, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:59.010' AS DateTime), CAST(N'2022-01-22T23:38:59.010' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1070, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:38:59.697' AS DateTime), CAST(N'2022-01-22T23:38:59.697' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1071, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:00.363' AS DateTime), CAST(N'2022-01-22T23:39:00.363' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1072, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:01.113' AS DateTime), CAST(N'2022-01-22T23:39:01.113' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1073, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:01.767' AS DateTime), CAST(N'2022-01-22T23:39:01.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1074, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:02.473' AS DateTime), CAST(N'2022-01-22T23:39:02.473' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1075, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:03.383' AS DateTime), CAST(N'2022-01-22T23:39:03.383' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1076, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:03.997' AS DateTime), CAST(N'2022-01-22T23:39:03.997' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1077, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:04.447' AS DateTime), CAST(N'2022-01-22T23:39:04.447' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1078, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:06.767' AS DateTime), CAST(N'2022-01-22T23:39:06.767' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1079, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:08.147' AS DateTime), CAST(N'2022-01-22T23:39:08.147' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1080, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:08.620' AS DateTime), CAST(N'2022-01-22T23:39:08.620' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1081, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:09.080' AS DateTime), CAST(N'2022-01-22T23:39:09.080' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1082, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:09.557' AS DateTime), CAST(N'2022-01-22T23:39:09.557' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1083, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:10.363' AS DateTime), CAST(N'2022-01-22T23:39:10.363' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1084, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:11.197' AS DateTime), CAST(N'2022-01-22T23:39:11.197' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1085, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:11.843' AS DateTime), CAST(N'2022-01-22T23:39:11.843' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1086, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:12.597' AS DateTime), CAST(N'2022-01-22T23:39:12.597' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1087, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:39:13.337' AS DateTime), CAST(N'2022-01-22T23:39:13.337' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1088, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:45:10.737' AS DateTime), CAST(N'2022-01-22T23:45:10.737' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1089, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:14.377' AS DateTime), CAST(N'2022-01-22T23:47:14.377' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1090, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:15.917' AS DateTime), CAST(N'2022-01-22T23:47:15.917' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1091, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:17.980' AS DateTime), CAST(N'2022-01-22T23:47:17.980' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1092, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:21.540' AS DateTime), CAST(N'2022-01-22T23:47:21.540' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1093, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:25.747' AS DateTime), CAST(N'2022-01-22T23:47:25.747' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1094, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:28.777' AS DateTime), CAST(N'2022-01-22T23:47:28.777' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1095, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:31.977' AS DateTime), CAST(N'2022-01-22T23:47:31.977' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1096, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:35.457' AS DateTime), CAST(N'2022-01-22T23:47:35.457' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1097, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:47:37.850' AS DateTime), CAST(N'2022-01-22T23:47:37.850' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1098, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:48:44.890' AS DateTime), CAST(N'2022-01-22T23:48:44.890' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1099, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:48:46.120' AS DateTime), CAST(N'2022-01-22T23:48:46.120' AS DateTime), 0)
GO
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1100, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:48:48.293' AS DateTime), CAST(N'2022-01-22T23:48:48.293' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1101, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:48:57.830' AS DateTime), CAST(N'2022-01-22T23:48:57.830' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1102, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:49:52.193' AS DateTime), CAST(N'2022-01-22T23:49:52.193' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1103, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:49:54.110' AS DateTime), CAST(N'2022-01-22T23:49:54.110' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1104, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:13.017' AS DateTime), CAST(N'2022-01-22T23:52:13.017' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1105, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:16.660' AS DateTime), CAST(N'2022-01-22T23:52:16.660' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1106, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:19.013' AS DateTime), CAST(N'2022-01-22T23:52:19.013' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1107, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:21.197' AS DateTime), CAST(N'2022-01-22T23:52:21.197' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1108, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:23.450' AS DateTime), CAST(N'2022-01-22T23:52:23.450' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1109, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:25.770' AS DateTime), CAST(N'2022-01-22T23:52:25.770' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1110, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:27.890' AS DateTime), CAST(N'2022-01-22T23:52:27.890' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1111, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:29.337' AS DateTime), CAST(N'2022-01-22T23:52:29.337' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1112, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:30.707' AS DateTime), CAST(N'2022-01-22T23:52:30.707' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1113, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:31.740' AS DateTime), CAST(N'2022-01-22T23:52:31.740' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1114, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:33.157' AS DateTime), CAST(N'2022-01-22T23:52:33.157' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1115, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:33.930' AS DateTime), CAST(N'2022-01-22T23:52:33.930' AS DateTime), 0)
INSERT [dbo].[Notification] ([id], [NotificationTypeId], [Subject], [Message], [JsonData], [SourceUserId], [TargetUserId], [Reply], [IsReplyable], [IsHandled], [created], [updated], [isDeleted]) VALUES (1116, 1, N'Bypass Approval Request', N'Please approve checkin bypass', N'{ "LPR": "1122335544", "CCR": "ASDF123456", "ArrivalTime": "12/22/2021 12:34:46 PM"  }', 4, 4, NULL, 1, 0, CAST(N'2022-01-22T23:52:34.857' AS DateTime), CAST(N'2022-01-22T23:52:34.857' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Notification] OFF
GO
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'ADMIN', CAST(1 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'Auditor', CAST(5 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'CheckIn', CAST(9 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'CheckOut', CAST(10 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'ImageAnalyst', CAST(7 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'ManualCheck', CAST(8 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'Operator', CAST(4 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'SCS Engineer', CAST(12 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'SCS Operator', CAST(11 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'SiteManager', CAST(3 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'Statistician', CAST(6 AS Numeric(19, 0)))
INSERT [dbo].[RoleList] ([roleName], [orderId]) VALUES (N'Supervisor', CAST(2 AS Numeric(19, 0)))
GO
SET IDENTITY_INSERT [dbo].[Vehicle] ON 

INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (2, N'1008939', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2022-01-16T13:29:33.710' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (3, N'1020139', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (4, N'1029263', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (5, N'1029458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (6, N'1042639', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (7, N'1046074', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (8, N'1050558', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (9, N'1059839', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (10, N'1059939', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (11, N'1060258', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (12, N'1091333', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (13, N'1093233', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (14, N'1094963', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (15, N'1095433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (16, N'1095633', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (17, N'1096013', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (18, N'1097433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (19, N'1097913', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (20, N'1098733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (21, N'1112139', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (22, N'1130539', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (23, N'1135063', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (24, N'1141839', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (25, N'1155563', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (26, N'1163063', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (27, N'1174639', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (28, N'1175939', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (29, N'1215646', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (30, N'1234568', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (31, N'1243479', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (32, N'1244359', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (33, N'1244369', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (34, N'1248079', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (35, N'1249079', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (36, N'1249679', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (37, N'1251179', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (38, N'1253579', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (39, N'1254579', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (40, N'1254679', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (41, N'1254779', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (42, N'1255079', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (43, N'1266079', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (44, N'1382354', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (45, N'1393254', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (46, N'1400454', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (47, N'1425615', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (48, N'1441058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (49, N'1448858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (50, N'1449358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (51, N'1452658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (52, N'1453058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (53, N'1454558', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (54, N'1455358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (55, N'1455458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (56, N'1455658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (57, N'1459658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (58, N'1462258', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (59, N'1464658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (60, N'1466058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (61, N'1473158', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (62, N'1473558', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (63, N'1479058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (64, N'1479958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (65, N'1487358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (66, N'1487658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (67, N'1488158', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (68, N'1488858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (69, N'1491151', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (70, N'1491158', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (71, N'1498958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (72, N'1507758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (73, N'1519858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (74, N'1520858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (75, N'1523458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (76, N'1523858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (77, N'1526758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (78, N'1527458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (79, N'1528258', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (80, N'1528658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (81, N'1529458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (82, N'1537258', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (83, N'1537458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (84, N'1540658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (85, N'1544858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (86, N'1547058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (87, N'1547958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (88, N'1552858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (89, N'1555658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (90, N'1559039', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (91, N'1561858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (92, N'1562039', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (93, N'1563458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (94, N'1564458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (95, N'1564958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (96, N'1573658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (97, N'1577058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (98, N'1581858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (99, N'1589058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (100, N'1589558', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (101, N'1592256', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (102, N'1596958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (103, N'1606712', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (104, N'1628812', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (105, N'1655812', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (106, N'1665912', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (107, N'1687358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (108, N'1851413', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (109, N'1863513', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (110, N'1883513', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (111, N'1890013', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (112, N'1935015', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (113, N'2038464', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (114, N'2074364', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (115, N'2110370', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (116, N'2121770', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (117, N'2127970', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (118, N'2129370', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (119, N'2131270', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (120, N'2134670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (121, N'2135270', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (122, N'2150670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (123, N'2155170', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (124, N'2155370', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (125, N'2156370', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (126, N'2159370', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (127, N'2174670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (128, N'2178970', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (129, N'2186870', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (130, N'2190870', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (131, N'2205570', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (132, N'2207770', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (133, N'2521472', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (134, N'2523172', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (135, N'2572339', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (136, N'2572439', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (137, N'2577439', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (138, N'2580039', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (139, N'2593139', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (140, N'2593739', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (141, N'2593939', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (142, N'2601839', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (143, N'2607839', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (144, N'2610639', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (145, N'2643939', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (146, N'2647839', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (147, N'2650839', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (148, N'2721886', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (149, N'2779758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (150, N'2787358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (151, N'2791958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (152, N'2794158', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (153, N'2799758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (154, N'2814758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (155, N'2815058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (156, N'2820058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (157, N'2821058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (158, N'2823358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (159, N'2825558', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (160, N'2835858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (161, N'2836758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (162, N'2837058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (163, N'2840058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (164, N'2840158', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (165, N'2840958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (166, N'2861072', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (167, N'2871672', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (168, N'2882115', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (169, N'2895676', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (170, N'2899576', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (171, N'2907176', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (172, N'2909176', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (173, N'2914058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (174, N'2914376', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (175, N'2915558', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (176, N'2941058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (177, N'2943058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (178, N'2947276', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (179, N'2948658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (180, N'2950276', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (181, N'2950376', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (182, N'2955976', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (183, N'2956176', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (184, N'2964676', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (185, N'2970376', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (186, N'3135958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (187, N'3136701', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (188, N'3137378', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (189, N'3143458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (190, N'3143658', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (191, N'3143672', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (192, N'3143958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (193, N'3145858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (194, N'3149758', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (195, N'3150578', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (196, N'3153358', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (197, N'3155158', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (198, N'3155858', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (199, N'3159458', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (200, N'3161772', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (201, N'3167578', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (202, N'3168178', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (203, N'3172878', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (204, N'3174278', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (205, N'3174672', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (206, N'3178078', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (207, N'3178278', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (208, N'3186778', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (209, N'3187078', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (210, N'3195378', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (211, N'3199278', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (212, N'3200772', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (213, N'3201572', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (214, N'3202472', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (215, N'3209272', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (216, N'3252272', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (217, N'3284972', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (218, N'3292372', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (219, N'3424289', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (220, N'3451389', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (221, N'3453089', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (222, N'3453189', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (223, N'3518014', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (224, N'3629478', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (225, N'3631978', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (226, N'3635878', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (227, N'3636478', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (228, N'3640872', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (229, N'3653478', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (230, N'3670672', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (231, N'3672451', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (232, N'3673178', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (233, N'3677578', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (234, N'3678151', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (235, N'3679256', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (236, N'3683278', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (237, N'3691078', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (238, N'3695778', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (239, N'3707256', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (240, N'3721178', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (241, N'3721278', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (242, N'3721578', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (243, N'3723178', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (244, N'3739478', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (245, N'3750156', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (246, N'3750256', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (247, N'3754255', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (248, N'3755155', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (249, N'3762455', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (250, N'3766255', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (251, N'3772455', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (252, N'3772655', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (253, N'3787055', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (254, N'3787256', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (255, N'3790855', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (256, N'3791855', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (257, N'3809955', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (258, N'3809959', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (259, N'3888880', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (260, N'3927515', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (261, N'4017764', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (262, N'4025972', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (263, N'4232515', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (264, N'4232615', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (265, N'4301008', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (266, N'4303130', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (267, N'4306708', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (268, N'4310630', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (269, N'4311630', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (270, N'4315830', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (271, N'4321208', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (272, N'4328530', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (273, N'4332430', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (274, N'4336930', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (275, N'4343408', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (276, N'4353270', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (277, N'4357930', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (278, N'4377008', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (279, N'4385708', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (280, N'4388208', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (281, N'4389408', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (282, N'4390231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (283, N'4391808', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (284, N'4392708', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (285, N'4398908', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (286, N'4399308', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (287, N'4399408', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (288, N'4470115', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (289, N'4517368', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (290, N'4549268', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (291, N'4557268', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (292, N'4557968', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (293, N'4562768', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (294, N'4566168', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (295, N'4567268', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (296, N'4569868', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (297, N'4577368', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (298, N'4584368', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (299, N'4593868', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (300, N'4662768', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (301, N'4853831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (302, N'4859631', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (303, N'4926473', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (304, N'4932531', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (305, N'4932731', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (306, N'4932831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (307, N'4936173', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (308, N'4948631', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (309, N'4957531', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (310, N'4959873', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (311, N'4960173', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (312, N'4964931', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (313, N'4978931', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (314, N'4980731', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (315, N'4980931', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (316, N'4981431', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (317, N'4982073', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (318, N'4985231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (319, N'4985273', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (320, N'4990131', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (321, N'4990231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (322, N'4993031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (323, N'4997331', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (324, N'5018173', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (325, N'5019773', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (326, N'5029373', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (327, N'5036473', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (328, N'5064115', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (329, N'5076673', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (330, N'5077973', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (331, N'5078373', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (332, N'5078378', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (333, N'5078379', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (334, N'5209574', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (335, N'5237350', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (336, N'5372668', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (337, N'5381261', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (338, N'5381268', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (339, N'53812?8', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (340, N'5381668', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (341, N'5392168', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (342, N'5392468', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (343, N'5394968', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (344, N'5395958', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (345, N'5395967', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (346, N'5395968', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (347, N'5395969', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (348, N'5433768', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (349, N'5437468', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (350, N'5447468', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (351, N'5454515', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (352, N'5458168', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (353, N'5461368', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (354, N'5480768', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (355, N'5490168', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (356, N'5494168', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (357, N'5503768', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (358, N'5507168', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (359, N'5518868', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (360, N'5519068', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (361, N'5521868', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (362, N'5523668', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (363, N'5526478', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (364, N'5531978', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (365, N'5532078', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (366, N'5532079', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (367, N'5535478', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (368, N'5541678', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (369, N'5633831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (370, N'5638131', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (371, N'5640669', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (372, N'5642331', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (373, N'5646531', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (374, N'5649660', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (375, N'5649860', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (376, N'5650031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (377, N'5650033', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (378, N'5660231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (379, N'5663631', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (380, N'5665860', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (381, N'5671669', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (382, N'5672560', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (383, N'5673831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (384, N'5677131', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (385, N'5685231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (386, N'5685831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (387, N'5694431', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (388, N'5700260', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (389, N'5706160', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (390, N'5830414', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (391, N'5888952', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (392, N'5911261', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (393, N'5983374', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (394, N'5992174', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (395, N'6043113', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (396, N'6047313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (397, N'6053813', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (398, N'6054913', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (399, N'6055013', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (400, N'6055913', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (401, N'6059113', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (402, N'6062713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (403, N'6063913', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (404, N'6066213', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (405, N'6073513', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (406, N'6073713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (407, N'6074013', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (408, N'6075913', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (409, N'6084313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (410, N'6088913', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (411, N'6089613', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (412, N'6089813', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (413, N'6092613', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (414, N'6092713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (415, N'6095713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (416, N'6099713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (417, N'6106113', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (418, N'6107813', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (419, N'6110713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (420, N'6111715', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (421, N'6113313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (422, N'6117813', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (423, N'6118313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (424, N'6121213', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (425, N'6122213', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (426, N'6122313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (427, N'6123070', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (428, N'6123513', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (429, N'6131113', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (430, N'6134970', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (431, N'6136770', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (432, N'6146070', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (433, N'6146870', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (434, N'6158870', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (435, N'6186672', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (436, N'6188472', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (437, N'6193072', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (438, N'6269363', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (439, N'6274963', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (440, N'6278463', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (441, N'6295263', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (442, N'6305031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (443, N'6307031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (444, N'6307037', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (445, N'6308031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (446, N'6310331', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (447, N'6312531', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (448, N'6312631', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (449, N'6312731', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (450, N'6319031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (451, N'6319931', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (452, N'6320331', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (453, N'6321331', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (454, N'6324631', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (455, N'6325831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (456, N'6336131', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (457, N'6337831', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (458, N'6339331', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (459, N'6349431', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (460, N'6357061', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (461, N'6365231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (462, N'6370431', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (463, N'6370568', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (464, N'6373131', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (465, N'6379031', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (466, N'6385068', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (467, N'6388468', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (468, N'6388931', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (469, N'6389231', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (470, N'6392668', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (471, N'6412068', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (472, N'6510661', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (473, N'6613915', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (474, N'6679232', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (475, N'6685632', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (476, N'6709370', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (477, N'6709570', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (478, N'6717032', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (479, N'6717570', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (480, N'6718032', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (481, N'6722670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (482, N'6723070', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (483, N'6725970', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (484, N'6731670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (485, N'6731870', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (486, N'6736470', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (487, N'6737232', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (488, N'6737332', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (489, N'6737532', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (490, N'6737632', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (491, N'6749770', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (492, N'6756670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (493, N'6756970', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (494, N'6764232', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (495, N'6764870', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (496, N'6765070', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (497, N'6765332', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (498, N'6766270', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (499, N'6774132', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (500, N'6803532', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (501, N'6845915', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (502, N'6853589', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (503, N'6875961', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (504, N'6877261', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (505, N'6888356', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (506, N'6890972', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (507, N'6894372', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (508, N'6994733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (509, N'6996734', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (510, N'6996834', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (511, N'7009433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (512, N'7016534', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (513, N'7040070', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (514, N'7052733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (515, N'7074233', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (516, N'7079033', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (517, N'7134570', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (518, N'7134670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (519, N'7213770', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (520, N'7220770', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (521, N'7221570', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (522, N'7226270', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (523, N'7227470', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (524, N'7238967', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (525, N'7243470', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (526, N'7255970', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (527, N'7260387', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (528, N'7268367', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (529, N'7274267', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (530, N'7275287', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (531, N'7278670', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (532, N'7286687', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (533, N'7288387', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (534, N'7291567', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (535, N'7292987', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (536, N'7293467', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (537, N'7295270', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (538, N'7297170', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (539, N'7299787', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (540, N'7300707', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (541, N'7334867', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (542, N'7338867', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (543, N'7356052', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (544, N'7356352', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (545, N'7356452', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (546, N'7357652', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (547, N'7362352', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (548, N'7365852', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (549, N'7366452', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (550, N'7366952', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (551, N'7367087', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (552, N'7367252', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (553, N'7367287', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (554, N'7368752', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (555, N'7370187', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (556, N'7372452', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (557, N'7373252', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (558, N'7373887', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (559, N'7380552', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (560, N'7380952', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (561, N'7383752', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (562, N'7386352', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (563, N'7386452', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (564, N'7403687', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (565, N'7420070', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (566, N'7426634', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (567, N'7427734', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (568, N'7432334', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (569, N'7433687', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (570, N'7450552', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (571, N'7453687', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (572, N'7458687', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (573, N'7459552', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (574, N'7473411', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (575, N'7473412', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (576, N'7473414', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (577, N'7485752', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (578, N'7489552', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (579, N'7490052', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (580, N'7495112', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (581, N'7495152', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (582, N'7495212', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (583, N'7495312', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (584, N'7507012', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (585, N'7508412', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (586, N'7508512', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (587, N'7708813', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (588, N'7710480', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (589, N'7718032', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (590, N'7725213', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (591, N'7725313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (592, N'7739613', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (593, N'7740413', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (594, N'7750113', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (595, N'7756713', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (596, N'7769613', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (597, N'7776313', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (598, N'7832983', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (599, N'7840058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (600, N'7941058', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (601, N'7950867', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (602, N'7951467', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (603, N'7953567', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (604, N'7956176', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (605, N'8003215', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (606, N'8040557', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (607, N'8049614', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (608, N'8049814', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (609, N'8049914', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (610, N'8056252', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (611, N'8103780', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (612, N'8107880', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (613, N'8108980', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (614, N'8110080', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (615, N'8302580', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (616, N'8303780', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (617, N'8306080', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (618, N'8308259', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (619, N'8308480', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (620, N'8316253', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (621, N'8349859', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (622, N'8366859', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (623, N'8451352', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (624, N'8451357', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (625, N'8467852', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (626, N'8503052', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (627, N'8503580', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (628, N'8503852', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (629, N'8505880', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (630, N'8509080', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (631, N'8524352', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (632, N'8529176', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (633, N'8540752', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (634, N'8545552', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (635, N'8550652', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (636, N'8579632', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (637, N'8584252', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (638, N'8586652', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (639, N'8586852', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (640, N'8588952', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (641, N'8589552', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (642, N'8589652', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (643, N'8595652', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (644, N'8607752', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (645, N'8607852', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (646, N'8607952', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (647, N'8621133', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (648, N'8624000', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (649, N'8631633', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (650, N'8639433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (651, N'8649033', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (652, N'8649833', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (653, N'8650133', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (654, N'8650333', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (655, N'8652133', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (656, N'8653533', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (657, N'8656733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (658, N'8658533', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (659, N'8664833', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (660, N'8675533', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (661, N'8676733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (662, N'8679952', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (663, N'8925733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (664, N'8925833', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (665, N'8935933', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (666, N'8938933', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (667, N'8947033', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (668, N'8950084', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (669, N'8962633', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (670, N'8969433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (671, N'8970833', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (672, N'8984033', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (673, N'8997972', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (674, N'8998072', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (675, N'9037115', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (676, N'90371?5', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (677, N'9178469', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (678, N'9185433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (679, N'9186533', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (680, N'9190233', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (681, N'9191233', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (682, N'9192633', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (683, N'9192733', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (684, N'9212833', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (685, N'9219433', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (686, N'9223033', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (687, N'9231633', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (688, N'9493200', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (689, N'9506067', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (690, N'9521067', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (691, N'9538067', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (692, N'9607752', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (693, N'9744286', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (694, N'9752086', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (695, N'9762386', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (696, N'9776786', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (697, N'9789885', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (698, N'9795486', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (699, N'9935012', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (700, N'9936212', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
GO
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (701, N'9936312', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (702, N'9939612', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (703, N'9942212', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (704, N'9945112', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (705, N'9945512', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (706, N'9947212', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (707, N'9955312', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (708, N'9961812', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (709, N'9963512', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (710, N'9966712', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (711, N'9982812', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (712, N'9986312', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (713, N'9986812', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (714, N'9987212', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (715, N'9991212', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (716, N'9991912', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (717, N'9997512', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
INSERT [dbo].[Vehicle] ([id], [LicensePlateNumber], [VehicleTypeId], [Remark], [created], [updated], [isDeleted]) VALUES (718, N'XXXXXXX', 1, N'Imported', CAST(N'2021-12-30T10:19:59.520' AS DateTime), CAST(N'2021-12-30T10:19:59.520' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Vehicle] OFF
GO
SET IDENTITY_INSERT [dbo].[WorkStation] ON 

INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (3, N'CIA1 managment system', N'Managment system work station at check In 1', N'192.111.111.111', N'34:17:EB:D9:4F:12', 2, 1, CAST(N'2021-12-23T12:14:32.947' AS DateTime), CAST(N'2022-01-13T18:35:39.507' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (4, N'CIA2 managment system', N'Managment system work station at check In 2', N'192.111.111.112', N'34:17:EB:D9:4F:6A', 2, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-26T10:46:47.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (5, N'SCA1 managment system', N'Managment system work station at old X-Ray area.', N'192.111.111.113', N'34:17:EB:D9:4F:A6', 2, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-26T10:48:07.920' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (6, N'MCA1 managment system', N'Managment system work station at old manual check area', N'192.111.111.116', N'34:17:EB:D9:4D:27', 2, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-26T10:46:10.063' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (7, N'COA1 managment system', N'Managment system work station at old check out area', N'192.111.111.120', N'34:17:EB:D9:50:96', 2, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-26T14:05:54.723' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (8, N'MCW1', N'Manual check system (IPS - Image processing system) work station at old manual check area (Office)', N'192.111.111.124', N'98:90:96:B6:DC:E8', 3, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (9, N'IAW1', N'X-Ray image analyzer system (IPS - Image processing system) 1 of 3 work station at (after) old X-Ray area', N'192.111.111.20', N'F0:1F:AF:46:D1:E3', 1, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (10, N'MCA1 managment system', N'Managment system work station at old manual check area', N'192.111.111.200', N'34:17:EB:D9:50:BD', 2, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (11, N'IAW2', N'X-Ray image analyzer system (IPS - Image processing system) 2 of 3 work station at (after) old X-Ray area', N'192.111.111.21', N'98:90:96:B6:DE:59', 1, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (12, N'IAW3', N'X-Ray image analyzer system (IPS - Image processing system) 3 of 3 work station at (after) old X-Ray area', N'192.111.111.22', N'98:90:96:B6:DE:59', 1, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (13, N'IAW4 (manager)', N'Manager X-Ray image analyzer system (IPS - Image processing system) 3 of 3 work station at (after) old X-Ray area', N'192.111.111.230', N'00:00:00:00:00:00', 1, 0, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (14, N'IAW5 (manager)', N'Manager X-Ray image analyzer system (IPS - Image processing system) 3 of 3 work station at (after) old X-Ray area', N'192.111.111.33', N'98:90:96:B6:DC:E8', 1, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (15, N'MCW2', N'Manual check system (IPS - Image processing system) work station at old manual check area (PIT)', N'192.111.111.25', N'98:90:96:B6:C5:9E', 3, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (16, N'MCW3', N'Manual check system (IPS - Image processing system) work station at old manual check area (UNKNOWN)', N'192.111.111.26', N'98:90:96:B6:DF:C2', 3, 1, CAST(N'2021-12-23T12:15:17.687' AS DateTime), CAST(N'2021-12-23T12:15:17.687' AS DateTime), 0)
INSERT [dbo].[WorkStation] ([id], [Name], [Description], [IP], [MAC], [WorkStationTypeId], [IsActive], [created], [updated], [isDeleted]) VALUES (19, N'SCS Scanner system', N'Scanner system at old X-Ray area', N'190.111.111.7', N'98:90:96:B6:DC:E8', 4, 1, CAST(N'2021-12-23T12:42:29.533' AS DateTime), CAST(N'2021-12-23T12:42:29.533' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[WorkStation] OFF
GO
SET IDENTITY_INSERT [mgmt].[TableDisplayedColumns] ON 

INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (10, N'auth.User', N'id', 1, 0, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (11, N'auth.User', N'FirstName', 3, 1, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (12, N'auth.User', N'LastName', 2, 1, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (13, N'auth.User', N'Email', 4, 1, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (14, N'auth.User', N'Password', 5, 0, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (15, N'auth.User', N'created', 6, 1, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (16, N'auth.User', N'updated', 7, 1, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (17, N'auth.User', N'isDeleted', 8, 0, CAST(N'2021-12-19T12:29:58.470' AS DateTime), CAST(N'2021-12-19T12:29:58.470' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (18, N'auth.User', N'Actions', 9, 1, CAST(N'2021-12-19T13:12:06.140' AS DateTime), CAST(N'2021-12-19T13:12:06.140' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (19, N'dbo.WorkStation', N'Id', 1, 0, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (20, N'dbo.WorkStation', N'Name', 2, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (21, N'dbo.WorkStation', N'Description', 3, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (22, N'dbo.WorkStation', N'IP', 4, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (23, N'dbo.WorkStation', N'MAC', 5, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (24, N'dbo.WorkStation', N'WorkStationType', 6, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (25, N'dbo.WorkStation', N'IsActive', 8, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (26, N'dbo.WorkStation', N'Created', 9, 0, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (27, N'dbo.WorkStation', N'Updated', 10, 0, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (28, N'dbo.WorkStation', N'IsDeleted', 11, 0, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (29, N'dbo.WorkStation', N'Actions', 12, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (30, N'dbo.WorkStation', N'Type Description', 7, 1, CAST(N'2021-12-23T13:18:19.010' AS DateTime), CAST(N'2021-12-23T13:18:19.010' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (31, N'dbo.inspection', N'Id', 1, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (32, N'dbo.inspection', N'VehicleLicencePlate', 3, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (33, N'dbo.inspection', N'WorkflowType', 4, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (34, N'dbo.inspection', N'LPR', 5, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (35, N'dbo.inspection', N'LPRCorrectionType', 6, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (36, N'dbo.inspection', N'CCR', 7, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (37, N'dbo.inspection', N'CCRCorrectionType', 8, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (38, N'dbo.inspection', N'ManifestCode', 9, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (39, N'dbo.inspection', N'DriverTagCode', 10, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (40, N'dbo.inspection', N'ConclusionType', 11, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (41, N'dbo.inspection', N'Arrival', 12, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (42, N'dbo.inspection', N'Leave', 13, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (43, N'dbo.inspection', N'End', 14, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (44, N'dbo.inspection', N'Remark', 15, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (45, N'dbo.inspection', N'SourceInspectionIdentifier', 16, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (46, N'dbo.inspection', N'SourceTable', 17, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (47, N'dbo.inspection', N'SourceId', 18, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (48, N'dbo.inspection', N'Created', 19, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (49, N'dbo.inspection', N'Updated', 20, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (50, N'dbo.inspection', N'IsDeleted', 21, 0, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (51, N'dbo.inspection', N'Actions', 22, 1, CAST(N'2022-01-11T08:05:25.080' AS DateTime), CAST(N'2022-01-11T08:05:25.080' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (57, N'dbo.inspection', N'Identifier', 2, 1, CAST(N'2022-01-11T16:09:49.830' AS DateTime), CAST(N'2022-01-11T16:09:49.830' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (58, N'type.WorkStationType', N'Id', 1, 1, CAST(N'2022-01-12T16:58:25.367' AS DateTime), CAST(N'2022-01-12T16:58:25.367' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (59, N'type.WorkStationType', N'Name', 2, 1, CAST(N'2022-01-12T16:58:25.367' AS DateTime), CAST(N'2022-01-12T16:58:25.367' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (60, N'type.WorkStationType', N'Description', 3, 1, CAST(N'2022-01-12T16:58:25.367' AS DateTime), CAST(N'2022-01-12T16:58:25.367' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (61, N'type.WorkStationType', N'Created', 4, 1, CAST(N'2022-01-12T16:58:25.367' AS DateTime), CAST(N'2022-01-12T16:58:25.367' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (62, N'type.WorkStationType', N'Updated', 5, 1, CAST(N'2022-01-12T16:58:25.367' AS DateTime), CAST(N'2022-01-12T16:58:25.367' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (63, N'type.WorkStationType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T16:58:25.367' AS DateTime), CAST(N'2022-01-12T16:58:25.367' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (64, N'type.WorkStationType', N'Actions', 7, 1, CAST(N'2022-01-12T16:58:25.370' AS DateTime), CAST(N'2022-01-12T16:58:25.370' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (65, N'type.VehicleType', N'Id', 1, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (66, N'type.VehicleType', N'Name', 2, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (67, N'type.VehicleType', N'Description', 3, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (68, N'type.VehicleType', N'Created', 4, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (69, N'type.VehicleType', N'Updated', 5, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (70, N'type.VehicleType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (71, N'type.VehicleType', N'Actions', 7, 1, CAST(N'2022-01-12T18:47:59.203' AS DateTime), CAST(N'2022-01-12T18:47:59.203' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (77, N'type.DeviceType', N'Id', 1, 1, CAST(N'2022-01-12T18:54:15.927' AS DateTime), CAST(N'2022-01-12T18:54:15.927' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (78, N'type.DeviceType', N'Name', 2, 1, CAST(N'2022-01-12T18:54:15.927' AS DateTime), CAST(N'2022-01-12T18:54:15.927' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (79, N'type.DeviceType', N'Description', 3, 1, CAST(N'2022-01-12T18:54:15.927' AS DateTime), CAST(N'2022-01-12T18:54:15.927' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (80, N'type.DeviceType', N'Created', 4, 1, CAST(N'2022-01-12T18:54:15.927' AS DateTime), CAST(N'2022-01-12T18:54:15.927' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (81, N'type.DeviceType', N'Updated', 5, 1, CAST(N'2022-01-12T18:54:15.927' AS DateTime), CAST(N'2022-01-12T18:54:15.927' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (82, N'type.DeviceType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:54:15.927' AS DateTime), CAST(N'2022-01-12T18:54:15.927' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (83, N'type.DeviceType', N'Actions', 7, 1, CAST(N'2022-01-12T18:54:15.930' AS DateTime), CAST(N'2022-01-12T18:54:15.930' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (84, N'type.DeviceStatusType', N'Id', 1, 1, CAST(N'2022-01-12T18:54:50.320' AS DateTime), CAST(N'2022-01-12T18:54:50.320' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (85, N'type.DeviceStatusType', N'Name', 2, 1, CAST(N'2022-01-12T18:54:50.320' AS DateTime), CAST(N'2022-01-12T18:54:50.320' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (86, N'type.DeviceStatusType', N'Description', 3, 1, CAST(N'2022-01-12T18:54:50.320' AS DateTime), CAST(N'2022-01-12T18:54:50.320' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (87, N'type.DeviceStatusType', N'Created', 4, 1, CAST(N'2022-01-12T18:54:50.320' AS DateTime), CAST(N'2022-01-12T18:54:50.320' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (88, N'type.DeviceStatusType', N'Updated', 5, 1, CAST(N'2022-01-12T18:54:50.320' AS DateTime), CAST(N'2022-01-12T18:54:50.320' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (89, N'type.DeviceStatusType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:54:50.320' AS DateTime), CAST(N'2022-01-12T18:54:50.320' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (90, N'type.DeviceStatusType', N'Actions', 7, 1, CAST(N'2022-01-12T18:54:50.323' AS DateTime), CAST(N'2022-01-12T18:54:50.323' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (91, N'type.DeviceEventType', N'Id', 1, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (92, N'type.DeviceEventType', N'Name', 2, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (93, N'type.DeviceEventType', N'Description', 3, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (94, N'type.DeviceEventType', N'Created', 4, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (95, N'type.DeviceEventType', N'Updated', 5, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (96, N'type.DeviceEventType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (97, N'type.DeviceEventType', N'Actions', 7, 1, CAST(N'2022-01-12T18:55:03.480' AS DateTime), CAST(N'2022-01-12T18:55:03.480' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (100, N'type.DataCorrectionType', N'Id', 1, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (101, N'type.DataCorrectionType', N'Name', 2, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (102, N'type.DataCorrectionType', N'Description', 3, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (103, N'type.DataCorrectionType', N'Created', 4, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (104, N'type.DataCorrectionType', N'Updated', 5, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (105, N'type.DataCorrectionType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (106, N'type.DataCorrectionType', N'Actions', 7, 1, CAST(N'2022-01-12T18:55:43.540' AS DateTime), CAST(N'2022-01-12T18:55:43.540' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (107, N'type.ConclusionType', N'Id', 1, 1, CAST(N'2022-01-12T18:55:58.030' AS DateTime), CAST(N'2022-01-12T18:55:58.030' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (108, N'type.ConclusionType', N'Name', 2, 1, CAST(N'2022-01-12T18:55:58.030' AS DateTime), CAST(N'2022-01-12T18:55:58.030' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (109, N'type.ConclusionType', N'Description', 3, 1, CAST(N'2022-01-12T18:55:58.030' AS DateTime), CAST(N'2022-01-12T18:55:58.030' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (110, N'type.ConclusionType', N'Created', 4, 1, CAST(N'2022-01-12T18:55:58.030' AS DateTime), CAST(N'2022-01-12T18:55:58.030' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (111, N'type.ConclusionType', N'Updated', 5, 1, CAST(N'2022-01-12T18:55:58.030' AS DateTime), CAST(N'2022-01-12T18:55:58.030' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (112, N'type.ConclusionType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:55:58.030' AS DateTime), CAST(N'2022-01-12T18:55:58.030' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (113, N'type.ConclusionType', N'Actions', 7, 1, CAST(N'2022-01-12T18:55:58.033' AS DateTime), CAST(N'2022-01-12T18:55:58.033' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (121, N'type.AreaType', N'Id', 1, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (122, N'type.AreaType', N'Name', 2, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (123, N'type.AreaType', N'Description', 3, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (124, N'type.AreaType', N'Created', 4, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (125, N'type.AreaType', N'Updated', 5, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (126, N'type.AreaType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (127, N'type.AreaType', N'Actions', 7, 1, CAST(N'2022-01-12T18:56:26.060' AS DateTime), CAST(N'2022-01-12T18:56:26.060' AS DateTime), 0)
GO
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (129, N'type.InspectionWorkflowType', N'Id', 1, 1, CAST(N'2022-01-12T18:57:07.397' AS DateTime), CAST(N'2022-01-12T18:57:07.397' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (130, N'type.InspectionWorkflowType', N'Name', 2, 1, CAST(N'2022-01-12T18:57:07.397' AS DateTime), CAST(N'2022-01-12T18:57:07.397' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (131, N'type.InspectionWorkflowType', N'Description', 3, 1, CAST(N'2022-01-12T18:57:07.397' AS DateTime), CAST(N'2022-01-12T18:57:07.397' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (132, N'type.InspectionWorkflowType', N'Created', 4, 1, CAST(N'2022-01-12T18:57:07.397' AS DateTime), CAST(N'2022-01-12T18:57:07.397' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (133, N'type.InspectionWorkflowType', N'Updated', 5, 1, CAST(N'2022-01-12T18:57:07.397' AS DateTime), CAST(N'2022-01-12T18:57:07.397' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (134, N'type.InspectionWorkflowType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:57:07.397' AS DateTime), CAST(N'2022-01-12T18:57:07.397' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (135, N'type.InspectionWorkflowType', N'Actions', 7, 1, CAST(N'2022-01-12T18:57:07.400' AS DateTime), CAST(N'2022-01-12T18:57:07.400' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (136, N'type.InspectionHistorySourceType', N'Id', 1, 1, CAST(N'2022-01-12T18:57:19.413' AS DateTime), CAST(N'2022-01-12T18:57:19.413' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (137, N'type.InspectionHistorySourceType', N'Name', 2, 1, CAST(N'2022-01-12T18:57:19.413' AS DateTime), CAST(N'2022-01-12T18:57:19.413' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (138, N'type.InspectionHistorySourceType', N'Description', 3, 1, CAST(N'2022-01-12T18:57:19.413' AS DateTime), CAST(N'2022-01-12T18:57:19.413' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (139, N'type.InspectionHistorySourceType', N'Created', 4, 1, CAST(N'2022-01-12T18:57:19.413' AS DateTime), CAST(N'2022-01-12T18:57:19.413' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (140, N'type.InspectionHistorySourceType', N'Updated', 5, 1, CAST(N'2022-01-12T18:57:19.413' AS DateTime), CAST(N'2022-01-12T18:57:19.413' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (141, N'type.InspectionHistorySourceType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:57:19.413' AS DateTime), CAST(N'2022-01-12T18:57:19.413' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (142, N'type.InspectionHistorySourceType', N'Actions', 7, 1, CAST(N'2022-01-12T18:57:19.417' AS DateTime), CAST(N'2022-01-12T18:57:19.417' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (143, N'type.InspectionDataType', N'Id', 1, 1, CAST(N'2022-01-12T18:57:30.453' AS DateTime), CAST(N'2022-01-12T18:57:30.453' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (144, N'type.InspectionDataType', N'Name', 2, 1, CAST(N'2022-01-12T18:57:30.453' AS DateTime), CAST(N'2022-01-12T18:57:30.453' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (145, N'type.InspectionDataType', N'Description', 3, 1, CAST(N'2022-01-12T18:57:30.453' AS DateTime), CAST(N'2022-01-12T18:57:30.453' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (146, N'type.InspectionDataType', N'Created', 4, 1, CAST(N'2022-01-12T18:57:30.453' AS DateTime), CAST(N'2022-01-12T18:57:30.453' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (147, N'type.InspectionDataType', N'Updated', 5, 1, CAST(N'2022-01-12T18:57:30.453' AS DateTime), CAST(N'2022-01-12T18:57:30.453' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (148, N'type.InspectionDataType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:57:30.453' AS DateTime), CAST(N'2022-01-12T18:57:30.453' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (149, N'type.InspectionDataType', N'Actions', 7, 1, CAST(N'2022-01-12T18:57:30.457' AS DateTime), CAST(N'2022-01-12T18:57:30.457' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (150, N'type.DeviceCommandSourceType', N'Id', 1, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (151, N'type.DeviceCommandSourceType', N'Name', 2, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (152, N'type.DeviceCommandSourceType', N'Description', 3, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (153, N'type.DeviceCommandSourceType', N'Created', 4, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (154, N'type.DeviceCommandSourceType', N'Updated', 5, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (155, N'type.DeviceCommandSourceType', N'IsDeleted', 6, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (156, N'type.DeviceCommandSourceType', N'Actions', 7, 1, CAST(N'2022-01-12T18:57:38.673' AS DateTime), CAST(N'2022-01-12T18:57:38.673' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (157, N'dbo.Vehicle', N'Id', 1, 0, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (158, N'dbo.Vehicle', N'LicensePlateNumber', 2, 1, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (159, N'dbo.Vehicle', N'VehicleTypeId', 3, 0, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (160, N'dbo.Vehicle', N'Remark', 6, 1, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (161, N'dbo.Vehicle', N'Created', 7, 1, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (162, N'dbo.Vehicle', N'Updated', 8, 1, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (163, N'dbo.Vehicle', N'IsDeleted', 9, 0, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (164, N'dbo.Vehicle', N'Actions', 10, 1, CAST(N'2022-01-16T11:01:29.497' AS DateTime), CAST(N'2022-01-16T11:01:29.497' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (165, N'dbo.Vehicle', N'VehicleType', 4, 1, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (166, N'dbo.Vehicle', N'VehicleTypeDescription', 5, 0, CAST(N'2022-01-16T11:01:29.493' AS DateTime), CAST(N'2022-01-16T11:01:29.493' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (167, N'type.NotificationType', N'Id', 1, 1, CAST(N'2022-01-17T13:38:50.737' AS DateTime), CAST(N'2022-01-17T13:38:50.737' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (168, N'type.NotificationType', N'Name', 2, 1, CAST(N'2022-01-17T13:38:50.737' AS DateTime), CAST(N'2022-01-17T13:38:50.737' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (169, N'type.NotificationType', N'Description', 3, 1, CAST(N'2022-01-17T13:38:50.737' AS DateTime), CAST(N'2022-01-17T13:38:50.737' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (170, N'type.NotificationType', N'Created', 4, 1, CAST(N'2022-01-17T13:38:50.737' AS DateTime), CAST(N'2022-01-17T13:38:50.737' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (171, N'type.NotificationType', N'Updated', 5, 1, CAST(N'2022-01-17T13:38:50.737' AS DateTime), CAST(N'2022-01-17T13:38:50.737' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (172, N'type.NotificationType', N'IsDeleted', 6, 1, CAST(N'2022-01-17T13:38:50.737' AS DateTime), CAST(N'2022-01-17T13:38:50.737' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (173, N'type.NotificationType', N'Actions', 7, 1, CAST(N'2022-01-17T13:38:50.740' AS DateTime), CAST(N'2022-01-17T13:38:50.740' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (175, N'dbo.Notification', N'Id', 1, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (176, N'dbo.Notification', N'NotificationTypeId', 2, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (177, N'dbo.Notification', N'Message', 5, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (178, N'dbo.Notification', N'JsonData', 6, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (179, N'dbo.Notification', N'SourceUserId', 7, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (180, N'dbo.Notification', N'TargetUserId', 10, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (181, N'dbo.Notification', N'Reply', 13, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (182, N'dbo.Notification', N'IsHandled', 15, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (183, N'dbo.Notification', N'Created', 16, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (184, N'dbo.Notification', N'Updated', 17, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (185, N'dbo.Notification', N'IsDeleted', 18, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (186, N'dbo.Notification', N'Actions', 19, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (187, N'dbo.Notification', N'NotificationType', 3, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (188, N'dbo.Notification', N'SourceUserFullName', 7, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (189, N'dbo.Notification', N'SourceUserEmail', 9, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (190, N'dbo.Notification', N'TargetUserFullName', 11, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (191, N'dbo.Notification', N'TargetUserEmail', 12, 0, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (192, N'dbo.Notification', N'IsReplyable', 14, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
INSERT [mgmt].[TableDisplayedColumns] ([id], [TableName], [ColumnName], [DisplayOrder], [visiable], [created], [updated], [isDeleted]) VALUES (193, N'dbo.Notification', N'Subject', 4, 1, CAST(N'2022-01-17T15:16:59.860' AS DateTime), CAST(N'2022-01-17T15:16:59.860' AS DateTime), 0)
SET IDENTITY_INSERT [mgmt].[TableDisplayedColumns] OFF
GO
SET IDENTITY_INSERT [type].[AreaType] ON 

INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (0, N'Default', N'Used as default value', CAST(N'2021-12-27T16:38:22.290' AS DateTime), CAST(N'2022-01-17T11:20:20.487' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'BIA', N'Befor Checkin', CAST(N'2021-12-13T15:45:34.047' AS DateTime), CAST(N'2021-12-13T15:45:34.047' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'BOA', N'Befor Checkout', CAST(N'2021-12-13T15:45:37.600' AS DateTime), CAST(N'2021-12-13T15:45:37.600' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'CIA', N'Check In Area', CAST(N'2021-12-13T15:45:52.733' AS DateTime), CAST(N'2021-12-13T15:45:52.733' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'COA', N'Check Out Area', CAST(N'2021-12-13T15:46:03.890' AS DateTime), CAST(N'2021-12-13T15:46:03.890' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'MCA', N'Manual Check Area', CAST(N'2021-12-13T15:48:16.153' AS DateTime), CAST(N'2021-12-13T15:48:16.153' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'PCA', N'Pre Clearance Area', CAST(N'2021-12-13T15:48:49.763' AS DateTime), CAST(N'2021-12-13T15:48:49.763' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (7, N'PSA', N'Pre San Area', CAST(N'2021-12-13T15:50:39.043' AS DateTime), CAST(N'2021-12-13T15:50:39.043' AS DateTime), 0)
INSERT [type].[AreaType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (8, N'SCA', N'Scan Area', CAST(N'2021-12-13T15:50:48.313' AS DateTime), CAST(N'2021-12-13T15:50:48.313' AS DateTime), 0)
SET IDENTITY_INSERT [type].[AreaType] OFF
GO
SET IDENTITY_INSERT [type].[ConclusionType] ON 

INSERT [type].[ConclusionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (0, N'NOT SET', NULL, CAST(N'2021-12-12T08:34:05.110' AS DateTime), CAST(N'2021-12-12T08:34:05.110' AS DateTime), 0)
INSERT [type].[ConclusionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'SUSPICIOUS', N'While in workflow or if defined as such', CAST(N'2021-12-12T08:34:05.110' AS DateTime), CAST(N'2021-12-12T08:34:05.110' AS DateTime), 0)
INSERT [type].[ConclusionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'PASS', N'Passed inspection', CAST(N'2021-12-12T08:34:05.110' AS DateTime), CAST(N'2021-12-12T08:34:05.110' AS DateTime), 0)
INSERT [type].[ConclusionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'Bypass', N'Bypass type will be used to indicate that an inspection was bypassed. This type should not be exposed for edit only for a team manager and / or be populated automatically behind the scenes, when a bypass request is approved. 
', CAST(N'2022-01-17T11:27:55.403' AS DateTime), CAST(N'2022-01-17T11:28:13.533' AS DateTime), 0)
SET IDENTITY_INSERT [type].[ConclusionType] OFF
GO
SET IDENTITY_INSERT [type].[DataCorrectionType] ON 

INSERT [type].[DataCorrectionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (0, N'NOT-SET', NULL, CAST(N'2021-12-13T10:14:35.640' AS DateTime), CAST(N'2021-12-13T10:14:35.640' AS DateTime), 0)
INSERT [type].[DataCorrectionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'CCR', N'Originally CCR correct', CAST(N'2021-12-13T10:14:56.150' AS DateTime), CAST(N'2021-12-13T10:14:56.150' AS DateTime), 0)
INSERT [type].[DataCorrectionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'MANUAL', N'Manual corrected', CAST(N'2021-12-13T10:15:05.983' AS DateTime), CAST(N'2021-12-13T10:15:05.983' AS DateTime), 0)
INSERT [type].[DataCorrectionType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'LPR', N'Originally LPR correct', CAST(N'2021-12-13T10:20:51.007' AS DateTime), CAST(N'2021-12-13T10:20:51.007' AS DateTime), 0)
SET IDENTITY_INSERT [type].[DataCorrectionType] OFF
GO
SET IDENTITY_INSERT [type].[DeviceCommandSourceType] ON 

INSERT [type].[DeviceCommandSourceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'IAW', N'XRay photography operator inspection system', CAST(N'2021-12-13T16:14:49.930' AS DateTime), CAST(N'2021-12-13T16:14:49.930' AS DateTime), 0)
INSERT [type].[DeviceCommandSourceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'BUSINESSLOGIC', N'System Business Logic', CAST(N'2021-12-13T16:15:22.230' AS DateTime), CAST(N'2021-12-13T16:15:22.230' AS DateTime), 0)
INSERT [type].[DeviceCommandSourceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'SYSTEM', N'Managment System', CAST(N'2021-12-13T16:16:09.943' AS DateTime), CAST(N'2021-12-13T16:16:09.943' AS DateTime), 0)
SET IDENTITY_INSERT [type].[DeviceCommandSourceType] OFF
GO
SET IDENTITY_INSERT [type].[DeviceEventType] ON 

INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'allowUp', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'changeVehicleInfo', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'changeWorkflow', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'checkIn', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'eventProcessFailed', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'fileDownload', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (7, N'finishWorkflow', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (8, N'imageAnalyze', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (9, N'ledDisplay', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (10, N'niiCardKey', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (11, N'niiEnterable', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (12, N'orderCorrection', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (13, N'receiptPrint', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (14, N'tagPrint', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (15, N'vehicleRefresh', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (16, N'warn', NULL, CAST(N'2021-12-13T15:39:23.247' AS DateTime), CAST(N'2021-12-13T15:39:23.247' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (17, N'vehicleImageUploaded', NULL, CAST(N'2021-12-14T08:49:29.703' AS DateTime), CAST(N'2021-12-14T08:49:29.703' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (18, N'vehicleScanning', NULL, CAST(N'2021-12-14T08:49:45.460' AS DateTime), CAST(N'2021-12-14T08:49:45.460' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (19, N'niiXrayOut', NULL, CAST(N'2021-12-14T08:50:02.440' AS DateTime), CAST(N'2021-12-14T08:50:02.440' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (20, N'niiScanEnd', NULL, CAST(N'2021-12-14T08:50:11.560' AS DateTime), CAST(N'2021-12-14T08:50:11.560' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (21, N'tagReader', NULL, CAST(N'2021-12-14T08:50:26.893' AS DateTime), CAST(N'2021-12-14T08:50:26.893' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (22, N'ipsConclusion', NULL, CAST(N'2021-12-14T08:50:41.460' AS DateTime), CAST(N'2021-12-14T08:50:41.460' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (23, N'ipcFaulty', NULL, CAST(N'2021-12-14T08:50:54.040' AS DateTime), CAST(N'2021-12-14T08:50:54.040' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (24, N'niiSystemStatus', NULL, CAST(N'2021-12-14T08:51:29.377' AS DateTime), CAST(N'2021-12-14T08:51:29.377' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (25, N'ocrCodeNumber', NULL, CAST(N'2021-12-14T08:51:49.687' AS DateTime), CAST(N'2021-12-14T08:51:49.687' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (26, N'ipcWell', NULL, CAST(N'2021-12-14T08:52:12.160' AS DateTime), CAST(N'2021-12-14T08:52:12.160' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (27, N'lprLicensePlate', NULL, CAST(N'2021-12-14T08:52:26.137' AS DateTime), CAST(N'2021-12-14T08:52:26.137' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (28, N'vehiclePripInfo', NULL, CAST(N'2021-12-14T08:52:39.927' AS DateTime), CAST(N'2021-12-14T08:52:39.927' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (29, N'niiScanStop', NULL, CAST(N'2021-12-14T08:52:54.313' AS DateTime), CAST(N'2021-12-14T08:52:54.313' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (30, N'closed', NULL, CAST(N'2021-12-14T08:53:08.260' AS DateTime), CAST(N'2021-12-14T08:53:08.260' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (31, N'opened', NULL, CAST(N'2021-12-14T08:53:23.780' AS DateTime), CAST(N'2021-12-14T08:53:23.780' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (32, N'receiptPrintReq', NULL, CAST(N'2021-12-14T08:53:37.653' AS DateTime), CAST(N'2021-12-14T08:53:37.653' AS DateTime), 0)
INSERT [type].[DeviceEventType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (33, N'וואלה', N'שדכדששגשג', CAST(N'2022-01-13T17:42:19.430' AS DateTime), CAST(N'2022-01-13T17:42:34.423' AS DateTime), 1)
SET IDENTITY_INSERT [type].[DeviceEventType] OFF
GO
SET IDENTITY_INSERT [type].[DeviceStatusType] ON 

INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'Closed', N'Closed', CAST(N'2021-12-13T16:04:57.630' AS DateTime), CAST(N'2021-12-13T16:04:57.630' AS DateTime), 0)
INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'Opened', N'Opened', CAST(N'2021-12-13T16:05:02.967' AS DateTime), CAST(N'2021-12-13T16:05:02.967' AS DateTime), 0)
INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'ipcFaulty', NULL, CAST(N'2021-12-13T16:05:24.027' AS DateTime), CAST(N'2021-12-13T16:05:24.027' AS DateTime), 0)
INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'ipcWell', NULL, CAST(N'2021-12-13T16:05:36.977' AS DateTime), CAST(N'2021-12-13T16:05:36.977' AS DateTime), 0)
INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'niiCardKey', NULL, CAST(N'2021-12-13T16:06:02.173' AS DateTime), CAST(N'2021-12-13T16:06:02.173' AS DateTime), 0)
INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'niiEnterable', NULL, CAST(N'2021-12-13T16:06:15.617' AS DateTime), CAST(N'2021-12-13T16:06:15.617' AS DateTime), 0)
INSERT [type].[DeviceStatusType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (7, N'niiSystemStatus', NULL, CAST(N'2021-12-13T16:06:27.493' AS DateTime), CAST(N'2021-12-13T16:06:27.493' AS DateTime), 0)
SET IDENTITY_INSERT [type].[DeviceStatusType] OFF
GO
SET IDENTITY_INSERT [type].[DeviceType] ON 

INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (0, N'NOT-SET', N'', CAST(N'2021-12-27T16:49:22.903' AS DateTime), CAST(N'2022-01-13T14:19:55.177' AS DateTime), 1)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'Barrier', N'Barrier', CAST(N'2021-12-13T15:54:37.550' AS DateTime), CAST(N'2021-12-13T15:54:37.550' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'LED', N'Notification LED Sign', CAST(N'2021-12-13T15:54:56.710' AS DateTime), CAST(N'2021-12-13T15:54:56.710' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'LPR', N'License Plate Reader Camera', CAST(N'2021-12-13T15:55:18.310' AS DateTime), CAST(N'2021-12-13T15:55:18.310' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'OCR', N'Optical Conteiner Recognition. Reader of container number (code) and license plate number (LPR + CCR)', CAST(N'2021-12-13T16:00:50.660' AS DateTime), CAST(N'2021-12-13T16:00:50.660' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'TagReader', N'Tag reader', CAST(N'2021-12-13T16:01:13.003' AS DateTime), CAST(N'2021-12-13T16:01:13.003' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'NII', N'Non-Intrusive Imaging (security) - X-Ray', CAST(N'2021-12-13T16:02:02.737' AS DateTime), CAST(N'2021-12-13T16:02:02.737' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (7, N'X-Ray', N'X-Ray', CAST(N'2021-12-15T14:12:40.593' AS DateTime), CAST(N'2021-12-15T14:12:40.593' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (8, N'SYSWS', N'Trucks system work station', CAST(N'2021-12-27T16:00:55.417' AS DateTime), CAST(N'2021-12-27T16:00:55.417' AS DateTime), 0)
INSERT [type].[DeviceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (9, N'Printer', N'Printer', CAST(N'2021-12-27T16:51:59.940' AS DateTime), CAST(N'2022-01-13T14:31:11.707' AS DateTime), 0)
SET IDENTITY_INSERT [type].[DeviceType] OFF
GO
SET IDENTITY_INSERT [type].[InspectionDataType] ON 

INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'OCR-LPR', N'Licence plate number taken at an OCR gate', CAST(N'2021-12-13T09:41:14.583' AS DateTime), CAST(N'2021-12-13T09:41:14.583' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'OCR-CCR', N'Container number taken at an OCR gate', CAST(N'2021-12-13T09:41:36.897' AS DateTime), CAST(N'2021-12-13T09:41:36.897' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'LPR', N'Licence plate number taken at an LPR standalone camera', CAST(N'2021-12-13T09:42:18.850' AS DateTime), CAST(N'2021-12-13T09:42:18.850' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'XRay', N'X Ray scaning data', CAST(N'2021-12-13T10:05:28.513' AS DateTime), CAST(N'2021-12-13T10:05:28.513' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'TagReader', N'Tag reader', CAST(N'2021-12-14T11:56:41.920' AS DateTime), CAST(N'2021-12-14T11:56:41.920' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'DriverTag', N'Driver tag html', CAST(N'2021-12-15T08:48:03.550' AS DateTime), CAST(N'2021-12-15T08:48:03.550' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (7, N'ManifestNum', N'Manifest number', CAST(N'2021-12-15T08:50:16.040' AS DateTime), CAST(N'2021-12-15T08:50:16.040' AS DateTime), 0)
INSERT [type].[InspectionDataType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (8, N'ManifestDoc', N'Manifest scanned document', CAST(N'2021-12-15T08:51:01.050' AS DateTime), CAST(N'2021-12-15T08:51:01.050' AS DateTime), 0)
SET IDENTITY_INSERT [type].[InspectionDataType] OFF
GO
SET IDENTITY_INSERT [type].[InspectionHistorySourceType] ON 

INSERT [type].[InspectionHistorySourceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'User', N'User action or update', CAST(N'2021-12-14T12:00:25.403' AS DateTime), CAST(N'2021-12-14T12:00:25.403' AS DateTime), 0)
INSERT [type].[InspectionHistorySourceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'System', N'Managment system business logic', CAST(N'2021-12-14T12:00:42.680' AS DateTime), CAST(N'2021-12-14T12:00:42.680' AS DateTime), 0)
INSERT [type].[InspectionHistorySourceType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'Device', N'Input that comes from a device', CAST(N'2021-12-14T12:00:59.907' AS DateTime), CAST(N'2021-12-14T12:00:59.907' AS DateTime), 0)
SET IDENTITY_INSERT [type].[InspectionHistorySourceType] OFF
GO
SET IDENTITY_INSERT [type].[InspectionOperationType] ON 

INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'CHECKIN', N'Checkin', CAST(N'2021-12-13T10:34:36.677' AS DateTime), CAST(N'2021-12-13T10:34:36.677' AS DateTime), 0)
INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'CHANGEVEHICLEINFO', N'Change inspection vehicle information (CCR or LPR)', CAST(N'2021-12-13T10:34:45.490' AS DateTime), CAST(N'2021-12-13T10:34:45.490' AS DateTime), 0)
INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'CHANGEWORKFLOW', N'Change workflow', CAST(N'2021-12-13T10:34:53.520' AS DateTime), CAST(N'2021-12-13T10:34:53.520' AS DateTime), 0)
INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'VEHICLESCANNED', N'Vehicle scanned', CAST(N'2021-12-13T10:34:58.457' AS DateTime), CAST(N'2021-12-13T10:34:58.457' AS DateTime), 0)
INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'IAWCONCLUSION', N'XRay photography conclusion', CAST(N'2021-12-13T10:35:02.953' AS DateTime), CAST(N'2021-12-13T10:35:02.953' AS DateTime), 0)
INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'MCWCONCLUSION', N'Manual conclusion', CAST(N'2021-12-13T10:35:07.567' AS DateTime), CAST(N'2021-12-13T10:35:07.567' AS DateTime), 0)
INSERT [type].[InspectionOperationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (7, N'FINISHWORKFLOW', N'Workflow finished - End of inspection', CAST(N'2021-12-13T10:35:11.587' AS DateTime), CAST(N'2021-12-13T10:35:11.587' AS DateTime), 0)
SET IDENTITY_INSERT [type].[InspectionOperationType] OFF
GO
SET IDENTITY_INSERT [type].[InspectionWorkflowType] ON 

INSERT [type].[InspectionWorkflowType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'R', N'X-Ray', CAST(N'2021-12-13T10:40:52.080' AS DateTime), CAST(N'2021-12-13T10:40:52.080' AS DateTime), 0)
INSERT [type].[InspectionWorkflowType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'RD', N'X-Ray & Detaction', CAST(N'2021-12-13T10:40:52.080' AS DateTime), CAST(N'2021-12-13T10:40:52.080' AS DateTime), 0)
INSERT [type].[InspectionWorkflowType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'M', N'Manual', CAST(N'2021-12-13T10:40:52.080' AS DateTime), CAST(N'2021-12-13T10:40:52.080' AS DateTime), 0)
INSERT [type].[InspectionWorkflowType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'RM', N'X-Ray & Manual', CAST(N'2021-12-13T10:40:52.080' AS DateTime), CAST(N'2021-12-13T10:40:52.080' AS DateTime), 0)
INSERT [type].[InspectionWorkflowType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (5, N'RDM', N'X-Ray & Detaction & Manual', CAST(N'2021-12-13T10:40:52.080' AS DateTime), CAST(N'2021-12-13T10:40:52.080' AS DateTime), 0)
INSERT [type].[InspectionWorkflowType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (6, N'E', N'Exit', CAST(N'2021-12-13T10:40:52.080' AS DateTime), CAST(N'2021-12-13T10:40:52.080' AS DateTime), 0)
SET IDENTITY_INSERT [type].[InspectionWorkflowType] OFF
GO
SET IDENTITY_INSERT [type].[NotificationType] ON 

INSERT [type].[NotificationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'Bypess', N'Bypass Approval Request', CAST(N'2022-01-17T13:44:40.590' AS DateTime), CAST(N'2022-01-17T13:55:13.800' AS DateTime), 0)
INSERT [type].[NotificationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'Checkin', N'Checkin Popup', CAST(N'2022-01-19T13:32:20.730' AS DateTime), CAST(N'2022-01-19T13:32:20.730' AS DateTime), 0)
SET IDENTITY_INSERT [type].[NotificationType] OFF
GO
SET IDENTITY_INSERT [type].[VehicleType] ON 

INSERT [type].[VehicleType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'Ispection', N'Truck and container', CAST(N'2021-12-13T08:17:36.967' AS DateTime), CAST(N'2021-12-13T08:17:36.967' AS DateTime), 0)
INSERT [type].[VehicleType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'Bluck', N'Trucks that may have technical issues or its driver is not legitimate', CAST(N'2021-12-13T08:18:59.910' AS DateTime), CAST(N'2021-12-13T08:18:59.910' AS DateTime), 0)
INSERT [type].[VehicleType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'White', N'Mostly private employees cars. As for creation of this entry, private cars are managed by a different system.', CAST(N'2021-12-13T08:20:15.077' AS DateTime), CAST(N'2022-01-16T11:10:08.040' AS DateTime), 0)
INSERT [type].[VehicleType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'Bypass', N'Service vehicles and / or trucks', CAST(N'2021-12-13T08:21:12.213' AS DateTime), CAST(N'2022-01-17T11:24:13.960' AS DateTime), 0)
SET IDENTITY_INSERT [type].[VehicleType] OFF
GO
SET IDENTITY_INSERT [type].[WorkStationType] ON 

INSERT [type].[WorkStationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (1, N'IAW', N'XRay photography operator inspection', CAST(N'2021-12-13T10:29:00.507' AS DateTime), CAST(N'2021-12-13T10:29:00.507' AS DateTime), 0)
INSERT [type].[WorkStationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (2, N'System', N'Managment System', CAST(N'2021-12-13T10:29:26.970' AS DateTime), CAST(N'2021-12-13T10:29:26.970' AS DateTime), 0)
INSERT [type].[WorkStationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (3, N'MCW', N'Manual Check', CAST(N'2021-12-13T10:29:42.407' AS DateTime), CAST(N'2021-12-13T10:29:42.407' AS DateTime), 0)
INSERT [type].[WorkStationType] ([id], [name], [description], [created], [updated], [isDeleted]) VALUES (4, N'SCS', N'XRay Scanner', CAST(N'2021-12-13T10:30:17.347' AS DateTime), CAST(N'2021-12-13T10:30:17.347' AS DateTime), 0)
SET IDENTITY_INSERT [type].[WorkStationType] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Vehicle_LPR_Unique]    Script Date: 1/23/2022 9:25:00 AM ******/
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [IX_Vehicle_LPR_Unique] UNIQUE NONCLUSTERED 
(
	[LicensePlateNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [auth].[User] ADD  CONSTRAINT [DF_User_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [auth].[User] ADD  CONSTRAINT [DF_User_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [auth].[User] ADD  CONSTRAINT [DF_User_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Area] ADD  CONSTRAINT [DF_Area_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[Area] ADD  CONSTRAINT [DF_Area_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[Area] ADD  CONSTRAINT [DF_Area_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Device_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Device_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Device_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Device_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[DeviceEvent] ADD  CONSTRAINT [DF_DeviceEvent_created]  DEFAULT (getdate()) FOR [timestamp]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_WorkflowTypeId]  DEFAULT ((2)) FOR [WorkflowTypeId]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_LicencePlateNumberCorrectionTypeId]  DEFAULT ((3)) FOR [LPRCorrectionTypeId]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_ContainerCodeCorrectionTypeId]  DEFAULT ((1)) FOR [CCRCorrectionTypeId]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_ConclusionTypeId]  DEFAULT ((1)) FOR [ConclusionTypeId]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_arrivaltime]  DEFAULT (getdate()) FOR [ArrivalTime]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[Inspection] ADD  CONSTRAINT [DF_Inspection_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[InspectionData] ADD  CONSTRAINT [DF_InspectionData_Value]  DEFAULT ('Unknown') FOR [Value]
GO
ALTER TABLE [dbo].[InspectionData] ADD  CONSTRAINT [DF_InspectionData_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[InspectionData] ADD  CONSTRAINT [DF_InspectionData_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[InspectionData] ADD  CONSTRAINT [DF_InspectionData_isDeleted]  DEFAULT ((1)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[InspectionDataHistory] ADD  CONSTRAINT [DF_InspectionDataHistory_Value]  DEFAULT ('Unknown') FOR [Value]
GO
ALTER TABLE [dbo].[InspectionDataHistory] ADD  CONSTRAINT [DF_InspectionDataHistory_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[InspectionDataHistory] ADD  CONSTRAINT [DF_InspectionDataHistory_isDeleted]  DEFAULT ((1)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[InspectionHistory] ADD  CONSTRAINT [DF_InspectionHistory_ConclusionTypeId]  DEFAULT ((1)) FOR [ConclusionTypeId]
GO
ALTER TABLE [dbo].[InspectionHistory] ADD  CONSTRAINT [DF_InspectionHistory_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[InspectionHistory] ADD  CONSTRAINT [DF_InspectionHistory_isDeleted]  DEFAULT ((1)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Lane] ADD  CONSTRAINT [DF_Lane_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[Lane] ADD  CONSTRAINT [DF_Lane_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[Lane] ADD  CONSTRAINT [DF_Lane_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_IsReplyable]  DEFAULT ((1)) FOR [IsReplyable]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_IsHandled]  DEFAULT ((0)) FOR [IsHandled]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_VehicleTypeId]  DEFAULT ((1)) FOR [VehicleTypeId]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[WorkStation] ADD  CONSTRAINT [DF_WorkStation_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[WorkStation] ADD  CONSTRAINT [DF_WorkStation_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[WorkStation] ADD  CONSTRAINT [DF_WorkStation_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[WorkStation] ADD  CONSTRAINT [DF_WorkStation_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] ADD  CONSTRAINT [DF_TableDisplayedColumns_isHidden]  DEFAULT ((1)) FOR [visiable]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] ADD  CONSTRAINT [DF_TableDisplayedColumns_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] ADD  CONSTRAINT [DF_TableDisplayedColumns_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [mgmt].[TableDisplayedColumns] ADD  CONSTRAINT [DF_TableDisplayedColumns_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[AreaType] ADD  CONSTRAINT [DF_AreaType_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[AreaType] ADD  CONSTRAINT [DF_AreaType_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[AreaType] ADD  CONSTRAINT [DF_AreaType_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[ConclusionType] ADD  CONSTRAINT [DF_Conclusion_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[ConclusionType] ADD  CONSTRAINT [DF_Conclusion_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[ConclusionType] ADD  CONSTRAINT [DF_Conclusion_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[DataCorrectionType] ADD  CONSTRAINT [DF_OCRCorrection_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[DataCorrectionType] ADD  CONSTRAINT [DF_OCRCorrection_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[DataCorrectionType] ADD  CONSTRAINT [DF_OCRCorrection_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[DeviceCommandSourceType] ADD  CONSTRAINT [DF_DeviceCommandSource_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[DeviceCommandSourceType] ADD  CONSTRAINT [DF_DeviceCommandSource_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[DeviceCommandSourceType] ADD  CONSTRAINT [DF_DeviceCommandSource_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[DeviceEventType] ADD  CONSTRAINT [DF_DeviceEventType_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[DeviceEventType] ADD  CONSTRAINT [DF_DeviceEventType_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[DeviceEventType] ADD  CONSTRAINT [DF_DeviceEventType_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[DeviceStatusType] ADD  CONSTRAINT [DF_DeviceStatus_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[DeviceStatusType] ADD  CONSTRAINT [DF_DeviceStatus_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[DeviceStatusType] ADD  CONSTRAINT [DF_DeviceStatus_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[DeviceType] ADD  CONSTRAINT [DF_DeviceType_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[DeviceType] ADD  CONSTRAINT [DF_DeviceType_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[DeviceType] ADD  CONSTRAINT [DF_DeviceType_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[InspectionDataType] ADD  CONSTRAINT [DF_InspectionData_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[InspectionDataType] ADD  CONSTRAINT [DF_InspectionData_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[InspectionDataType] ADD  CONSTRAINT [DF_InspectionData_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[InspectionHistorySourceType] ADD  CONSTRAINT [DF_InspectionHistorySourceType_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[InspectionHistorySourceType] ADD  CONSTRAINT [DF_InspectionHistorySourceType_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[InspectionHistorySourceType] ADD  CONSTRAINT [DF_InspectionHistorySourceType_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[InspectionOperationType] ADD  CONSTRAINT [DF_InspectionOperation_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[InspectionOperationType] ADD  CONSTRAINT [DF_InspectionOperation_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[InspectionOperationType] ADD  CONSTRAINT [DF_InspectionOperation_isDeleted]  DEFAULT ((1)) FOR [isDeleted]
GO
ALTER TABLE [type].[InspectionWorkflowType] ADD  CONSTRAINT [DF_InspectionWorkflow_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[InspectionWorkflowType] ADD  CONSTRAINT [DF_InspectionWorkflow_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[InspectionWorkflowType] ADD  CONSTRAINT [DF_InspectionWorkflow_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[NotificationType] ADD  CONSTRAINT [DF_NotificationType_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[NotificationType] ADD  CONSTRAINT [DF_NotificationType_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[NotificationType] ADD  CONSTRAINT [DF_NotificationType_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[VehicleType] ADD  CONSTRAINT [DF_VehicleType_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[VehicleType] ADD  CONSTRAINT [DF_VehicleType_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[VehicleType] ADD  CONSTRAINT [DF_VehicleType_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [type].[WorkStationType] ADD  CONSTRAINT [DF_WorkStation_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [type].[WorkStationType] ADD  CONSTRAINT [DF_WorkStation_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [type].[WorkStationType] ADD  CONSTRAINT [DF_WorkStation_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Area]  WITH CHECK ADD  CONSTRAINT [FK_Area_AreaType] FOREIGN KEY([AreaTypeId])
REFERENCES [type].[AreaType] ([id])
GO
ALTER TABLE [dbo].[Area] CHECK CONSTRAINT [FK_Area_AreaType]
GO
ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_Device_DeviceType] FOREIGN KEY([DeviceTypeId])
REFERENCES [type].[DeviceType] ([id])
GO
ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_Device_DeviceType]
GO
ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_Device_Lane] FOREIGN KEY([LaneId])
REFERENCES [dbo].[Lane] ([id])
GO
ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_Device_Lane]
GO
ALTER TABLE [dbo].[DeviceEvent]  WITH CHECK ADD  CONSTRAINT [FK_DeviceEvent_DeviceEventType] FOREIGN KEY([DeviceEventTypeId])
REFERENCES [type].[DeviceEventType] ([id])
GO
ALTER TABLE [dbo].[DeviceEvent] CHECK CONSTRAINT [FK_DeviceEvent_DeviceEventType]
GO
ALTER TABLE [dbo].[Inspection]  WITH CHECK ADD  CONSTRAINT [FK_Inspection_ConclusionType] FOREIGN KEY([ConclusionTypeId])
REFERENCES [type].[ConclusionType] ([id])
GO
ALTER TABLE [dbo].[Inspection] CHECK CONSTRAINT [FK_Inspection_ConclusionType]
GO
ALTER TABLE [dbo].[Inspection]  WITH CHECK ADD  CONSTRAINT [FK_Inspection_WorkflowType] FOREIGN KEY([WorkflowTypeId])
REFERENCES [type].[InspectionWorkflowType] ([id])
GO
ALTER TABLE [dbo].[Inspection] CHECK CONSTRAINT [FK_Inspection_WorkflowType]
GO
ALTER TABLE [dbo].[InspectionData]  WITH CHECK ADD  CONSTRAINT [FK_InspectionData_InspectionDataType] FOREIGN KEY([DataTypeId])
REFERENCES [type].[InspectionDataType] ([id])
GO
ALTER TABLE [dbo].[InspectionData] CHECK CONSTRAINT [FK_InspectionData_InspectionDataType]
GO
ALTER TABLE [dbo].[InspectionDataHistory]  WITH CHECK ADD  CONSTRAINT [FK_InspectionDataHistory_UpdaterSourceType] FOREIGN KEY([UpdaterSourceTypeId])
REFERENCES [type].[InspectionHistorySourceType] ([id])
GO
ALTER TABLE [dbo].[InspectionDataHistory] CHECK CONSTRAINT [FK_InspectionDataHistory_UpdaterSourceType]
GO
ALTER TABLE [dbo].[Lane]  WITH CHECK ADD  CONSTRAINT [FK_Lane_AreaId] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([id])
GO
ALTER TABLE [dbo].[Lane] CHECK CONSTRAINT [FK_Lane_AreaId]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_SourceUserId] FOREIGN KEY([SourceUserId])
REFERENCES [auth].[User] ([id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_SourceUserId]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_TargetUserId] FOREIGN KEY([TargetUserId])
REFERENCES [auth].[User] ([id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_TargetUserId]
GO
ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_VehicleType] FOREIGN KEY([VehicleTypeId])
REFERENCES [type].[VehicleType] ([id])
GO
ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_Vehicle_VehicleType]
GO
ALTER TABLE [dbo].[WorkStation]  WITH CHECK ADD  CONSTRAINT [FK_WorkStation_WorkStationType] FOREIGN KEY([WorkStationTypeId])
REFERENCES [type].[WorkStationType] ([id])
GO
ALTER TABLE [dbo].[WorkStation] CHECK CONSTRAINT [FK_WorkStation_WorkStationType]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_type_table_entries]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_get_type_table_entries] 
(	
	@schema varchar(20),
	@table_name varchar(50),
	@include_deleted bit = 0
)
AS

/*
	sp_GetTypeTableEntries 'type','VehicleType'
	sp_GetTypeTableEntries 'type','VehicleType', true
	sp_GetTypeTableEntries 'type','VehicleType', false
	sp_GetTypeTableEntries 'type','VehicleType', 0
*/
BEGIN
	DECLARE @sql varchar(500) = ' 
	SELECT 
		[id] As Id
		,[name] As [Name]
		,[description] As [Description]
		,FORMAT([created], ''dd/MM/yyyy HH:mm:ss'') As [Created]
		,FORMAT([updated], ''dd/MM/yyyy HH:mm:ss'') As [Updated]
		,[isDeleted] As IsDeleted
		
	FROM ' + @schema + '.' + @table_name 
	
	IF @include_deleted = 0
		BEGIN
			SET @sql = @sql + '
	WHERE isDeleted = 0' 
		END
	


	PRINT (@sql)
	EXEC (@sql)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_inspection_add]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[sp_inspection_add]
			@vehicleId int,
			@inspectionTypeId int, -- Workflow
			@lpr varchar(20),
			@lprCorrectionTypeId int,
			@ccr varchar(20),
			@ccrCorrectionTypeId int,
			@manifestCode varchar(20),
			@driverTagCode varchar(20),
			@conclosionTypeId int,
			@arrivalTime datetime,
			@remark varchar(300)
			
AS
BEGIN
INSERT INTO [dbo].[Inspection]
           (
			[Identifier]
			,[VehicleId]
			,[WorkflowTypeId]
			,[LPR]
			,[LPRCorrectionTypeId]
			,[CCR]
			,[CCRCorrectionTypeId]
			,[ManifestCode]
			,[DriverTagCode]
			,[ConclusionTypeId]
			,[ArrivalTime]
			,[Remark]
		   )
VALUES
           (
		   (select [dbo].[fn_getNextInspectionIdentifier] ())
		   ,@vehicleId
           ,@inspectionTypeId
           ,@lpr
           ,@lprCorrectionTypeId
		   ,@ccr
		   ,@ccrCorrectionTypeId
		   ,@manifestCode
		   ,@driverTagCode
		   ,@conclosionTypeId
		   ,@arrivalTime
		   ,@remark
           )
END
GO
/****** Object:  StoredProcedure [dbo].[sp_inspection_init]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_inspection_init]
	@lpr varchar(20),
	@ccr varchar(20),
	@temp_arrival_time datetime

AS

/*
	EXEC sp_inspection_init '7380952', 'MCLU5092803', '2021-12-28 10:29:33.400'
*/

BEGIN
	
	DECLARE @VehicleId int;


	-- When OCR1 trigger event with photos this proceadure needs to:
	-- 1. Check is the vehicle exists - Create and / or return vehicle id.

	
	PRINT ''
	PRINT 'Validating if a vehicle by LPR ' + @lpr + ' exists. ADD if not exists.'
	IF NOT EXISTS (SELECT 1 FROM [dbo].[Vehicle] v WHERE v.LicensePlateNumber = @lpr)
		BEGIN
			INSERT INTO [dbo].[Vehicle] ([LicensePlateNumber], [VehicleTypeId])
			VALUES (@lpr,1 /*1=Inspection*/);

			SELECT @VehicleId = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			SET @VehicleId = (SELECT Id FROM [dbo].[Vehicle] v WHERE v.LicensePlateNumber = @lpr)
		END

	PRINT ''
	PRINT 'VehicleId for LPR ' + @lpr +  ' is: ' + CAST(@VehicleId as varchar(50))

	PRINT ''
	PRINT 'Adding Inspection record.'

	INSERT INTO [dbo].[Inspection]
           (
			   [VehicleId]
			   ,[ArrivalTime]
			   ,[LPR]
			   ,[CCR]
           )

	SELECT
		@VehicleId
		,@temp_arrival_time -- This value might be changed later when vehicle arrives check in. That event is conceddured as the actual arrival. For this first insert it is OK to use OCR event time
		,@lpr
		,@ccr
	
	
	SELECT SCOPE_IDENTITY()

END
GO
/****** Object:  StoredProcedure [dbo].[sp_inspection_update]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	EXEC	[dbo].[sp_update_user] @id = N'14', @password = N'Aa123'
*/

CREATE PROC [dbo].[sp_inspection_update]
			@id int,
			@inspectionTypeId int = -1,
			@lpr varchar(20) = NULL,
			@lprCorrectionTypeId int = -1,
			@ccr varchar(20) = NULL,
			@ccrCorrectionTypeId int = -1,
			@manifestCode varchar(20) = NULL,
			@driverTagCode varchar(20) = NULL,
			@conclusionTypeId int= -1,
			@remark varchar(300) = NULL,
			@arrivalTime datetime = NULL,
			@leaveTime datetime = NULL,
			@inspectionEndTime datetime = NULL
			
AS
BEGIN

	UPDATE [dbo].[Inspection] 
		SET 
		WorkflowTypeId = ISNULL(NULLIF(@inspectionTypeId, -1), WorkflowTypeId),
		LPR = ISNULL(NULLIF(@lpr, ''), LPR),
		LPRCorrectionTypeId = ISNULL(NULLIF(@lprCorrectionTypeId, -1), LPRCorrectionTypeId),
		CCR = ISNULL(NULLIF(@ccr, ''), CCR),
		CCRCorrectionTypeId = ISNULL(NULLIF(@ccrCorrectionTypeId, -1), CCRCorrectionTypeId),
		ManifestCode = ISNULL(NULLIF(@manifestCode, ''), ManifestCode),
		DriverTagCode = ISNULL(NULLIF(@driverTagCode, ''), DriverTagCode),
		ConclusionTypeId = ISNULL(NULLIF(@conclusionTypeId, -1), ConclusionTypeId),
		Remark = ISNULL(NULLIF(@remark, ''), Remark),
		ArrivalTime = ISNULL(NULLIF(@arrivalTime, ''), ArrivalTime),
		LeaveTime = ISNULL(NULLIF(@leaveTime, ''), LeaveTime),
		InspectionEndTime = ISNULL(NULLIF(@inspectionEndTime, ''), InspectionEndTime),

		[updated] = getdate()

	WHERE [id]=@id 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_add]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[sp_notification_add]
			@notification_type_id int,
			@subject varchar(50),
			@message varchar(250),
			@src_user_id int = NULL,
			@json_data varchar(1000) = NULL,
			@target_user_id int = NULL
			
			
AS
BEGIN
	INSERT INTO [dbo].[Notification]
			   ([NotificationTypeId]
			   ,[Subject]
			   ,[Message]
			   ,[SourceUserId]
			   ,[JsonData]
			   ,[TargetUserId]
			   )
	VALUES
			(
			   @notification_type_id,
			   @subject,
			   @Message,
			   @src_user_id,
			   @json_data,
			   @target_user_id
		   
			 )
END
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_add_return_id]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [dbo].[sp_notification_add_return_id]
			@notification_type_id int,
			@subject varchar(50),
			@message varchar(250),
			@src_user_id int = NULL,
			@json_data varchar(1000) = NULL,
			@target_user_id int = NULL,
			@id int output
			
			
AS
BEGIN
	INSERT INTO [dbo].[Notification]
			   ([NotificationTypeId]
			   ,[Subject]
			   ,[Message]
			   ,[SourceUserId]
			   ,[JsonData]
			   ,[TargetUserId]
			   )
	VALUES
			(
			   @notification_type_id,
			   @subject,
			   @Message,
			   @src_user_id,
			   @json_data,
			   @target_user_id
		   
			 )
	SELECT @id = SCOPE_IDENTITY() 

END
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_update]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[sp_notification_update]
			@id int,
			@notification_type_id int = NULL,
			@subject varchar(50) = NULL,
			@message varchar(250) = NULL,
			@src_user_id int = NULL,
			@json_data varchar(1000) = NULL,
			@target_user_id int = NULL,
			@reply varchar(250) = NULL,
			@is_replyable bit = 1
			
			
AS
BEGIN

	
	UPDATE [dbo].[Notification]
	SET
		[NotificationTypeId] = ISNULL(NULLIF(@notification_type_id, ''), [NotificationTypeId])
		,[Subject] = ISNULL(NULLIF(@subject, ''), [Subject])
		,[Message] = ISNULL(NULLIF(@message, ''), [Message])
		,[SourceUserId] = ISNULL(NULLIF(@src_user_id, ''), [SourceUserId])
		,[JsonData] = ISNULL(NULLIF(@json_data, ''), [JsonData])
		,[TargetUserId] = ISNULL(NULLIF(@target_user_id, ''), [TargetUserId])
		,[Reply] = ISNULL(NULLIF(@reply, ''), [Reply])
		,IsReplyable = @is_replyable
		,IsHandled = CASE WHEN ISNULL(NULLIF(@reply, ''), '') IS NULL THEN 0 ELSE 1 END
		,Updated = GETDATE()
		   
	WHERE [id] = @id 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_notification_update_ishandled]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[sp_notification_update_ishandled]
			@id int,
			@is_handled bit
			
			
AS
BEGIN

	
	UPDATE [dbo].[Notification]
	SET IsHandled = @is_handled
		,Updated = GETDATE()
		   
	WHERE [id] = @id 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_type_entry_add]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_type_entry_add] 
(	
	@schema varchar(20),
	@table_name varchar(50),
	@name varchar(100),
	@description varchar(250)
)
AS


BEGIN
	DECLARE @sql varchar(2000) = ' 
	INSERT INTO ' + @schema + '.' + @table_name + ' 
		([name] ,[description])
		VALUES(''0' + @name + ''',''' + @description +''')'

	PRINT (@sql)
	EXEC (@sql)
END
GO
/****** Object:  StoredProcedure [dbo].[sp_type_entry_update]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[sp_type_entry_update] 
(	
	@schema varchar(20),
	@table_name varchar(50),
	@id int,
	@name varchar(100) = NULL,
	@description varchar(250) = NULL
)
AS
/*
	[dbo].[sp_type_entry_update] 'type', 'VehicleType', 4, 'Byass'
*/

BEGIN

	
	IF ISNULL(@name, '') = '' AND ISNULL(@description, '') = '' BEGIN RETURN END

	DECLARE @sql varchar(2000) = ''
	
	IF ISNULL(@name, '') != '' 
		BEGIN
			SET @sql = 'UPDATE ' + @schema + '.' + @table_name + ' SET ' 
			SET @sql = @sql + ' Name = ''' + @name + ''' , Updated = GETDATE() WHERE Id = ' + CAST(@id as varchar) 
			PRINT (@sql)
			EXEC (@sql)
		END

	IF ISNULL(@description, '') != '' 
		BEGIN
			SET @sql = 'UPDATE ' + @schema + '.' + @table_name + ' SET ' 
			SET @sql = @sql + ' Description = ''' + @description + ''' , Updated = GETDATE() WHERE Id = ' + CAST(@id as varchar) 
			PRINT (@sql)
			EXEC (@sql)
		END
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_user_add]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_user_add]
			@firstName varchar(50),
			@lastName varchar(50),
			@email varchar(250),
			@password varchar(10)
			
AS
BEGIN
INSERT INTO [auth].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password])
VALUES
           (
		   @firstName,
           @lastName,
           @email,
           @password
           )
END
GO
/****** Object:  StoredProcedure [dbo].[sp_user_update]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	EXEC	[dbo].[sp_update_user] @id = N'14', @password = N'Aa123'
*/

CREATE PROC [dbo].[sp_user_update]
			@id int,
			@firstName varchar(50) = NULL,
			@lastName varchar(50) = NULL,
			@email varchar(250) = NULL,
			@password varchar(10) = NULL
			
AS
BEGIN

	UPDATE [auth].[User] 
		SET 
		FirstName = ISNULL(NULLIF(@firstName, ''), firstName),
		LastName = ISNULL(NULLIF(@lastName, ''), lastName),
		Email = ISNULL(NULLIF(@email, ''), email),
		[Password] = ISNULL(NULLIF(@password, ''), [password]),
		[updated] = getdate()

	WHERE Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[sp_vehicle_add]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_vehicle_add]
			@licensePlateNumber varchar(20),
			@VehicleTypeId int = 1, -- Inspectin
			@remark varchar(250) = NULL
			
AS
BEGIN
INSERT INTO [dbo].[Vehicle]
           (
		   [LicensePlateNumber]
           ,[VehicleTypeId]
		   ,[Remark]
		   )
VALUES
           (
		   @licensePlateNumber,
           ISNULL(NULLIF(@VehicleTypeId, -1), 1),
           @remark
		   )
END
GO
/****** Object:  StoredProcedure [dbo].[sp_vehicle_update]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_vehicle_update]
			@id int,
			@licensePlateNumber varchar(20) = NULL,
			@VehicleTypeId int = 1, -- Inspection
			@remark varchar(250) = NULL
			
AS
BEGIN
UPDATE [dbo].[Vehicle]
         
SET 
           LicensePlateNumber	= ISNULL(NULLIF(@licensePlateNumber, ''), LicensePlateNumber),
		   VehicleTypeId		= ISNULL(NULLIF(@VehicleTypeId, -1), VehicleTypeId),
		   Remark				= ISNULL(NULLIF(@remark, ''), Remark),
		   Updated = GETDATE()
WHERE Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[sp_work_station_add]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[sp_work_station_add]
			@name varchar(100),
			@description varchar(250),
			@ip varchar(20),
			@mac varchar(20),
			@workStationTypeId int = 2 -- System
			
			
AS
BEGIN
	INSERT INTO [dbo].[WorkStation]
			   ([Name]
			   ,[Description]
			   ,[IP]
			   ,[MAC]
			   ,[WorkStationTypeId]
			   )
	VALUES
			   (
			   @name,
			   @description,
			   @ip,
			   @mac,
			   ISNULL(NULLIF(@workStationTypeId, -1), 2)-- System
		   
			   )
END
GO
/****** Object:  StoredProcedure [dbo].[sp_work_station_update]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

*/

CREATE PROC [dbo].[sp_work_station_update]
			@id int,
			@name varchar(100),
			@description varchar(250) = NULL,
			@ip varchar(20) = NULL,
			@mac varchar(20) = NULL,
			@workStationTypeId int = 2, -- Management System
			@isActive bit = NULL,
			@isDeleted bit = null
			
AS
BEGIN

	UPDATE [dbo].[WorkStation] 
		SET 
		[Name] = ISNULL(NULLIF(@name, ''), [Name]),
		[Description] = ISNULL(NULLIF(@description, ''), [Description]),
		[IP] = ISNULL(NULLIF(@ip, ''), [IP]),
		[MAC] = ISNULL(NULLIF(@mac, ''), [MAC]),
		[WorkStationTypeId] = ISNULL(NULLIF(@workStationTypeId, ''), [WorkStationTypeId]),
		[IsActive] = ISNULL(@isActive, [IsActive]),
		[isDeleted] = ISNULL(@isDeleted, [isDeleted]),
		[updated] = getdate()

	WHERE [id] = @id 
END
GO
/****** Object:  StoredProcedure [mgmt].[sp_add_table_display_columns]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [mgmt].[sp_add_table_display_columns]
	@tblName varchar(100)
	,@scheme varchar(20) = 'dbo'

AS
/*
	EXEC mgmt.sp_add_table_display_columns 'WorkStation'
	EXEC mgmt.sp_add_table_display_columns 'InspectionWorkflowType', 'type'
*/

BEGIN

	INSERT INTO [mgmt].[TableDisplayedColumns]
			   ([TableName]
			   ,[ColumnName]
			   ,[DisplayOrder]
			  )


	SELECT 
		TableName = ISNULL(NULLIF(@scheme,''),'dbo') + '.' + @tblName
		,[Name] = UPPER(LEFT(c.[name],1))+SUBSTRING(c.[name],2,LEN(c.[name])) 
		,c.column_id
	FROM sys.columns c 
	INNER JOIN sys.tables t on t.object_id = c.object_id
	WHERE t.[name] = @tblName
	ORDER BY c.column_id

	
	INSERT INTO [mgmt].[TableDisplayedColumns]
			   ([TableName]
			   ,[ColumnName]
			   ,[DisplayOrder]
			  )

	SELECT
	TableName = ISNULL(NULLIF(@scheme,''),'dbo') + '.' + @tblName
	,'Actions'
	,[DisplayOrder] = (
						Select max(c.column_id) + 1
						from sys.columns c 
						inner join sys.tables t on t.object_id = c.object_id
						where t.[name] = @tblName
					)
END
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_device_events_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [mgmt].[sp_import_device_events_from_sdms]
AS

/*
	EXEC mgmt.sp_import_device_events_from_sdms
*/


BEGIN
	-- STEP 1 - Fix xml parsing errors at events sources
	PRINT ''
	PRINT 'Updating [Israel_AshDod_PB].[dbo].[EventHistory] known xml errors' 

	-- Update data column to be xml with no errors
	
	-- Update remark - Event History
	update [Israel_AshDod_PB].[dbo].[EventHistory]
		set [data] = 
				Replace(
					[data]
					,SUBSTRING([data], CHARINDEX('<entry key="remark">', [data]), charindex('</entry>', [data], charindex('<entry key="remark">', [data])) + 8 - charindex('<entry key="remark">', [data]))
					,'<entry key="remark">Xml Parse Error Replaced</entry>'
					)
		where [data] like '%<entry key="remark">×%'

    
	-- Update ccr  - Event History
	update [Israel_AshDod_PB].[dbo].[EventHistory]
		set [data] = 
				Replace(
					[data]
					,SUBSTRING([data], CHARINDEX('<entry key="ccr">', [data]), charindex('</entry>', [data], charindex('<entry key="ccr">', [data])) + 8 - charindex('<entry key="ccr">', [data]))
					,'<entry key="ccr">Xml Parse Error Replaced</entry>'
					)
		where [data] like '%<entry key="ccr">×%'

	-- Tag reader  - Event History
	update [Israel_AshDod_PB].[dbo].[EventHistory]
		set [data] = 
			Replace(
					[data]
					,'&#2;'
					,''
					)
	where [data] like '%<entry key="barcode">&#2;%'

	
	-- Update remark - Event
	update [Israel_AshDod_PB].[dbo].[Event]
		set [data] = 
				Replace(
					[data]
					,SUBSTRING([data], CHARINDEX('<entry key="remark">', [data]), charindex('</entry>', [data], charindex('<entry key="remark">', [data])) + 8 - charindex('<entry key="remark">', [data]))
					,'<entry key="remark">Xml Parse Error Replaced</entry>'
					)
		where [data] like '%<entry key="remark">×%'

    
	-- Update ccr  - Event
	update [Israel_AshDod_PB].[dbo].[Event]
		set [data] = 
				Replace(
					[data]
					,SUBSTRING([data], CHARINDEX('<entry key="ccr">', [data]), charindex('</entry>', [data], charindex('<entry key="ccr">', [data])) + 8 - charindex('<entry key="ccr">', [data]))
					,'<entry key="ccr">Xml Parse Error Replaced</entry>'
					)
		where [data] like '%<entry key="ccr">×%'

	-- Tag reader  - Event
	update [Israel_AshDod_PB].[dbo].[Event]
		set [data] = 
			Replace(
					[data]
					,'&#2;'
					,''
					)
	where [data] like '%<entry key="barcode">&#2;%'

	
	-- STEP 2 Validate source
	declare @temp xml;

	BEGIN TRY  
		PRINT ''
		PRINT 'Validating [Israel_AshDod_PB].[dbo].[Event] known xml errors' 
		select @temp = Convert (xml, t.data, 2) from  [Israel_AshDod_PB].[dbo].[Event] t;
		
		
	END TRY  
	BEGIN CATCH  
		PRINT ''
		PRINT 'Validating [Israel_AshDod_PB].[dbo].[Event] table faild. Please search and correct errors and try importing again.' 
		RETURN;

	END CATCH  
	
	BEGIN TRY  
		PRINT ''
		PRINT 'Validating [Israel_AshDod_PB].[dbo].[EventHistory] known xml errors' 
		select @temp = Convert (xml, t.data, 2) from  [Israel_AshDod_PB].[dbo].[EventHistory] t;
		
		
	END TRY  
	BEGIN CATCH  
		PRINT ''
		PRINT 'Validating [Israel_AshDod_PB].[dbo].[EventHistory] table faild. Please search and correct errors and try importing again.' 
		RETURN;


	END CATCH  
	
	PRINT ''
	PRINT 'Validating Events source - SUCCESS.' 

	-- - Import into DeviceEvents table


	PRINT ''
	PRINT 'Truncating (cleaning) [dbo].[DeviceEvent] table.' 

	TRUNCATE TABLE [dbo].[DeviceEvent]
	DBCC CHECKIDENT('DeviceEvent', RESEED, 1)

	PRINT ''
	PRINT 'Importing into [dbo].[DeviceEvent] table.' 
	PRINT 'Dont forget to:' 
	PRINT '	1. Remove where statement.' 
	PRINT '	2. Add importing of new events without truncating.' 
	PRINT '	3. In the future the xml validations and insertions should be per event as they come from source devices !!!!!.' 

	INSERT INTO [dbo].[DeviceEvent]
		([DeviceId], [DeviceEventTypeId], [Data], [InspectionId], [Remark], [SourceInspectionIdentifier], [SourceTable], [SourceId], [timestamp])

	SELECT 
		--,t.DeviceName
		d.id as DeviceId
		,DeviceEventTypeId = et.id 
		--,StrData = t.[data]
		,[Data] = convert(XML, [data],2)
		--,EventName = t.[event]
		,InspectionId = i.id
		,[Remark] = 'Imported'
		,SourceInspectioIdentifier = t.vehicleId
		,t.[SourceTable]
		,[SourceTableID] = t.id
		,t.eventDateTime
	from

	(
	SELECT  [id]
		  ,[DeviceName] = [site] + cast(laneNumber as varchar(20)) + REPLACE(device, 'SDMS', 'SYSWS') + cast(deviceNumber as varchar(20)) COLLATE Hebrew_100_CI_AS
		  ,[data]
		  ,[event] = [event] COLLATE Hebrew_100_CI_AS
		  ,[eventDateTime]
		  ,[success]
		  ,[vehicleId]
		  ,[SourceTable] = 'EventHistory'
	  FROM [Israel_AshDod_PB].[dbo].[EventHistory]
	  where data like '%7380952%'  and [eventDateTime] > '2021-12-22 00:00:00.410'


	UNION  

	SELECT [id]
		  ,[DeviceName] = [site] + cast(laneNumber as varchar(20)) + REPLACE(device, 'SDMS', 'SYSWS') + cast(deviceNumber as varchar(20)) COLLATE Hebrew_100_CI_AS
		  ,[data]
		  ,[event] = [event] COLLATE Hebrew_100_CI_AS
		  ,[eventDateTime]
		  ,[success]
		  ,[vehicleId]
		  ,[SourceTable] = 'Event'
	  FROM [Israel_AshDod_PB].[dbo].[Event]
	   where data like '%7380952%'  and [eventDateTime] > '2021-12-22 00:00:00.410'
  
	  ) t

	LEFT JOIN [dbo].[Device] d on d.[name] = t.DeviceName
	LEFT JOIN [type].[DeviceEventType] et on et.[name] = t.[event]
	LEFT JOIN [dbo].[Inspection] i on i.SourceInspectionIdentifier =  LTRIM(RTRIM(t.vehicleId)) COLLATE Hebrew_100_CI_AS 

	PRINT ''
	PRINT 'Importing into [dbo].[DeviceEvent] table - Success.' 

END
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [mgmt].[sp_import_from_sdms]
AS

/*
	EXEC mgmt.sp_import_from_sdms
*/


BEGIN
	
	PRINT ''
	PRINT 'Importing Vehicles.' 

	EXEC mgmt.sp_import_vehicles_from_sdms ;
	
	PRINT ''
	PRINT 'Importing Inspections.' 

	EXEC mgmt.sp_import_inspection_from_sdms

	PRINT ''
	PRINT 'Importing Inspection Data.' 

	EXEC mgmt.sp_import_inspection_data_from_sdms 1

	PRINT ''
	PRINT 'Importing Device Events.'
	EXEC mgmt.sp_import_device_events_from_sdms

END
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_inspection_data_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [mgmt].[sp_import_inspection_data_from_sdms]

	@inspection_id int

AS

/*
	EXEC mgmt.sp_import_inspection_data_from_sdms 12
*/


BEGIN
	
	-- Import into InspectionData table


	PRINT ''
	PRINT 'Cleaning [dbo].[InspectionData] table. TBD !! in the future remove truncation' 

	DELETE FROM [dbo].[InspectionData]
	DBCC CHECKIDENT('InspectionData', RESEED, 1)
	
	INSERT INTO [dbo].[InspectionData]
			   ([InspectionId]
			   ,[DataTypeId]
			   ,[Value]
			   ,[Qualifier]
			   ,[DataPath]
			   ,[Remark]
			   ,[SourceInspectionIdentifier]
			   ,[SourceTable]
			   ,[SourceId]
			   ,[created]
			   ,[updated]
			   )


	SELECT	
		[InspectionId] = @inspection_id
		,[DataTypeId] = 2 -- OCR-CCR
		,[Value] = v.ccrResult
		,[ValueQualifier] = v.ccrCredibility
		,[DataPath] = x.[value]
		,[Remark] = 'Imported'
		,[SourceInspectionIdentifier] = v.vehicleId -- Actually this is the inspection and not some truck!!
		,[SourceTable] = 'OCRHistory' 
		,[SourceId] = v.id
		,[created] = v.ccrDateTime
		,[updated] = v.ccrDateTime
		

	FROM [Israel_AshDod_PB].[dbo].[VehicleOCRHistory] v
	Cross Apply (select value from STRING_SPLIT(v.ccrPhoto,',')) x
	WHERE v.ccrCorrectionType = 'CCRCORRECT'

	AND v.ccrPhoto like '%BIA1OCR1/2021/12/22/115254%'

	UNION

	-- All LPRCORRECT ==> No need to update history for changes
	SELECT	[InspectionId] = @inspection_id
			,[DataTypeId] = 1 -- OCR-LPR
			,[Value] = v.lprResult
			,[ValueQualifier] = v.lprCredibility
			,[DataPath] = x.[value]
			,[Remark] = 'Imported'
			,[SourceInspectionIdentifier] = v.vehicleId -- Actually this is the inspection and not some truck!!
			,[SourceTable] = 'OCRHistory' 
			,[SourceId] = v.id
			,[created] = v.lprDateTime
			,[updated] = v.lprDateTime


	FROM [Israel_AshDod_PB].[dbo].[VehicleOCRHistory] v
	Cross Apply (select value from STRING_SPLIT(v.lprPhotoPath,',')) x
	WHERE v.lprCorrectionType = 'LPRCORRECT'

	AND v.lprPhotoPath like '%BIA1OCR1/2021/12/22/115254%'

	--UNION

	-- LPR DATA

	PRINT ''
	PRINT 'Importing into [dbo].[InspectionData] table - Success.' 

	-- Import into InspectionDataHistory table

	PRINT ''
	PRINT 'Cleaning [dbo].[InspectionData] table. TBD !! in the future remove truncation' 
	
	DELETE FROM [dbo].[InspectionDataHistory]
	DBCC CHECKIDENT('InspectionDataHistory', RESEED, 1)
	
	
	
	PRINT ''
	PRINT 'Insert into [dbo].[InspectionDataHistory] table.' 

	INSERT INTO [dbo].[InspectionDataHistory]
           ([InspectionDataId]
		   ,[InspectionId]
           ,[DataTypeId]
           ,[Value]
           ,[Qualifier]
           ,[DataPath]
           ,[Remark]
           ,[UpdaterSourceTypeId]
           ,[UpdaterIdentifier]
           ,[created]
           ,[isDeleted])

    SELECT
		src.id
		,src.InspectionId
		,src.DataTypeId
		,src.[Value]
		,src.Qualifier
		,src.DataPath
		,src.Remark
		,2 -- System
		,NULL
		,src.created
		,src.isDeleted
	FROM [dbo].[InspectionData] src

	PRINT ''
	PRINT 'Insert into [dbo].[InspectionDataHistory] table - Success.' 

END
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_inspection_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [mgmt].[sp_import_inspection_from_sdms]

AS

/*
	EXEC mgmt.sp_import_inspection_from_sdms
*/


BEGIN
	
	-- Import into Inspection table


	PRINT ''
	PRINT 'Cleaning [dbo].[Inspection] table. TBD !! in the future remove truncation' 

	TRUNCATE TABLE [dbo].[Inspection]
	DBCC CHECKIDENT('Inspection', RESEED, 1)
	
	INSERT INTO [dbo].[Inspection]
			   (
				VehicleId
				,WorkFlowTypeId
				,LPR
				,[LPRCorrectionTypeId]
				,CCR
				,[CCRCorrectionTypeId]
				,ManifestCode
				,DriverTagCode
				,ConclusionTypeId
				,ArrivalTime
				,InspectionEndTime
				,LeaveTime
				,remark
				,SourceInspectionIdentifier
				,SourceTable
				,[SourceId]
			   )


	SELECT 
		VehicleId = (Select id from [dbo].[Vehicle] vc where  vc.LicensePlateNumber = v.[licensePlateNumber] COLLATE Hebrew_100_CI_AS)
		--,WorkFlowTypeName = v.[inspectWorkFlow]
		,WorkFlowTypeId = wft.id
		,LPR = LTRIM(RTRIM(v.[licensePlateNumber]))
		,[LPRCorrectionTypeId] = 0 -- NOT SET
		,CCR = v.[containerNumberCsv]
		,[CCRCorrectionTypeId] = 0 -- NOT SET
		,ManifestCode = v.[manifestNo]
		,DriverTagCode = v.[driverTag]
		--,ConclusionTypeName = v.[conclusionType]
		,[ConclusionTypeId] = ct.id
		,ArrivalTime = v.[arrivedSiteDate]
		,InspectionEndTime = v.[concludeDateTime]
		,LeaveTime = v.[checkOutDateTime]
		,v.[remark]
		,SourceInspectionIdentifier = v.[id]
		,SourceTable = 'IsraelVehicleHistory'
		,[SourceId] = v.[id]

		
	FROM [Israel_AshDod_PB].[dbo].[IsraelVehicleHistory] v
	LEFT JOIN [type].[InspectionWorkflowType] wft on wft.[name] = v.[inspectWorkFlow] COLLATE Hebrew_100_CI_AS
	LEFT JOIN [dbo].[Vehicle] vec on vec.LicensePlateNumber = LTRIM(RTRIM(v.[licensePlateNumber])) COLLATE Hebrew_100_CI_AS
	LEFT JOIN [type].[ConclusionType] ct on ct.[name] = v.[conclusionType] COLLATE Hebrew_100_CI_AS
	where v.id = '202112220064'

	PRINT ''
	PRINT 'Importing into [dbo].[Inspection] table - Success.' 

	
END
GO
/****** Object:  StoredProcedure [mgmt].[sp_import_vehicles_from_sdms]    Script Date: 1/23/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [mgmt].[sp_import_vehicles_from_sdms]
AS

/*
	EXEC mgmt.sp_import_vehicles_from_sdms
*/


BEGIN
	
	PRINT ''
	PRINT 'Truncating (cleaning) [dbo].[Vehicle] table.' 

	DELETE FROM [dbo].[Vehicle]
	DBCC CHECKIDENT('Vehicle', RESEED, 1)

	INSERT INTO [dbo].[Vehicle]
		([LicensePlateNumber], [VehicleTypeId], [Remark])

	SELECT DISTINCT
		LTRIM(RTRIM(vh.licensePlateNumber))
		,1 -- Inspection
		,'Imported'
		
	FROM Israel_AshDod_PB.[dbo].[IsraelVehicleHistory] vh
	WHERE nullif(vh.licensePlateNumber, '') != ''
		and vh.licensePlateNumber is not null
		and LEN(LTRIM(RTRIM(vh.licensePlateNumber))) = 7
		and LTRIM(RTRIM(vh.licensePlateNumber)) not in 
		(
		'0000000'
		,'0000002'
		,'0000003'
		,'0000333'
		)


	PRINT ''
	PRINT 'Importing into [dbo].[Vehicle] table - Success.' 

END
GO

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using Microsoft.Extensions.Configuration;

namespace EgrofanServer.DataAccess
{
    public class Dal : IDisposable
    {
        private readonly IConfiguration _configuration;
        private SqlConnection connection = new SqlConnection();
        private SqlDataAdapter adapter = new SqlDataAdapter();
        private SqlCommandBuilder builder = new SqlCommandBuilder();
        private int globalSqlTimeout = 60;
        private SqlCommand command = new SqlCommand();



        public Dal(IConfiguration configuration)
        {
            _configuration = configuration;
            connection.ConnectionString = _configuration.GetConnectionString("DevConnection");
            command.Connection = connection;
            adapter.SelectCommand = command;
            builder.DataAdapter = adapter;

            int.TryParse(_configuration.GetValue("GlobalSqlTimeout", "60"), out globalSqlTimeout);

            command.CommandTimeout = globalSqlTimeout;
        }

        public void Open()
        {
            connection.Open();
        }

        public void Close()
        {
            connection.Close();
        }

        public void BeginTransaction()
        {
            command.Transaction = connection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            command.Transaction.Commit();
        }

        public void RollBackTransaction()
        {
            command.Transaction.Rollback();
        }

        /// <summary>
        /// Execute by sql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int Execute(string sql)
        {
            int affectedRows;
            command.CommandText = sql;
            command.CommandType = CommandType.Text;

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
                affectedRows = command.ExecuteNonQuery();
                connection.Close();
            }
            else
            {
                affectedRows = command.ExecuteNonQuery();
            }
            return affectedRows;
        }

        /// <summary>
        /// Execute by sp
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int Execute(string storedProcedureName, SqlParameter[] parameters)
        {
            int affectedRows;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            command.Parameters.Clear();
            command.Parameters.AddRange(parameters);

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
                affectedRows = command.ExecuteNonQuery(); // Executing stored procedure!
                connection.Close();
            }
            else
            {
                affectedRows = command.ExecuteNonQuery();
            }
            return affectedRows;
        }

        /// <summary>
        /// Execute by sp
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="parameters">Including output parameters</param>
        /// <returns></returns>
        public SqlParameter[] ExecuteSPGetResalutValues(string storedProcedureName, SqlParameter[] parameters, ParameterDirection direction)
        {
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            command.Parameters.Clear();
            command.Parameters.AddRange(parameters);

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
                command.ExecuteNonQuery(); // Executing stored procedure!
                connection.Close();
            }
            else
            {
                command.ExecuteNonQuery(); // Executing stored procedure!
            }
            return parameters.Where(p => p.Direction == direction).Select(p => p).ToArray();
        }



        /// <summary>
        /// Get scalar by sql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public object GetScalar(string sql)
        {
            object obj = null;

            command.CommandText = sql;
            command.CommandType = CommandType.Text;

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
                obj = command.ExecuteScalar();
                connection.Close();
            }
            else
            {
                obj = command.ExecuteScalar();
            }
            return obj;
        }

        public SqlDataReader GetDataReader(string sql)
        {
            command.CommandText = sql;
            command.CommandType = CommandType.Text;
            return command.ExecuteReader();
        }

        /// <summary>
        /// Get table by sql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable GetTable(string sql)
        {
            command.CommandText = sql;
            command.CommandType = CommandType.Text;
            command.CommandTimeout = 120;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        /// <summary>
        /// Get table by sp
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetTable(string storedProcedureName, SqlParameter[] parameters)
        {
            DataTable dt = new DataTable();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            command.Parameters.Clear();
            if (null != parameters && parameters.Count() > 0)
            {
                command.Parameters.AddRange(parameters);
            }

            adapter.Fill(dt);
            return dt;
        }

        /// <summary>
        /// Get set of tables by sp
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataSet GetTables(string storedProcedureName, SqlParameter[] parameters)
        {
            DataSet ds = new DataSet();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            command.Parameters.Clear();
            if (null != parameters && parameters.Count() > 0)
            {
                command.Parameters.AddRange(parameters);
            }

            adapter.Fill(ds);
            return ds;

        }

        public void UpdateTable(DataTable dt)
        {
            adapter.Update(dt);
        }

        public void Dispose()
        {
            if (null != builder)
                builder.Dispose();
            if (null != command)
                command.Dispose();
            if (null != connection)
                connection.Dispose();
            if (null != adapter)
                adapter.Dispose();
        }



    }

    /// <summary>
    /// See http://codereview.stackexchange.com/questions/30714/converting-datatable-to-list-of-class
    /// </summary>
    public static class DalHelper
    {
        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {

            List<T> list = new List<T>();

            foreach (var row in table.AsEnumerable())
            {
                T obj = new T();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                list.Add(obj);
            }

            return list;
        }

        /// <summary>
        /// Converts DataRow into generic object.
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="row">DataRow</param>
        /// <returns>T</returns>
        public static T DataRowToObject<T>(this DataRow row) where T : class, new()
        {
            T obj = new T();

            foreach (var prop in obj.GetType().GetProperties())
            {
                try
                {
                    PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                    Type propertyType = propertyInfo.PropertyType;
                    // For handeling nullables see: https://www.examplefiles.net/cs/273408
                    var targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                    var propertyVal = Convert.ChangeType(row[prop.Name], targetType);
                    propertyInfo.SetValue(obj, propertyVal, null);
                    // This is the original code
                    //propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                }
                catch
                {
                    continue;
                }
            }

            return obj;
        }

        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
    }

}

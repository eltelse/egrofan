﻿using Microsoft.Extensions.Configuration;
using EgrofanServer.Controllers;
using EgrofanServer.DataAccess;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Core.BusinessLogic
{
    public class NotificationBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;

        public NotificationBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetNotificationDisplayColumns()
        {
            string sql = @"SELECT * from fn_GetTableColumns('dbo.Notification')";
            return _dal.GetTable(sql);
        }

        public DataTable GetNotifications()
        {
            string sql = @"select * from dbo.v_notifications";
            return _dal.GetTable(sql);
        }

        public DataTable GetNotificationById(string id)
        {
            string sql = "select * from dbo.v_notifications WHERE id=" + id;
            return _dal.GetTable(sql);
        }

        public string AddNotificationGetIdentity(NotificationModel i)
        {
            string res = "";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("notification_type_id", SqlDbType.Int) { Value = i.NotificationTypeId},
                    new SqlParameter("subject", SqlDbType.VarChar, 50) { Value = i.Subject},
                    new SqlParameter("message", SqlDbType.VarChar, 250) { Value = i.Message},
                    new SqlParameter("src_user_id", SqlDbType.Int) { Value = i.SourceUserId},
                    new SqlParameter("json_data", SqlDbType.VarChar, 1000) { Value = i.JsonData},
                    new SqlParameter("target_user_id", SqlDbType.Int) { Value = i.TargetUserId},
                    new SqlParameter("id", SqlDbType.Int){ Direction = ParameterDirection.Output },
                };

                var resParams = _dal.ExecuteSPGetResalutValues("sp_notification_add_return_id", pArr, ParameterDirection.Output);

                if (resParams.Length < 1)
                {
                    res = "Failure. Message: Something went wrong while adding notification.";
                    return res;
                }

                res = resParams[0].Value.ToString();

            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }


        public string AddNotification(NotificationModel i)
        {
            string res = "Success";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("notification_type_id", SqlDbType.Int) { Value = i.NotificationTypeId},
                    new SqlParameter("subject", SqlDbType.VarChar, 50) { Value = i.Subject},
                    new SqlParameter("message", SqlDbType.VarChar, 250) { Value = i.Message},
                    new SqlParameter("src_user_id", SqlDbType.Int) { Value = i.SourceUserId},
                    new SqlParameter("json_data", SqlDbType.VarChar, 1000) { Value = i.JsonData},
                    new SqlParameter("target_user_id", SqlDbType.Int) { Value = i.TargetUserId},
                };

                var affectedRows = _dal.Execute("sp_notification_add", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while adding notification.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateNotification(NotificationModel i)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = i.Id });

                if (i.NotificationTypeId != default(int)) dbParams.Add(new SqlParameter("notification_type_id", SqlDbType.Int) { Value = i.NotificationTypeId });
                if (!string.IsNullOrEmpty(i.Subject)) dbParams.Add(new SqlParameter("subject", SqlDbType.VarChar, 50) { Value = i.Subject });
                if (!string.IsNullOrEmpty(i.Message)) dbParams.Add(new SqlParameter("message", SqlDbType.VarChar, 250) { Value = i.Message });
                if (i.SourceUserId != default(int)) dbParams.Add(new SqlParameter("src_user_id", SqlDbType.Int) { Value = i.SourceUserId });
                if (!string.IsNullOrEmpty(i.JsonData)) dbParams.Add(new SqlParameter("json_data", SqlDbType.VarChar, 20) { Value = i.JsonData });
                if (i.TargetUserId != default(int)) dbParams.Add(new SqlParameter("target_user_id", SqlDbType.Int) { Value = i.TargetUserId });
                if (!string.IsNullOrEmpty(i.Reply)) dbParams.Add(new SqlParameter("reply", SqlDbType.VarChar, 20) { Value = i.Reply });
                if (i.IsReplyable.HasValue) dbParams.Add(new SqlParameter("is_replyable", SqlDbType.Bit) { Value = i.IsReplyable.Value });
                //if (i.IsHandled.HasValue) dbParams.Add(new SqlParameter("is_handled", SqlDbType.Bit) { Value = i.IsHandled.Value });
                
                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_notification_update", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while updating a notification.";

                //ServerSideEventsController.AddBypassNotification(i);
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateIsHandled(int id, bool isHandled)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = id });
                dbParams.Add(new SqlParameter("is_handled", SqlDbType.Bit) { Value = isHandled });

                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_notification_update_ishandled", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while updating a notification.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string DeleteNotification(string id)
        {
            string sql = "update dbo.Notification SET isDeleted = 1 WHERE id=" + id;
            return _dal.Execute(sql) > 0 ? "Success" : "Failure. Message: Something went wrong while deleting a notification."; ;
        }

    }
}

﻿using Microsoft.Extensions.Configuration;
using EgrofanServer.DataAccess;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Core.BusinessLogic
{
    public class UsersBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;
        public UsersBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetUserDisplayColumns()
        {
            string sql = @"SELECT * from fn_GetTableColumns('auth.user')";
            return _dal.GetTable(sql);
        }

        public DataTable GetUsers()
        {
            string sql = @"select * from dbo.v_users";
            return _dal.GetTable(sql);
        }

        public DataTable GetUserByEmail(string email)
        {
            string sql = "select * from dbo.v_users WHERE email=''  " + email + "''";
            return _dal.GetTable(sql);
        }

        public string AddUser(UserModel u)
        {
            string res = "Success";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("firstName", SqlDbType.VarChar, 50) { Value = u.FirstName},
                    new SqlParameter("lastName", SqlDbType.VarChar, 50) { Value = u.LastName },
                    new SqlParameter("email", SqlDbType.VarChar, 250) { Value = u.Email},
                    new SqlParameter("password", SqlDbType.VarChar, 10) { Value = u.Password },
                };

                var affectedRows = _dal.Execute("sp_user_add", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while adding a user into Users table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateUser(UserModel u)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = u.Id });

                if (!string.IsNullOrEmpty(u.FirstName)) dbParams.Add(new SqlParameter("firstName", SqlDbType.VarChar, 50) { Value = u.FirstName });
                if (!string.IsNullOrEmpty(u.LastName)) dbParams.Add(new SqlParameter("lastName", SqlDbType.VarChar, 50) { Value = u.LastName });
                if (!string.IsNullOrEmpty(u.Email)) dbParams.Add(new SqlParameter("email", SqlDbType.VarChar, 250) { Value = u.Email });
                if (!string.IsNullOrEmpty(u.Password)) dbParams.Add(new SqlParameter("password", SqlDbType.VarChar, 10) { Value = u.Password });

                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_user_update", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while updating a user in Users table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string DeleteUser(string id)
        {
            string sql = "update [auth].[user] Set isDeleted = 1 WHERE id=" + id;
            return _dal.Execute(sql) > 0 ? "Success" : "Failure. Message: Something went wrong while deleting a user from Users table.";
        }

        public Tuple<string, UserModel> ValidateUserCredentials(UserModel user)
        {
            try
            {
                string sql = "select * from auth.[user] WHERE email='" + user.Email + "' AND password='" + user.Password + "'";
                var table = _dal.GetTable(sql);

                if (table.Rows.Count > 0)
                    return new Tuple<string, UserModel>("Success", DalHelper.DataRowToObject<UserModel>(table.Rows[0]));
                else
                    return new Tuple<string, UserModel>("Invalidating user credentials.", null);
            }
            catch (Exception ex)
            {
                return new Tuple<string, UserModel>("Failure. Message: Something went wrong while validating user credentials.", null);
            }
        }

    }
}

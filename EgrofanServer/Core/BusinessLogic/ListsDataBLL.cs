﻿using Microsoft.Extensions.Configuration;
using EgrofanServer.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Core.BusinessLogic
{
    public class ListsDataBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;

        public ListsDataBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetInspectionTypes()
        {
            string sql = @"select * from dbo.v_inspection_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetConclusionTypes()
        {
            string sql = @"select * from dbo.v_conclusion_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetWorkStationTypes()
        {
            string sql = @"select * from dbo.v_work_station_types";
            return _dal.GetTable(sql);
        }

        internal object GetNotificationTypes()
        {
            string sql = @"select * from dbo.v_notification_types";
            return _dal.GetTable(sql);
        }

        internal object GetUsersList()
        {
            string sql = @"select * from dbo.v_users_list";
            return _dal.GetTable(sql);
        }

        internal object GetVehicleTypes()
        {
            string sql = @"select * from dbo.v_vehicle_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetLPRCorrectionTypes()
        {
            string sql = @"select * from dbo.v_lpr_correction_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetCCRCorrectionTypes()
        {
            string sql = @"select * from dbo.v_ccr_correction_types";
            return _dal.GetTable(sql);
        }


    }
}

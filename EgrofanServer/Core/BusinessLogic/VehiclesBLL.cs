﻿using Microsoft.Extensions.Configuration;
using EgrofanServer.DataAccess;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Core.BusinessLogic
{
    public class VehiclesBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;

        public VehiclesBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetVehicles()
        {
            string sql = @"select * from dbo.v_vehicles";
            return _dal.GetTable(sql);
        }

        public DataTable GetVehiclesDisplayColumns()
        {
            string sql = @"SELECT * from fn_GetTableColumns('dbo.Vehicle')";
            return _dal.GetTable(sql);
        }

        public DataTable GetVehicleById(string id)
        {
            string sql = "select * from dbo.v_vehicles WHERE [id]=" + id;
            return _dal.GetTable(sql);
        }

        public string AddVehicle(VehicleModel v)
        {
            string res = "Success";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("licensePlateNumber", SqlDbType.VarChar, 20) { Value = v.LicensePlateNumber},
                    new SqlParameter("vehicleTypeId", SqlDbType.Int) { Value = v.VehicleTypeId},
                    new SqlParameter("remark", SqlDbType.VarChar, 250) { Value = v.Remark },
                    
                };

                var affectedRows = _dal.Execute("sp_vehicle_add", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while adding a vehicle.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateVehicle(VehicleModel v)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = v.Id });

                if (!string.IsNullOrEmpty(v.LicensePlateNumber)) dbParams.Add(new SqlParameter("licensePlateNumber", SqlDbType.VarChar, 20) { Value = v.LicensePlateNumber });
                if (v.VehicleTypeId != default(int)) dbParams.Add(new SqlParameter("vehicleTypeId", SqlDbType.Int) { Value = v.VehicleTypeId });
                if (!string.IsNullOrEmpty(v.Remark)) dbParams.Add(new SqlParameter("remark", SqlDbType.VarChar, 250) { Value = v.Remark });
                
                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_vehicle_update", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while updating a vehicle.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string DeleteVehicle(string id)
        {
            string sql = "update dbo.Vehicle set isDeleted = 1 WHERE id=" + id;
            return _dal.Execute(sql) > 0 ? "Success" : "Failure. Message: Something went wrong while deleting a vehicle.";
        }
    }
}

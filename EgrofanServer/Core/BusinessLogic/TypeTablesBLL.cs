﻿using Microsoft.Extensions.Configuration;
using EgrofanServer.DataAccess;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPm.Core.BusinessLogic
{
    public class TypeTablesBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;

        public TypeTablesBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetTypeTableDisplayColumns(string schema, string tableName)
        {
            string sql = $"SELECT * from fn_GetTableColumns('{schema}.{tableName}')";
            return _dal.GetTable(sql);
        }

        public DataTable GetTypeTableEntries(string schema, string tableName, bool include_deleted = false)
        {
            try
            {
                SqlParameter[] pArr =
                      {
                    new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = schema },
                    new SqlParameter("table_name", SqlDbType.VarChar, 50) { Value = tableName },
                    new SqlParameter("include_deleted", SqlDbType.Bit) { Value = include_deleted },
                };

                return _dal.GetTable("sp_get_type_table_entries", pArr);
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }


        public string AddEntry(TypeTableModel m)
        {
            string res = "Success";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = m.Schema },
                    new SqlParameter("table_name", SqlDbType.VarChar, 50) { Value = m.TableName },
                    new SqlParameter("name", SqlDbType.VarChar, 100) { Value = m.Name },
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = m.Description},
                };

                var affectedRows = _dal.Execute("sp_type_entry_add", pArr);
                if (affectedRows < 1) res = $"Failure. Message: Something went wrong while adding an entry into  {m.Schema}.{m.TableName} table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateEntry(TypeTableModel m)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = m.Id });
                dbParams.Add(new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = m.Schema });
                dbParams.Add(new SqlParameter("table_name", SqlDbType.VarChar, 50) { Value = m.TableName });
                if (!string.IsNullOrEmpty(m.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = m.Name });
                if (!string.IsNullOrEmpty(m.Description)) dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = m.Description });
                
                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_type_entry_update", pArr);
                if (affectedRows < 1) res = $"Failure. Message: Something went wrong while updating an entry in {m.Schema}.{m.TableName} table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string DeleteEntry(TypeTableModel m)
        {
            string sql = $"update {m.Schema}.{m.TableName} SET isDeleted = 1 WHERE id=" + m.Id;
            return _dal.Execute(sql) > 0 ? "Success" : $"Failure. Message: Something went wrong while deleting an entry in {m.Schema}.{m.TableName} table."; ;
        }
    }
}

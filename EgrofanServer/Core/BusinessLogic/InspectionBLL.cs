﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using EgrofanServer.Controllers;
using EgrofanServer.DataAccess;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EgrofanServer.Core.BusinessLogic
{
    public class InspectionBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;

        public InspectionBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetInspectionDisplayColumns()
        {
            string sql = @"SELECT * from fn_GetTableColumns('dbo.inspection')";
            return _dal.GetTable(sql);
        }

        public DataTable GetInspections()
        {
            string sql = @"select * from dbo.v_inspections";
            return _dal.GetTable(sql);
        }

        public DataTable GetInspectionById(string id)
        {
            string sql = "select * from dbo.inspection WHERE id=" + id;
            return _dal.GetTable(sql);
        }

        public string AskForBypass(string id)
        {
            string res = "Success";

            try
            {
                var tbl = GetInspectionById(id);
                if (tbl.Rows.Count == 0)
                {
                    res = $"Failure. Message: InspectionId {id} was not found in the database.";
                    return res;
                }

                var i = tbl.Rows[0].DataRowToObject<InspectionModel>();
                NotificationModel n = new NotificationModel();
                n.Subject = "Bypass Approval Request";
                n.NotificationTypeId = (int)NotificationType.Bypass;
                n.Message = "Please approve checkin bypass";
                n.SourceUserId = 4; // TODO: get that from client
                n.TargetUserId = 4; // TODO: get that from client or some login on logged on manager

                var sb = new StringBuilder();
                sb.Append("{ ");
                sb.Append($"\"LPR\": \"{i.LPR}\", ");
                sb.Append($"\"CCR\": \"{i.CCR}\", ");
                sb.Append($"\"ArrivalTime\": \"{i.ArrivalTime}\" ");
                sb.Append(" }");
                n.JsonData = sb.ToString();


                NotificationBLL bll = new NotificationBLL(_configuration);
                string identity_msg = bll.AddNotificationGetIdentity(n);
                int identity = 0;
                if(identity_msg.Contains("Failure") || !Int32.TryParse(identity_msg, out identity))
                {
                    res = "Failure. Message: Something went wrong while trying to add bypass notification to the database.";
                    return res;
                }

                n.Id = identity;

                //Task.Delay(250); // Hopefully this will let the UI regain itself after the bypass click and be ready to listen to the notification comming back from server.
                ServerSideEventsController.AddNotification(n);

            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string AddInspection(InspectionModel i)
        {
            string res = "Success";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("vehicleId", SqlDbType.Int) { Value = i.VehicleId },
                    new SqlParameter("inspectionTypeId", SqlDbType.Int) { Value = i.WorkflowTypeId },
                    new SqlParameter("lpr", SqlDbType.VarChar, 20) { Value = i.LPR},
                    new SqlParameter("lprCorrectionTypeId", SqlDbType.Int) { Value = i.LPRCorrectionTypeId },
                    new SqlParameter("ccr", SqlDbType.VarChar, 20) { Value = i.CCR},
                    new SqlParameter("ccrCorrectionTypeId", SqlDbType.Int) { Value = i.CCRCorrectionTypeId },
                    new SqlParameter("manifestCode", SqlDbType.VarChar, 20) { Value = i.ManifestCode},
                    new SqlParameter("driverTagCode", SqlDbType.VarChar, 20) { Value = i.DriverTagCode},
                    new SqlParameter("arrivalTime", SqlDbType.DateTime) { Value = i.ArrivalTime},
                    new SqlParameter("remark", SqlDbType.VarChar, 300) { Value = i.Remark},
                };

                var affectedRows = _dal.Execute("sp_inspection_add", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while adding inspection into Inspection table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateInspection(InspectionModel i)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = i.Id });

                if (i.WorkflowTypeId != default(int)) dbParams.Add(new SqlParameter("inspectionTypeId", SqlDbType.Int) { Value = i.WorkflowTypeId });
                if (!string.IsNullOrEmpty(i.LPR)) dbParams.Add(new SqlParameter("lpr", SqlDbType.VarChar, 20) { Value = i.LPR });
                if (i.LPRCorrectionTypeId != default(int)) dbParams.Add(new SqlParameter("lprCorrectionTypeId", SqlDbType.Int) { Value = i.LPRCorrectionTypeId });
                if (!string.IsNullOrEmpty(i.CCR)) dbParams.Add(new SqlParameter("ccr", SqlDbType.VarChar, 20) { Value = i.CCR });
                if (i.CCRCorrectionTypeId != default(int)) dbParams.Add(new SqlParameter("ccrCorrectionTypeId", SqlDbType.Int) { Value = i.CCRCorrectionTypeId });
                if (!string.IsNullOrEmpty(i.ManifestCode)) dbParams.Add(new SqlParameter("manifestCode", SqlDbType.VarChar, 20) { Value = i.ManifestCode });
                if (!string.IsNullOrEmpty(i.DriverTagCode)) dbParams.Add(new SqlParameter("driverTagCode", SqlDbType.VarChar, 20) { Value = i.DriverTagCode });
                if (i.ConclusionTypeId != default(int)) dbParams.Add(new SqlParameter("conclusionTypeId", SqlDbType.Int) { Value = i.ConclusionTypeId });
                if (!string.IsNullOrEmpty(i.Remark)) dbParams.Add(new SqlParameter("remark", SqlDbType.VarChar, 300) { Value = i.Remark });
                if (i.ArrivalTime != default(DateTime)) dbParams.Add(new SqlParameter("arrivalTime", SqlDbType.DateTime) { Value = i.ArrivalTime });
                if (i.LeaveTime != default(DateTime)) dbParams.Add(new SqlParameter("leaveTime", SqlDbType.DateTime) { Value = i.LeaveTime });
                if (i.InspectionEndTime != default(DateTime)) dbParams.Add(new SqlParameter("inspectionEndTime", SqlDbType.DateTime) { Value = i.InspectionEndTime });

                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_inspection_update", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while updating an inspection.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string DeleteInspection(string id)
        {
            string sql = "update dbo.Inspection SET isDeleted = 1 WHERE id=" + id;
            return _dal.Execute(sql) > 0 ? "Success" : "Failure. Message: Something went wrong while deleting an inspection."; ;
        }

    }
}

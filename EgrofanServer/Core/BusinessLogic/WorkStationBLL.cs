﻿using Microsoft.Extensions.Configuration;
using EgrofanServer.DataAccess;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Core.BusinessLogic
{
    public class WorkStationBLL
    {
        private IConfiguration _configuration;
        private Dal _dal;

        public WorkStationBLL(IConfiguration configuration)
        {
            _configuration = configuration;
            _dal = new Dal(configuration);
        }

        public DataTable GetWorkStationsDisplayColumns()
        {
            string sql = @"SELECT * from fn_GetTableColumns('dbo.WorkStation')";
            return _dal.GetTable(sql);
        }
        

        public DataTable GetWorkStations()
        {
            string sql = @"select * from dbo.v_work_stations";
            return _dal.GetTable(sql);
        }

        public DataTable GetWorkStationByIp(string ip)
        {
            string sql = "select * from dbo.v_work_stations WHERE [IP]=''" + ip + "''";
            return _dal.GetTable(sql);
        }

        public DataTable GetWorkStationById(string id)
        {
            string sql = "select * from dbo.v_work_stations WHERE id=" + id;
            return _dal.GetTable(sql);
        }


        public string AddWorkStation(WorkStationModel u)
        {
            string res = "Success";

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("Name", SqlDbType.VarChar, 100) { Value = u.Name },
                    new SqlParameter("Description", SqlDbType.VarChar, 250) { Value = u.Description },
                    new SqlParameter("IP", SqlDbType.VarChar, 20) { Value = u.IP},
                    new SqlParameter("MAC", SqlDbType.VarChar, 20) { Value = u.MAC},
                    new SqlParameter("WorkStationTypeId", SqlDbType.Int) { Value = u.WorkStationTypeId },
                };

                var affectedRows = _dal.Execute("sp_work_station_add", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while adding a station into Station table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string UpdateWorkStation(WorkStationModel u)
        {
            string res = "Success";
            try
            {
                List<SqlParameter> dbParams = new List<SqlParameter>();

                dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = u.Id });

                if (!string.IsNullOrEmpty(u.Name)) dbParams.Add(new SqlParameter("Name", SqlDbType.VarChar, 100) { Value = u.Name });
                if (!string.IsNullOrEmpty(u.Description)) dbParams.Add(new SqlParameter("Description", SqlDbType.VarChar, 250) { Value = u.Description });
                if (!string.IsNullOrEmpty(u.IP)) dbParams.Add(new SqlParameter("IP", SqlDbType.VarChar, 20) { Value = u.IP });
                if (!string.IsNullOrEmpty(u.MAC)) dbParams.Add(new SqlParameter("MAC", SqlDbType.VarChar, 20) { Value = u.MAC });
                if (u.WorkStationTypeId != default(int)) dbParams.Add(new SqlParameter("workStationTypeId", SqlDbType.Int) { Value = u.WorkStationTypeId });
                if (u.IsActive.HasValue) dbParams.Add(new SqlParameter("isActive", SqlDbType.Bit) { Value = u.IsActive.Value });
                if (u.IsDeleted.HasValue) dbParams.Add(new SqlParameter("isDeleted", SqlDbType.Bit) { Value = u.IsDeleted.Value });

                SqlParameter[] pArr = dbParams.ToArray();

                var affectedRows = _dal.Execute("sp_work_station_update", pArr);
                if (affectedRows < 1) res = "Failure. Message: Something went wrong while updating a station into Stations table.";
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        public string DeleteWorkStation(string id)
        {
            string sql = "update dbo.WorkStation set isDeleted = 1 WHERE id=" + id;
            return _dal.Execute(sql) > 0 ? "Success" : "Failure. Message: Something went wrong while deleting a Work Station.";
        }

    }
}

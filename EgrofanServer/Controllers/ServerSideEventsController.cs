﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EgrofanServer.Core;
using EgrofanServer.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServerSideEventsController : ControllerBase
    {
        // See: https://stackoverflow.com/questions/44851970/implement-sending-server-sent-events-in-c-sharp-no-asp-net-mvc for BlockingCollection approach
        // I (Oren) decided to try and use a static list that can be accessd from other classes  to add items to the list using lock.


        private static List<NotificationModel> _notificationsList = new List<NotificationModel>();

        private static readonly object _lock = new object();
        public static void AddNotification(NotificationModel n)
        {
            lock (_lock)
            {
                _notificationsList.Add(n);
            }
        }

        [HttpGet]
        [Route("SubscribeNotifications")]
        public async Task SubscribeNotifications()
        {
            Response.Headers.Add("Content-Type", "text/event-stream");
            CancellationToken clientDisconnectToken = new CancellationToken();
            while (!clientDisconnectToken.IsCancellationRequested)
            {
                while (_notificationsList.Count == 0)
                    await Task.Delay(100);

                NotificationModel n = null;
                lock (_lock)
                {
                    if (_notificationsList.Count > 0)
                    {
                        n = _notificationsList.First();
                        _notificationsList.Remove(n);
                    }
                }

                if (n == null)
                {
                    // TODO: Log
                    continue;
                }


                var eventType = (NotificationType)n.NotificationTypeId;
                string eventName = eventType.ToString().ToLower();
                string eventItem = $"event: {eventName}\n";
                // Debug.WriteLine("Responsing to: " + eventItem);
                string dataItem = GetDataJson(n);
                byte[] eventItemBytes = ASCIIEncoding.ASCII.GetBytes(eventItem);
                byte[] dataItemBytes = ASCIIEncoding.ASCII.GetBytes(dataItem);

                
                await Response.Body.WriteAsync(eventItemBytes);
                await Response.Body.WriteAsync(dataItemBytes);
                await Response.Body.FlushAsync();

            }
        }

        private string GetDataJson(NotificationModel n)
        {
            var sb = new StringBuilder();
            sb.Append("data: { ");
            sb.Append($"\"NotificationId\": \"{n.Id}\", ");
            sb.Append($"\"Subject\": \"{n.Subject}\", ");
            sb.Append($"\"Message\": \"{n.Message}\", ");
            sb.Append($"\"JsonData\": {n.JsonData}, ");
            sb.Append($"\"SourceUserId\": \"{n.SourceUserId}\", ");
            sb.Append($"\"TargetUserId\": \"{n.TargetUserId}\", ");
            sb.Append($"\"Reply\": \"{n.Reply}\", ");
            var isReplyable = n.IsReplyable.HasValue ? n.IsReplyable.Value.ToString() : "false";
            sb.Append($"\"IsReplyable\": \"{isReplyable}\"");
            sb.Append(" }\n\n");

            return sb.ToString();

        }
    }


}

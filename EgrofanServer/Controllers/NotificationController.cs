﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EgrofanServer.DataAccess;
using Microsoft.AspNetCore.Hosting;
using EgrofanServer.Core.BusinessLogic;
using System.Dynamic;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class NotificationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private NotificationBLL _bll;

        public NotificationController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new NotificationBLL(configuration);
        }


        [Route("GetNotificationDisplayColumns")]
        [HttpGet]
        public JsonResult GetNotificationDisplayColumns()
        {
            return new JsonResult(_bll.GetNotificationDisplayColumns());
        }

        public JsonResult Get()
        {
            return new JsonResult(_bll.GetNotifications());
        }

        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetNotificationById(id));
        }

        [HttpPost]
        public JsonResult Post(NotificationModel Notification)
        {
            return new JsonResult(_bll.AddNotification(Notification));
        }


        [HttpPut]
        public JsonResult Put(NotificationModel Notification)
        {
            return new JsonResult(_bll.UpdateNotification(Notification));
        }

        [HttpGet("UpdateNotificationIsHandled/{id}/{is_deleted}")]
        public JsonResult UpdateNotificationIsHandled(string id, string is_deleted)
        {
            return new JsonResult(_bll.UpdateIsHandled(Int32.Parse(id), bool.Parse(is_deleted)));
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteNotification(id));
        }

    }

}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using EgrofanServer.Core.BusinessLogic;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private VehiclesBLL _bll;
        private ListsDataBLL _listBll;
        
        public VehicleController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new VehiclesBLL(configuration);
            _listBll = new ListsDataBLL(_configuration);
        }

        [Route("GetVehiclesDisplayColumns")]
        [HttpGet]
        public JsonResult GetGetVehiclesDisplayColumns()
        {
            return new JsonResult(_bll.GetVehiclesDisplayColumns());
        }


        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetVehicles());
        }

        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetVehicleById(id));
        }

        [HttpPost]
        public JsonResult Post(VehicleModel vehicle)
        {
            return new JsonResult(_bll.AddVehicle(vehicle));
        }

        [HttpPut]
        public JsonResult Put(VehicleModel vehicle)
        {
            return new JsonResult(_bll.UpdateVehicle(vehicle));
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteVehicle(id));
        }


        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + filename;

                using (var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                return new JsonResult(filename);
            }
            catch (Exception)
            {

                return new JsonResult("anonymous.png");
            }
        }
    }
}


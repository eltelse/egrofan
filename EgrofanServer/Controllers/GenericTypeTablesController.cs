﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EgrofanServer.DataAccess;
using Microsoft.AspNetCore.Hosting;
using EgrofanServer.Core.BusinessLogic;
using System.Dynamic;
using SdmsAPm.Core.BusinessLogic;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class GenericTypeTablesController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private TypeTablesBLL _bll;

        public GenericTypeTablesController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new TypeTablesBLL(configuration);
        }

        [Route("GetTypeTableDisplayColumns")]
        [HttpGet("{GetTypeTableDisplayColumns}/{schema}/{table_name}")]
        public JsonResult GetTypeTableDisplayColumns(string schema, string table_name)
        {
            return new JsonResult(_bll.GetTypeTableDisplayColumns(schema, table_name));
        }

        [HttpGet("{schema}/{table_name}")]
        public JsonResult Get(string schema, string table_name)
        {
            return new JsonResult(_bll.GetTypeTableEntries(schema, table_name));
        }

        [HttpPost]
        public JsonResult Post(TypeTableModel tt)
        {
            return new JsonResult(_bll.AddEntry(tt));
        }


        [HttpPut]
        public JsonResult Put(TypeTableModel tt)
        {
            return new JsonResult(_bll.UpdateEntry(tt));
        }

        [HttpDelete("{id}/{schema}/{table_name}")]
        public JsonResult Delete(string id, string schema, string table_name)
        {
            TypeTableModel tt = new TypeTableModel()
            {
                Id = Int32.Parse(id),
                Schema = schema,
                TableName = table_name
            };
            return new JsonResult(_bll.DeleteEntry(tt));
        }
    }
}

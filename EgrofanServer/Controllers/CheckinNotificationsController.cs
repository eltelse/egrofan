﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckinNotificationsController : ControllerBase
    {
        [HttpGet]
        public async Task Get()
        {
            string[] data = new string[] { "{\"msg\": \"Hello World!\"}", "{\"msg\": \"Hello Galaxy!\"}" };

            Response.Headers.Add("Content-Type", "text/event-stream");

            for (int i = 0; i < data.Length; i++)
            {
                await Task.Delay(TimeSpan.FromSeconds(5));
                string dataItem = $"data: {data[i]}\n\n";
                byte[] dataItemBytes = ASCIIEncoding.ASCII.GetBytes(dataItem);
                await Response.Body.WriteAsync(dataItemBytes, 0, dataItemBytes.Length);
                await Response.Body.FlushAsync();
            }
        }
    }


}

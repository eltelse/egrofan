﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EgrofanServer.DataAccess;
using Microsoft.AspNetCore.Hosting;
using EgrofanServer.Core.BusinessLogic;
using System.Dynamic;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class InspectionController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private InspectionBLL _bll;

        public InspectionController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new InspectionBLL(configuration);
        }


        [Route("GetInspectionDisplayColumns")]
        [HttpGet]
        public JsonResult GetInspectionDisplayColumns()
        {
            return new JsonResult(_bll.GetInspectionDisplayColumns());
        }

        public JsonResult Get()
        {
            return new JsonResult(_bll.GetInspections());
        }

        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetInspectionById(id));
        }

        [HttpPost]
        public JsonResult Post(InspectionModel Inspection)
        {
            return new JsonResult(_bll.AddInspection(Inspection));
        }

        [HttpGet]
        [Route("AskForBypass/{id}")]
        public JsonResult AskForBypass(string id)
        {
            return new JsonResult(_bll.AskForBypass(id));
        }


        [HttpPut]
        public JsonResult Put(InspectionModel Inspection)
        {
            return new JsonResult(_bll.UpdateInspection(Inspection));
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteInspection(id));
        }

    }

}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EgrofanServer.DataAccess;
using Microsoft.AspNetCore.Hosting;
using EgrofanServer.Core.BusinessLogic;
using System.Dynamic;

namespace EgrofanServer.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private UsersBLL _bll;

        public UserController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new UsersBLL(configuration);
        }

        [Route("GetUserDisplayColumns")]
        [HttpGet]
        public JsonResult GetUserDisplayColumns()
        {
            return new JsonResult(_bll.GetUserDisplayColumns());
        }

        public JsonResult Get()
        {
            return new JsonResult(_bll.GetUsers());
        }

        [HttpGet("{email}")]
        public JsonResult Get(string email)
        {
            return new JsonResult(_bll.GetUserByEmail(email));
        }

        [HttpPost]
        public JsonResult Post(UserModel user)
        {
            return new JsonResult(_bll.AddUser(user));
        }

        [Route("UpdatePassword")]
        [HttpPut]
        public JsonResult UpdatePassword(UserModel user)
        {
            string query = @"
                 update dbo.user set 
                 password = '" + user.Password + @"'
                 where email = " + user.Email + @" 
                 ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DevConnection");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Updated Successfully");
        }


        [HttpPut]
        public JsonResult Put(UserModel user)
        {
            return new JsonResult(_bll.UpdateUser(user));
        }

        /*   [Route("UpdateUserName")]
           public JsonResult UpdateUserName(string email, string lName)
           {
               string query = @"
                       update dbo.user set 
                       lastName = '" + lName + @"'
                       where email = " + email + @" 
                       ";
               DataTable table = new DataTable();
               string sqlDataSource = _configuration.GetConnectionString("DevConnection");
               SqlDataReader myReader;
               using (SqlConnection myCon = new SqlConnection(sqlDataSource))
               {
                   myCon.Open();
                   using (SqlCommand myCommand = new SqlCommand(query, myCon))
                   {
                       myReader = myCommand.ExecuteReader();
                       table.Load(myReader); ;

                       myReader.Close();
                       myCon.Close();
                   }
               }

               return new JsonResult("Updated Successfully");
           }
        */

        [HttpDelete("{id}")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteUser(id));
        }


        [Route("ValidateUserCredentials")]
        [HttpPost]
        public JsonResult ValidateUserCredentials(UserModel user)
        {
            var user_tpl = _bll.ValidateUserCredentials(user);
            dynamic res = new ExpandoObject();
            res.status = user_tpl.Item1;
            res.obj = user_tpl.Item2;
            
            return new JsonResult(res);
        }

    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Models;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EgrofanServer.DataAccess;
using Microsoft.AspNetCore.Hosting;
using EgrofanServer.Core.BusinessLogic;
using System.Dynamic;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SseController : ControllerBase
    {
        [HttpGet]
        public async Task Get()
        {
            var response = Response;
            response.Headers.Add("Content-Type", "text/event-stream");
            int numItem = 0;
            for (var i = 0; true; ++i)
            {
                string path = @"C:\2\5\";
                             bool isEmpty = !System.IO.Directory.EnumerateFiles(path).Any();

                             if (!isEmpty)
                             {
                    string text = System.IO.File.ReadAllText(@"C:\2\5\_f2.txt");
                //    System.IO.File.Delete(Path.Combine(@"C:\2\5\", "_f2.txt"));
                    //{ "msg": "hello", "id": 12345}
                    // WriteAsync requires `using Microsoft.AspNetCore.Http`
                    //await response.WriteAsync($"data:"+numItem+"\r\r");
                    await response.WriteAsync($"data:"+text +"\r\r ");

                    numItem++;
                                 await response.Body.FlushAsync();

                             }
                             await Task.Delay(5 * 1000);
            }

        }
    }
    //app.Use(async (context, next) =>
    // {
    //     if (context.Request.Path.ToString().Equals("/sse"))
    //     {
    //         var response = context.Response;
    //         response.Headers.Add("Content-Type", "text/event-stream");

    //         for (var i = 0; true; ++i)
    //         {

    //             string path = @"C:\2\5\";
    //             bool isEmpty = !System.IO.Directory.EnumerateFiles(path).Any();

    //             if (!isEmpty)
    //             {
    //                 // WriteAsync requires `using Microsoft.AspNetCore.Http`
    //                 await response.WriteAsync($"data: Middleware {i} at {DateTime.Now}\r\r");

    //                 await response.Body.FlushAsync();

    //             }
    //             await Task.Delay(5 * 1000);
    //         }

    //     }

    //     await next.Invoke();
    // });


    //[Route("api/[controller]")]
    //[ApiController]
    //public class SseController : ControllerBase
    //{
    //    private readonly IConfiguration _configuration;
    //    private readonly IWebHostEnvironment _env;
    //    private UsersBLL _bll;







    //    public SseController(IConfiguration configuration, IWebHostEnvironment env)
    //    {
    //        _configuration = configuration;
    //        _env = env;
    //     //   _bll = new UsersBLL(configuration);
    //    }


    //        [Route("/api/sse")]
    //        public class ServerSentEventController : Controller
    //    {
    //        [HttpGet]
    //        public async Task Get()
    //        {
    //            var response = Response;
    //            response.Headers.Add("Content-Type", "text/event-stream");

    //            for (var i = 0; true; ++i)
    //            {
    //                await response
    //                    .WriteAsync($"data: Controller {i} at {DateTime.Now}\r\r");

    //                response.Body.Flush();
    //                await Task.Delay(5 * 1000);
    //            }
    //        }
    //    }

    //    [HttpPost]
    //    public JsonResult Post(UserModel user)
    //    {
    //        return new JsonResult("SSE is here2");
    //    }

    //    [HttpPut]
    //    public JsonResult Put(UserModel user)
    //    {
    //        return new JsonResult("SSE is here3");
    //    }


    //}
}

﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Core.BusinessLogic;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkStationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private WorkStationBLL _bll;
        private ListsDataBLL _listBll;

        public WorkStationController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new WorkStationBLL(configuration);
            _listBll = new ListsDataBLL(_configuration);
        }

        [Route("GetWorkStationsDisplayColumns")]
        [HttpGet]
        public JsonResult GetWorkStationsDisplayColumns()
        {
            return new JsonResult(_bll.GetWorkStationsDisplayColumns());
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetWorkStations());
        }


        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetWorkStationById(id));
        }


        [HttpPost]
        public JsonResult Post(WorkStationModel station)
        {
            return new JsonResult(_bll.AddWorkStation(station));
        }


        [HttpPut]
        public JsonResult Put(WorkStationModel station)
        {
            return new JsonResult(_bll.UpdateWorkStation(station));
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteWorkStation(id));
        }
    }
}


﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EgrofanServer.Core.BusinessLogic;
using EgrofanServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EgrofanServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private ListsDataBLL _bll;

        public ListsController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
            _bll = new ListsDataBLL(configuration);
        }

        [HttpGet]
        [Route("GetCCRCorrectionTypes")]
        public JsonResult GetCCRCorrectionTypes()
        {
            return new JsonResult(_bll.GetCCRCorrectionTypes());
        }

        [HttpGet]
        [Route("GetLPRCorrectionTypes")]
        public JsonResult GetLPRCorrectionTypes()
        {
            return new JsonResult(_bll.GetLPRCorrectionTypes());
        }

        [HttpGet]
        [Route("GetConclusionTypes")]
        public JsonResult GetConclusionTypes()
        {
            return new JsonResult(_bll.GetConclusionTypes());
        }

        [HttpGet]
        [Route("GetInspectionTypes")]
        public JsonResult GetInspectionTypes()
        {
            return new JsonResult(_bll.GetInspectionTypes());
        }

        [HttpGet]
        [Route("GetNotificationTypes")]
        public JsonResult GetNotificationTypes()
        {
            return new JsonResult(_bll.GetNotificationTypes());
        }

        /// <summary>
        /// THis one returs a list of users for display or drop down select display and NOT for users CRUD purpose. That is why it is plased here in Lists Controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUsersList")]
        public JsonResult GetUsersList()
        {
            return new JsonResult(_bll.GetUsersList());
        }


        [HttpGet]
        [Route("GetVehicleTypes")]
        public JsonResult GetVehicleTypes()
        {
            return new JsonResult(_bll.GetVehicleTypes());
        }

        [HttpGet]
        [Route("GetWorkStationsTypes")]
        public JsonResult GetWorkStationsTypes()
        {
            return new JsonResult(_bll.GetWorkStationTypes());
        }

    }
}

